import 'package:trade_app/models/models.dart';
import 'package:equatable/equatable.dart';
import 'package:intl/intl.dart';
import 'package:trade_app/utilities/utilities.dart';

class ProductVM extends Equatable {
  final int id;
  final int pid;
  final String pname;
  final double price;
  final String displayPrice;
  final int qty;
  final int pcs;
  final String displayQty;
  final String dateCreated;

  // constructor
  ProductVM(
      {this.id,
      this.pid,
      this.pname,
      this.price,
      this.displayPrice,
      this.qty,
      this.pcs,
      this.displayQty,
      this.dateCreated});

  @override
  List<Object> get props =>
      [id, pid, pname, price, displayPrice, qty, pcs, displayQty, dateCreated];

  static ProductVM fromProduct(Product p, {bool inCartons = true}) {
    return ProductVM(
        id: p.id,
        pid: p.pid,
        pname: p.pname,
        price: p.price,
        displayPrice: NumberFormat.simpleCurrency(name: 'NGN', decimalDigits: 2)
            .format(p.price),
        qty: p.qty,
        displayQty: inCartons ? Calc.getQuantityLeftInCartons(p.qty, p.pcs) : (p.qty > 1?p.qty.toString()+' pieces left':p.qty.toString()+' piece left'),
        pcs: p.pcs,
        dateCreated: DateFormat('MMM d, y jm', 'en_US')
            .format(DateTime.fromMillisecondsSinceEpoch(Calc.getTimeStampInMilliseconds(p.dateCreated))));
  }

  static ProductVM fromApiProduct(ApiProduct p, {bool inCartons = true}) {
    return ProductVM(
        pid: p.id,
        pname: p.pname,
        price: p.price,
        displayPrice: NumberFormat.simpleCurrency(name: 'NGN', decimalDigits: 2)
            .format(p.price),
        qty: p.qty,
        displayQty: inCartons ? Calc.getQuantityLeftInCartons(p.qty, p.pcs) : (p.qty > 1?p.qty.toString()+' pieces left':p.qty.toString()+' piece left'),
        pcs: p.pcs,
        dateCreated: DateFormat('MMM d, y jm', 'en_US')
            .format(DateTime.fromMillisecondsSinceEpoch(Calc.getTimeStampInMilliseconds(p.dateCreated))));
  }

  
}
