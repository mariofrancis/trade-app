import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:connectivity/connectivity.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:trade_app/models/api_purchase.dart';
import 'package:trade_app/models/api_sale.dart';
import 'package:trade_app/models/api_setting.dart';
import 'package:trade_app/repositories/api_repository.dart';
import 'package:trade_app/repositories/db_repository.dart';

part 'connectivity_event.dart';
part 'connectivity_state.dart';

class ConnectivityBloc extends Bloc<ConnectivityEvent, ConnectivityState> {
  final Connectivity connectivity;
  final ApiRepository apiRepo;
  final DbRepository dbRepo;

  StreamSubscription<ConnectivityResult> _connectivitySubscription;

  ConnectivityBloc(
      {@required this.connectivity,
      @required this.apiRepo,
      @required this.dbRepo})
      : assert(connectivity != null && apiRepo != null && dbRepo != null) {
    _connectivitySubscription =
        connectivity.onConnectivityChanged.listen(_connectivityListener);
    this.add(FetchConnectivityStatus());
  }
  @override
  ConnectivityState get initialState => ConnectivityInitial();

  @override
  Stream<ConnectivityState> mapEventToState(
    ConnectivityEvent event,
  ) async* {
    if (event is NotifyConnectivityChange) {
      yield* _mapNotifyEventToState(event);
    } else if (event is SyncOfflineData) {
      yield* _mapSyncDataEventToState();
    } else {
      yield* _mapFetchStatusEventToState();
    }
  }

  // =============================== private methods ================================

  Future<bool> isConnected() async {
    var connectivityResult = await connectivity.checkConnectivity();
    if (connectivityResult == ConnectivityResult.mobile ||
        connectivityResult == ConnectivityResult.wifi) {
      return true;
    } else {
      return false;
    }
  }

  void _connectivityListener(ConnectivityResult result) {
    this.add(NotifyConnectivityChange(result: result));
  }

  Stream<ConnectivityState> _mapNotifyEventToState(
      NotifyConnectivityChange event) async* {
    if (event.result == ConnectivityResult.mobile ||
        event.result == ConnectivityResult.wifi) {
      yield ConnectivityOnline();
    } else {
      yield ConnectivityOffline();
    }
  }

  Stream<ConnectivityState> _mapFetchStatusEventToState() async* {
    if (await isConnected()) {
      yield ConnectivityOnline();
    } else {
      yield ConnectivityOffline();
    }
  }

  Stream<ConnectivityState> _mapSyncDataEventToState() async* {
    try {
      // fetch offline purchases
      var purchases = await dbRepo.purchasesDataProvider.getAll();
      // fetch offline sales
      var sales = await dbRepo.salesDataProvider.getAll();

      // fetch setting
      var setting = await dbRepo.settingsDataProvider.get(1);
      await apiRepo.settingsDataProvider.update(ApiSetting(
          pswd: setting.pswd,
          sqstn: setting.sqstn,
          sans: setting.sans,
          url: setting.url));

      bool isSync = false;
      // sync data
      if (purchases.length > 0) {
        await apiRepo.purchasesDataProvider.insertBatch(
            purchases.map((p) => ApiPurchase.fromPurchase(p)).toList());
        dbRepo.purchasesDataProvider.truncate();
        isSync = true;
      }
      if (sales.length > 0) {
        await apiRepo.salesDataProvider
            .insertBatch(sales.map((s) => ApiSale.fromSale(s)).toList());
        dbRepo.salesDataProvider.truncate();
        isSync = true;
      }

      if (isSync) {
        yield OfflineDataSynced();
        yield ConnectivityOnline();
      }
    } catch (ex) {
      print(ex.toString());
      yield ConnectivityOnline(message: ex.toString());
    }
  }

  void dispose() {
    _connectivitySubscription.cancel();
  }
}
