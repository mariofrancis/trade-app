import 'package:trade_app/models/models.dart';
import 'package:trade_app/repositories/api_dataprovider/api_param.dart';
import 'dart:convert';
import 'package:meta/meta.dart';

class ProductsDataProvider {
  final ApiParam apiParam;
  ProductsDataProvider({@required this.apiParam}) : assert(apiParam != null);

  // insert
  Future<ApiProduct> insert(ApiProduct product) async {
    final url = '${apiParam.baseUrl}/products/add';
    final data = jsonEncode(product.toMap());
    final response = await apiParam.httpClient.post(url, body: data, headers: {
      apiParam.headerName: apiParam.key
    }).timeout(Duration(seconds: apiParam.timeout));
    if (response.statusCode == 500) {
      throw Exception('A server error was encountered! Please try again');
    } else {
      final jsonResponse = jsonDecode(response.body);
      if (!jsonResponse['is_success']) {
        throw Exception(jsonResponse['message']);
      } else {
        return ApiProduct.fromMap(jsonResponse['data']);
      }
    }
  }

  // get
  Future<ApiProduct> get(int id) async {
    final url = '${apiParam.baseUrl}/products/get/$id';
    final response = await apiParam.httpClient.get(url, headers: {
      apiParam.headerName: apiParam.key
    }).timeout(Duration(seconds: apiParam.timeout));
    if (response.statusCode == 500) {
      throw Exception('A server error was encountered! Please try again');
    } else {
      final jsonResponse = jsonDecode(response.body);
      if (!jsonResponse['is_success']) {
        throw Exception(jsonResponse['message']);
      } else {
        return ApiProduct.fromMap(jsonResponse['data']);
      }
    }
  }

  // get all
  Future<List<ApiProduct>> getAll() async {
    final url = '${apiParam.baseUrl}/products/get_all';
    final response = await apiParam.httpClient.get(url, headers: {
      apiParam.headerName: apiParam.key
    }).timeout(Duration(seconds: apiParam.timeout));
    if (response.statusCode == 500) {
      throw Exception('A server error was encountered! Please try again');
    } else {
      final jsonResponse = jsonDecode(response.body);
      if (!jsonResponse['is_success']) {
        throw Exception(jsonResponse['message']);
      } else {
        final jsonProducts = jsonResponse['data'] as List;
        return jsonProducts.map((e) => ApiProduct.fromMap(e)).toList();
      }
    }
  }

  // update
  Future<void> update(ApiProduct product) async {
    final url = '${apiParam.baseUrl}/products/update';
    final data = jsonEncode(product.toMap());
    final response = await apiParam.httpClient.post(url, body: data, headers: {
      apiParam.headerName: apiParam.key
    }).timeout(Duration(seconds: apiParam.timeout));
    if (response.statusCode == 500) {
      throw Exception('A server error was encountered! Please try again');
    } else {
      final jsonResponse = jsonDecode(response.body);
      if (!jsonResponse['is_success']) {
        throw Exception(jsonResponse['message']);
      }
    }
  }

  Future<void> delete(int id) async {
    final url = '${apiParam.baseUrl}/products/delete/$id';
    final response = await apiParam.httpClient.get(url, headers: {
      apiParam.headerName: apiParam.key
    }).timeout(Duration(seconds: apiParam.timeout));
    if (response.statusCode == 500) {
      throw Exception('A server error was encountered! Please try again');
    } else {
      final jsonResponse = jsonDecode(response.body);
      if (!jsonResponse['is_success']) {
        throw Exception(jsonResponse['message']);
      }
    }
  }
}
