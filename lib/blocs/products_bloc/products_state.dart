part of 'products_bloc.dart';

abstract class ProductsState extends Equatable {
  const ProductsState();

  @override
  List<Object> get props => [];
}

class ProductsEmpty extends ProductsState {}

class ProductsLoading extends ProductsState {}
class ProductsRefreshLoading extends ProductsState {}

class ProductsLoaded extends ProductsState {
  final List<ProductVM> products;

  ProductsLoaded({@required this.products}) : assert(products != null);
  @override
  List<Object> get props => [products];
}

class ProductsFetchError extends ProductsState {
  final String message;

  ProductsFetchError({@required this.message}) : assert(message != null);
  @override
  List<Object> get props => [message];
}