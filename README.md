# Trade App

This is a mobile app project built with Flutter and is used for product/goods inventory management and accurate record keeping of purchases and sales.

# Features
- Product Management
- Purchase Records Management
- Sales Records Management
- Records Backup to remote server

# Tools
- Flutter SDK
- Dart
- SQLite
- BLoC for state management