import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:trade_app/blocs/blocs.dart';
import 'package:trade_app/utilities/utilities.dart';
import 'package:trade_app/widgets/widgets.dart';

class SalesOnline extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _SaleOnlineState();
}

class _SaleOnlineState extends State<SalesOnline> {
  var _month = '';
  Completer<void> _refreshCompleter;
  ScrollController _scrollController = new ScrollController();

  @override
  void initState() {
    super.initState();
    _handleScroll();
    _refreshCompleter = Completer<void>();
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocConsumer<SalesOnlineBloc, SalesOnlineState>(
          listener: (context, state) {
            if (state is SalesOnlineLoaded) {
              _month = state.sales.month;
              _refreshCompleter?.complete();
              _refreshCompleter = Completer();
              BlocProvider.of<FloatButtonBloc>(context)
                  .add(SetButtonVisibility(isVisible: true));
            }
          },
          buildWhen: (prevState, newState) =>
              !(newState is SalesOnlineRefreshLoading),
          builder: (context, state) {
            if (state is SalesOnlineEmpty) {
              return EmptyPage(
                  icon:
                      Image.asset('assets/images/empty_record.png', width: 70),
                  message: 'No records found',
                  onRefresh: _fetchSales);
            } else if (state is SalesOnlineLoading) {
              return Center(child: CircularProgressIndicator());
            } else if (state is SalesOnlineFetchError) {
              return LoadError(
                  message: state.message, onTryAgain: _fetchSales);
            } else {
              var _state = state as SalesOnlineLoaded;
              return _buildBody(context, _state);
            }
          }),
    );
  }

  Widget _buildBody(BuildContext context, SalesOnlineLoaded state) {
    _month = state.sales.month;
    return GradientContainer(
      child: Column(
        children: <Widget>[
          Card(
            elevation: 4,
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 4),
              child: DropdownButtonFormField<String>(
                isDense: true,
                isExpanded: true,
                value: _month,
                decoration: InputDecoration(
                    isDense: true,
                    enabledBorder: InputBorder.none,
                    contentPadding: EdgeInsets.only(left:4)),
                items: state.sales.months
                    .map((m) => DropdownMenuItem(
                          child: Text(m),
                          value: m,
                        ))
                    .toList(),
                    style: Styles.getInputTextStyle(context).copyWith(
                            decorationColor: Theme.of(context)
                                .primaryColorDark
                                .withOpacity(.5)),
                onChanged: (val) {
                  BlocProvider.of<SalesOnlineBloc>(context)
                      .add(FetchSalesOnline(month: val));
                },
              ),
            ),
          ),
          Expanded(
              child: RefreshIndicator(
            onRefresh: _onRefresh,
            child: ListView.separated(
              controller: _scrollController,
              itemCount: state.sales.dayRecords.length + 1,
              itemBuilder: _buildListItem,
              separatorBuilder: _separatorBuilder,
            ),
          ))
        ],
      ),
    );
  }

  Widget _separatorBuilder(BuildContext context, int itemIndex) {
    return Padding(padding: EdgeInsets.symmetric(vertical: 4));
  }

  Widget _buildListItem(BuildContext context, int itemIndex) {
    var state = BlocProvider.of<SalesOnlineBloc>(context).state
        as SalesOnlineLoaded;
    if (itemIndex == state.sales.dayRecords.length) {
      return Card(
          elevation: 2,
          child: Row(
            children: <Widget>[
              Container(
                decoration: BoxDecoration(
                    color: Theme.of(context).primaryColor,
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(4),
                        bottomLeft: Radius.circular(4))),
                child: Padding(
                  padding: const EdgeInsets.symmetric(
                      vertical: 8.0, horizontal: 16.0),
                  child: Text(
                    'Total',
                    style: Theme.of(context)
                        .textTheme
                        .subtitle1
                        .copyWith(color: Theme.of(context).primaryColorLight, fontSize: 12),
                  ),
                ),
              ),
              Expanded(
                child: Padding(
                    padding:
                        EdgeInsets.symmetric(horizontal: 16.0, vertical: 4.0),
                    child: Text(state.sales.total,
                        style: Theme.of(context).textTheme.subtitle1.copyWith(
                            color: Theme.of(context).textTheme.headline4.color,
                            fontWeight: FontWeight.bold, fontSize: 12),
                        textAlign: TextAlign.end)),
              ),
            ],
          ));
    } else {
      return Card(
        elevation: 4,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Container(
              decoration: BoxDecoration(
                  color: Theme.of(context).primaryColor,
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(4),
                      topRight: Radius.circular(4))),
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  state.sales.dayRecords[itemIndex].day,
                  style: Theme.of(context)
                      .textTheme
                      .bodyText1
                      .copyWith(color: Theme.of(context).primaryColorLight, fontSize: 12),
                ),
              ),
            ),
            Scrollbar(
              child: SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: SalesDataTable(
                  dayRecord: state.sales.dayRecords[itemIndex],
                  isOnline: true,
                  onItemOptionSelect: _onItemOptionSelect,
                ),
              ),
            ),
          ],
        ),
      );
    }
  }

  void _fetchSales() {
    BlocProvider.of<SalesOnlineBloc>(context).add(FetchSalesOnline(
        month: BlocProvider.of<SalesOnlineBloc>(context).month ?? ''));
  }

  Future<void> _onRefresh() async {
    BlocProvider.of<SalesOnlineBloc>(context)
        .add(RefreshSalesOnline(month: _month));
    return _refreshCompleter.future;
  }

  void _handleScroll() {
    _scrollController.addListener(() {
      Debouncer(milliseconds: 500).run(() {
        if (_scrollController.position.userScrollDirection ==
            ScrollDirection.reverse) {
          BlocProvider.of<FloatButtonBloc>(context)
              .add(SetButtonVisibility(isVisible: false));
        }
        if (_scrollController.position.userScrollDirection ==
            ScrollDirection.forward) {
          BlocProvider.of<FloatButtonBloc>(context)
              .add(SetButtonVisibility(isVisible: true));
        }
      });
    });
  }

  Future<void> _onItemOptionSelect(
      BuildContext _context, String optionValue) async {
        var isAuthorized = await Dialogs.getAuthDialog(context);
    if (!isAuthorized) {
      Dialogs.alertDialog(context, 'Access denied', type: AlertType.error);
    } else {
    var arr = optionValue.split('_');
    int id = int.parse(arr[1]);

    if (arr[0] == 'Edit') {
      var state = BlocProvider.of<SalesOnlineBloc>(context).state
          as SalesOnlineLoaded;
      var saleVM = state.sales.dayRecords
          .singleWhere((d) => d.records.any((r) => r.id == id))
          .records
          .singleWhere((r) => r.id == id);
      var sale = saleVM.toApiSale().toSale();
      var product = await BlocProvider.of<SalesOnlineBloc>(context)
          .dbRepo
          .productsDataProvider
          .get(sale.pid);
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) {
          return EditSale(
            mainContext: _context,
            product: product,
            sale: sale,
            isOnline: true,
          );
        }),
      );
    } else if (arr[0] == 'Delete') {
      var connectivityState = BlocProvider.of<ConnectivityBloc>(context).state;
      if (connectivityState is ConnectivityOffline) {
        Dialogs.alertDialog(context,
            "You're currently offline! Ensure your mobile device has internet access and try again.",
            type: AlertType.info);
      } else {
        var proceed = await Dialogs.confirmDialog(
            context, 'Do you wish to delete this sale record?',
            // subtitle:
            //     'Deleting this record will also reduce the available quantity of the product associated with it.',
            buttonLabels: ['No', 'Yes']);
        if (proceed) {
          // BlocProvider.of<DeleteSaleBloc>(context)
          //     .add(DeleteSale(isOnline: true, saleId: id));
          _deleteSale(id);
        }
      }
    }
    }
  }

  void _deleteSale(int saleId) async {
    var loader =
        Dialogs.getLoaderDialog(context, initialMessage: 'Deleting record...');
          await loader.show();
    try {
      var apiRepo = BlocProvider.of<SalesOnlineBloc>(context).apiRepo;

      await apiRepo.salesDataProvider.delete(saleId);

      await loader.hide();
      _refreshViews();
      await Dialogs.alertDialog(context, 'Record was deleted successfully.',
          type: AlertType.success);
      
    } catch (ex) {
      var msg = ex is TimeoutException
          ? 'Request timed out! Ensure you have a strong network signal'
          : ex.toString();
      await loader.hide();
      await Dialogs.alertDialog(context, msg, type: AlertType.error);
    }
  }

  void _refreshViews() {
    BlocProvider.of<SalesOnlineBloc>(context).add(RefreshSalesOnline(
        month: BlocProvider.of<SalesOnlineBloc>(context).month));
    BlocProvider.of<ProductsBloc>(context)
        .add(RefreshProducts(isOnline: true, searchText: ''));
  }
}
