import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:trade_app/blocs/blocs.dart';
import 'package:trade_app/models/models.dart';
import 'package:trade_app/utilities/utilities.dart';
import 'package:trade_app/widgets/connectivity_offline_banner.dart';
import 'package:trade_app/widgets/widgets.dart';

class NewProduct extends StatefulWidget {
  final BuildContext mainContext;
  NewProduct({@required this.mainContext});
  @override
  _NewProductState createState() => _NewProductState(mainContext: mainContext);
}

class _NewProductState extends State<NewProduct> {
  final BuildContext mainContext;
  final _formKey = GlobalKey<FormState>();
  final _nameController = new TextEditingController();
  final _priceController = new TextEditingController();
  final _qtyController = new TextEditingController();
  final _pcsController = new TextEditingController();
  String _qtyUnit = 'piece';

  _NewProductState({@required this.mainContext});

  @override
  void dispose() {
    _nameController.dispose();
    _priceController.dispose();
    _qtyController.dispose();
    _pcsController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('New Product'),
      ),
      body: Column(
        children: <Widget>[
          ConnectivityOfflineBanner(),
          Expanded(
            child: GradientContainer(
                child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 4.0, vertical: 0),
              child: _buildForm(context),
            )),
          ),
        ],
      ),
    );
  }

  // get form
  Widget _buildForm(BuildContext context) {
    return Form(
        key: _formKey,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: <Widget>[
            Expanded(
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Padding(padding: EdgeInsets.only(top: 32)),
                    // product name
                    FormFieldCard(
                      child: TextFormField(
                        controller: _nameController,
                        decoration: Styles.getInputDecoration(context,
                            label: 'Name *', hint: 'What is the product name?'),
                        style: Styles.getInputTextStyle(context).copyWith(
                            decorationColor: Theme.of(context)
                                .primaryColorDark
                                .withOpacity(.5)),
                        validator: (value) {
                          if (value.trim().isEmpty) {
                            return 'Product name is required';
                          }
                          return null;
                        },
                      ),
                    ),
                    Padding(padding: EdgeInsets.symmetric(vertical: 12)),
                    // Unit price
                    FormFieldCard(
                      child: TextFormField(
                          controller: _priceController,
                          decoration: Styles.getInputDecoration(context,
                              label: 'Unit Price *',
                              hint: 'What is the price for a piece?'),
                          style: Styles.getInputTextStyle(context).copyWith(
                              decorationColor: Theme.of(context)
                                  .primaryColorDark
                                  .withOpacity(.5)),
                          keyboardType: TextInputType.number,
                          validator: (value) {
                            if (value.trim().isEmpty) {
                              return 'Unit price is required';
                            }
                            return null;
                          }),
                    ),
                    Padding(padding: EdgeInsets.symmetric(vertical: 12)),
                    Padding(
                      padding: const EdgeInsets.only(bottom: 4, left: 4),
                      child: Text(
                        'Available Quantity',
                        style: Theme.of(context).textTheme.headline4.copyWith(
                              fontSize: 12, fontWeight: FontWeight.w500
                            ),
                      ),
                    ),
                    Row(
                      children: <Widget>[
                        Expanded(
                          flex: 1,
                          child: FormFieldCard(
                            child: DropdownButtonFormField<String>(
                              value: _qtyUnit,
                              decoration: Styles.getInputDecoration(context, label: 'Qty Unit *'),
                              items: [
                                DropdownMenuItem(
                                  child: Text('Piece'),
                                  value: 'piece',
                                ),
                                DropdownMenuItem(
                                  child: Text('Carton'),
                                  value: 'carton',
                                )
                              ],
                              style: Styles.getInputTextStyle(context).copyWith(
                                  decorationColor: Theme.of(context)
                                      .primaryColorDark
                                      .withOpacity(.5)),
                              onChanged: (val) {
                                setState(() {
                                  _qtyUnit = val;
                                });
                              },
                            ),
                          ),
                        ),
                        Padding(padding: EdgeInsets.symmetric(horizontal: 4)),
                        Expanded(
                          flex: 2,
                          child: FormFieldCard(
                            child: TextFormField(
                                controller: _qtyController,
                                decoration: Styles.getInputDecoration(context,
                                    label: 'Quantity *',
                                    hint: 'Quantity in pieces/cartons'),
                                style: Styles.getInputTextStyle(context)
                                    .copyWith(
                                        decorationColor: Theme.of(context)
                                            .primaryColorDark
                                            .withOpacity(.5)),
                                keyboardType: TextInputType.numberWithOptions(decimal: false),
                                validator: (value) {
                                  if (value.trim().isEmpty) {
                                    return 'Total quantity is required';
                                  }
                                  return null;
                                }),
                          ),
                        )
                      ],
                    ),
                    Padding(padding: EdgeInsets.symmetric(vertical: 12)),
                    // Unit price
                    FormFieldCard(
                      child: TextFormField(
                          controller: _pcsController,
                          decoration: Styles.getInputDecoration(context,
                              label: 'Quantity per carton *',
                              hint: 'How many pieces are in a carton?'),
                          style: Styles.getInputTextStyle(context).copyWith(
                              decorationColor: Theme.of(context)
                                  .primaryColorDark
                                  .withOpacity(.5)),
                          keyboardType: TextInputType.numberWithOptions(decimal: false, signed: false),
                          validator: (value) {
                            if (value.trim().isEmpty) {
                              return 'Quantity per carton is required';
                            }
                            return null;
                          }),
                    ),
                    Padding(padding: EdgeInsets.symmetric(vertical: 12)),
                  ],
                ),
              ),
            ),
            Divider(
              height: 2,
            ),
            RaisedButton.icon(
              onPressed: () {
                _onSubmit(context);
              },
              icon: Icon(
                Icons.check_circle_outline,
                size: 16,
              ),
              label: Text('Create Product'),
              colorBrightness: Theme.of(context).accentColorBrightness,
            )
          ],
        ));
  }

  Future<void> _onSubmit(BuildContext _context) async {
    if (_formKey.currentState.validate()) {
      var qty = int.parse(_qtyController.text);
      var pcs = int.parse(_pcsController.text);
      var price = double.parse(_priceController.text);
      int timeStamp = int.parse(
          (DateTime.now().millisecondsSinceEpoch / 1000).round().toString());
      var product = ApiProduct(
          pname: _nameController.text,
          price: _qtyUnit == 'piece' ? price : (price / pcs),
          pcs: pcs,
          qty: _qtyUnit == 'piece' ? qty : (qty * pcs),
          dateCreated: timeStamp);

      var isNameExist = await BlocProvider.of<ProductsBloc>(mainContext)
          .dbRepo
          .productsDataProvider
          .isProductNameExist(product.pname);
      if (isNameExist) {
        Dialogs.alertDialog(
            context, "Product with name '${product.pname}' already exist.",
            type: AlertType.info);
      } else {
        var connectivityState =
            BlocProvider.of<ConnectivityBloc>(mainContext).state;
        if (connectivityState is ConnectivityOffline) {
          Dialogs.alertDialog(context,
              "You're currently offline! Ensure your mobile device has internet access and try again.",
              type: AlertType.info);
        } else {
          // BlocProvider.of<NewProductBloc>(_context)
          //     .add(AddNewProduct(product: product));
          _addNewProduct(product);
        }
      }
    }
  }

  void _addNewProduct(ApiProduct product) async {
    var loader =
        Dialogs.getLoaderDialog(context, initialMessage: 'Adding product...')
          ..show();
    try {
      var apiRepo = BlocProvider.of<ProductsBloc>(context).apiRepo;
      var dbRepo = BlocProvider.of<ProductsBloc>(context).dbRepo;
      var apiProduct = await apiRepo.productsDataProvider.insert(product);
      await dbRepo.productsDataProvider.insert(apiProduct.toProduct());
      BlocProvider.of<ProductsBloc>(context)
          .add(RefreshProducts(isOnline: false, searchText: ''));

      await loader.hide();
      _clearFields();
      await Dialogs.alertDialog(context, 'Product was added successfully.',
          type: AlertType.success);
    } catch (ex) {
      var msg = ex is TimeoutException
          ? 'Request timed out! Ensure you have a strong network signal'
          : ex.toString();
      loader.hide();
      Dialogs.alertDialog(context, msg, type: AlertType.error);
    }
  }

  void _clearFields() {
    _nameController.clear();
    _priceController.clear();
    _qtyController.clear();
    _pcsController.clear();
    // setState(() {
    //   _qtyUnit = 'piece';
    // });
  }
}
