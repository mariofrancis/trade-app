import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:trade_app/blocs/blocs.dart';
import 'package:trade_app/main.dart';
import 'package:trade_app/models/models.dart';
import 'package:trade_app/repositories/api_repository.dart';
import 'package:trade_app/repositories/db_repository.dart';
import 'package:trade_app/utilities/utilities.dart';
import 'package:trade_app/widgets/widgets.dart';

class Setup extends StatefulWidget {
  final ApiRepository apiRepo;
  final DbRepository dbRepo;

  Setup({@required this.apiRepo, @required this.dbRepo});

  @override
  _SetupState createState() => _SetupState(apiRepo: apiRepo, dbRepo: dbRepo);
}

class _SetupState extends State<Setup> {
  final ApiRepository apiRepo;
  final DbRepository dbRepo;
  final _formKey = GlobalKey<FormState>();
  final _pswdController = new TextEditingController();
  final _cpswdController = new TextEditingController();
  final _saController = new TextEditingController();
  final _urlController = new TextEditingController();
  final _keyController = new TextEditingController();
  String _sq = '';

  final questions = [
    'What is your nickname?',
    'What is your favourite color?',
    'What is the name of your best friend?',
    'What is your favourite food?',
    'In which month of the year were you born?'
  ];

  _SetupState({@required this.apiRepo, @required this.dbRepo});

  @override
  void dispose() {
    _pswdController.dispose();
    _cpswdController.dispose();
    _saController.dispose();
    _urlController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Setup'),
        centerTitle: false,
        leading: Icon(
          Icons.settings_applications,
          size: 32,
        ),
      ),
      body: Column(children: <Widget>[
        ConnectivityOfflineBanner(),
        Expanded(
          child: GradientContainer(
              child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Center(
              child: Column(
                children: <Widget>[
                  Expanded(child: _buildForm(context)),
                ],
              ),
            ),
          )),
        ),
      ]),
    );
  }

  Widget _buildForm(BuildContext context) {
    return Form(
        key: _formKey,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: <Widget>[
            Expanded(
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Padding(padding: EdgeInsets.symmetric(vertical: 8.0)),
                    Text(
                      'Welcome!',
                      style: Theme.of(context).textTheme.headline4,
                    ),
                    Padding(padding: EdgeInsets.symmetric(vertical: 4.0)),
                    Text(
                      'First, lets set things up for you.',
                      style: Theme.of(context)
                          .textTheme
                          .headline4
                          .copyWith(fontSize: 20),
                    ),
                    Padding(padding: EdgeInsets.symmetric(vertical: 16.0)),
                    //=====================================
                    // password
                    FormFieldCard(
                      child: TextFormField(
                        controller: _pswdController,
                        obscureText: true,
                        decoration: Styles.getInputDecoration(context,
                            label: 'Password *', hint: 'Enter password?'),
                        style: Styles.getInputTextStyle(context).copyWith(
                            decorationColor: Theme.of(context)
                                .primaryColorDark
                                .withOpacity(.5)),
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Password is required';
                          } else if (value.length < 8) {
                            return 'Password should be up to eight(8) characters in length';
                          }
                          return null;
                        },
                      ),
                    ),
                    Padding(padding: EdgeInsets.symmetric(vertical: 12)),
                    // confirm password
                    FormFieldCard(
                      child: TextFormField(
                          controller: _cpswdController,
                          obscureText: true,
                          decoration: Styles.getInputDecoration(context,
                              label: 'Confirm Password *',
                              hint: 'Enter password again to confirm?'),
                          style: Styles.getInputTextStyle(context).copyWith(
                              decorationColor: Theme.of(context)
                                  .primaryColorDark
                                  .withOpacity(.5)),
                          validator: (value) {
                            if (value.isEmpty) {
                              return 'Confirm password is required';
                            } else if (value != _pswdController.text) {
                              return 'Passwords do not match';
                            }
                            return null;
                          }),
                    ),
                    Padding(padding: EdgeInsets.symmetric(vertical: 12)),
                    // Security question
                    FormFieldCard(
                      child: DropdownButtonFormField<String>(
                        isExpanded: true,
                        value: _sq,
                        style: Styles.getInputTextStyle(context).copyWith(
                            decorationColor: Theme.of(context)
                                .primaryColorDark
                                .withOpacity(.5)),
                        decoration: Styles.getInputDecoration(context,
                            label: 'Security Question'),
                        items: questions
                            .map((q) => DropdownMenuItem(
                                  child: Text(q),
                                  value: q,
                                ))
                            .toList()
                              ..insert(
                                  0,
                                  DropdownMenuItem(
                                    child:
                                        Text('-- Select security question --'),
                                    value: '',
                                  )),
                        onChanged: (val) {
                          setState(() {
                            _sq = val;
                          });
                        },
                        validator: (value) =>
                            _sq == '' ? 'Security question is required' : null,
                      ),
                    ),

                    Padding(padding: EdgeInsets.symmetric(vertical: 12)),
                    // Security answer
                    FormFieldCard(
                      child: TextFormField(
                          controller: _saController,
                          decoration: Styles.getInputDecoration(context,
                              label: 'Security Answer *',
                              hint:
                                  'Enter an answer to the above selected question'),
                          style: Styles.getInputTextStyle(context).copyWith(
                              decorationColor: Theme.of(context)
                                  .primaryColorDark
                                  .withOpacity(.5)),
                          validator: (value) {
                            if (value.trim().isEmpty) {
                              return 'Security answer is required';
                            }
                            return null;
                          }),
                    ),
                    Padding(padding: EdgeInsets.symmetric(vertical: 12)),
                    FormFieldCard(
                      child: TextFormField(
                          controller: _urlController,
                          decoration: Styles.getInputDecoration(context,
                              label: 'API URL *', hint: 'Enter API URL'),
                          style: Styles.getInputTextStyle(context).copyWith(
                              decorationColor: Theme.of(context)
                                  .primaryColorDark
                                  .withOpacity(.5)),
                          validator: (value) {
                            if (value.trim().isEmpty) {
                              return 'API URL is required';
                            }
                            return null;
                          }),
                    ),
                    Padding(padding: EdgeInsets.symmetric(vertical: 12)),
                    FormFieldCard(
                      child: TextFormField(
                          controller: _keyController,
                          decoration: Styles.getInputDecoration(context,
                              label: 'API key *', hint: 'Enter API key'),
                          style: Styles.getInputTextStyle(context).copyWith(
                              decorationColor: Theme.of(context)
                                  .primaryColorDark
                                  .withOpacity(.5)),
                          validator: (value) {
                            if (value.trim().isEmpty) {
                              return 'API key is required';
                            }
                            return null;
                          }),
                    ),
                    Padding(padding: EdgeInsets.symmetric(vertical: 12)),
                  ],
                ),
              ),
            ),
            Divider(
              height: 2,
            ),
            RaisedButton.icon(
              onPressed: () {
                _onSubmit(context);
              },
              icon: Container(),
              label: Row(
                children: <Widget>[
                  Text('Get Started'),
                  Padding(padding: EdgeInsets.symmetric(horizontal: 4)),
                  Icon(
                    Icons.arrow_forward,
                    size: 16,
                  ),
                ],
              ),
              colorBrightness: Theme.of(context).accentColorBrightness,
            )
          ],
        ));
  }

  void _onSubmit(BuildContext context) async {
    if (_formKey.currentState.validate()) {
      var pswd = _pswdController.text;
      var sa = _saController.text;
      var url = _urlController.text;
      var key = _keyController.text;
      var setting = Setting(
          pswd: pswd,
          sqstn: _sq,
          sans: sa,
          url: url,
          key: key,
          displayCarton: 0,
          theme: 1);

      _addSetting(setting);
    }
  }

  void _addSetting(Setting setting) async {
    var loader =
        Dialogs.getLoaderDialog(context, initialMessage: 'Setting up...')
          ..show();
    try {
      var dbRepo = BlocProvider.of<SettingsBloc>(context).dbRepo;
      await dbRepo.settingsDataProvider.insert(setting);
      // update app
      _updateApp(setting);
      BlocProvider.of<SettingsBloc>(context).add(FetchSettings());
      await loader.hide();
      _clearFields();
      await Dialogs.alertDialog(context, 'Setup was completed successfully.',
          type: AlertType.success);
      //navigate to home
      Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(builder: (context) => Home()),
          (Route<dynamic> route) => route is MyApp);
    } catch (ex) {
      var msg = ex.toString();
      await loader.hide();
      await Dialogs.alertDialog(context, msg, type: AlertType.error);
    }
  }

  Future<void> _updateApp(Setting _setting) async {
    // update ApiParams
    BlocProvider.of<ConnectivityBloc>(context)
        .apiRepo
        .productsDataProvider
        .apiParam
        .baseUrl = _setting.url;
    BlocProvider.of<ConnectivityBloc>(context)
        .apiRepo
        .productsDataProvider
        .apiParam
        .key = _setting.key;
    BlocProvider.of<SettingsBloc>(context).setting = _setting;

    var connected =
        BlocProvider.of<ConnectivityBloc>(context).state is ConnectivityOnline;
    BlocProvider.of<SettingsBloc>(context).add(FetchSettings());
    BlocProvider.of<ThemeBloc>(context)
        .add(UpdateTheme(themeId: _setting.theme));
    BlocProvider.of<ProductsBloc>(context)
        .add(FetchProducts(isOnline: connected));
    if (connected) {
      BlocProvider.of<PurchasesOnlineBloc>(context).add(FetchPurchasesOnline(
          month: BlocProvider.of<PurchasesOnlineBloc>(context).month));
      BlocProvider.of<SalesOnlineBloc>(context).add(FetchSalesOnline(
          month: BlocProvider.of<SalesOnlineBloc>(context).month));
    } else {
      BlocProvider.of<PurchasesOfflineBloc>(context)
          .add(FetchPurchasesOffline());
      BlocProvider.of<SalesOfflineBloc>(context).add(FetchSalesOffline());
    }
  }

  void _clearFields() {
    _pswdController.clear();
    _cpswdController.clear();
    _urlController.clear();
    _saController.clear();
    _keyController.clear();
    setState(() {
      _sq = '';
    });
  }
}
