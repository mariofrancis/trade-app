part of 'home_bloc.dart';

abstract class HomeState extends Equatable {
  const HomeState();

  @override
  List<Object> get props => [];
}

class HomeInitial extends HomeState {}
class PageLoaded extends HomeState{
  final Page page;

  PageLoaded({@required this.page});

  @override
  List<Object> get props => [page];
}
