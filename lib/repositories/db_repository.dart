import 'package:trade_app/repositories/db_dataprovider/settings_dataprovider.dart';
import 'package:trade_app/repositories/db_dataprovider/products_dataprovider.dart';
import 'package:trade_app/repositories/db_dataprovider/purchases_dataprovider.dart';
import 'package:trade_app/repositories/db_dataprovider/sales_dataprovider.dart';

class DbRepository {
  SettingsDataProvider settingsDataProvider;
  ProductsDataProvider productsDataProvider;
  PurchasesDataProvider purchasesDataProvider;
  SalesDataProvider salesDataProvider;

  DbRepository() {
    settingsDataProvider = SettingsDataProvider();
    productsDataProvider = ProductsDataProvider();
    purchasesDataProvider = PurchasesDataProvider();
    salesDataProvider = SalesDataProvider();
  }
}
