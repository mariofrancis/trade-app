import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:trade_app/blocs/blocs.dart';
import 'package:trade_app/utilities/utilities.dart';
import 'package:trade_app/widgets/widgets.dart';

class Authenticate extends StatefulWidget {
  @override
  _AuthenticateState createState() => _AuthenticateState();
}

class _AuthenticateState extends State<Authenticate> {
  final pswdController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor:
          Theme.of(context).scaffoldBackgroundColor.withOpacity(.8),
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.transparent,
        automaticallyImplyLeading: false,
        actions: <Widget>[
          IconButton(
              icon: Icon(
                Icons.close,
                color: Theme.of(context).textTheme.headline4.color,
              ),
              onPressed: () {
                Navigator.pop(context, false);
              })
        ],
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.fromLTRB(16, 100, 16, 0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Icon(
                  Icons.lock,
                  size: 50,
                  color: Theme.of(context).textTheme.headline4.color,
                ),
              ),
              Padding(padding: EdgeInsets.symmetric(vertical: 8)),
              FormFieldCard(
                child: TextFormField(
                  obscureText: true,
                  controller: pswdController,
                  decoration: Styles.getInputDecoration(context,
                      label: 'Enter password *',
                      hint: 'Enter application password'),
                  style: Styles.getInputTextStyle(context).copyWith(
                      decorationColor:
                          Theme.of(context).primaryColorDark.withOpacity(.5)),
                ),
              ),
              Padding(padding: EdgeInsets.symmetric(vertical: 8)),
              RaisedButton.icon(
                onPressed: _authorize,
                icon: Icon(
                  Icons.check_circle_outline,
                  size: 18,
                ),
                label: Text('Authorize', style: GoogleFonts.openSans()),
                colorBrightness: Theme.of(context).accentColorBrightness,
              ),
            ],
          ),
        ),
      ),
    );
  }

  void _authorize() {
    var pswd = pswdController.text;
    var _setting = BlocProvider.of<SettingsBloc>(context).setting;
    if (_setting.pswd != pswd) {
      Navigator.pop(context, false);
    } else {
      Navigator.pop(context, true);
    }
  }
}
