part of 'sales_offline_bloc.dart';

abstract class SalesOfflineState extends Equatable {
  const SalesOfflineState();

  @override
  List<Object> get props => [];
}

class SalesOfflineEmpty extends SalesOfflineState {}

class SalesOfflineLoading extends SalesOfflineState {}

class SalesOfflineRefreshLoading extends SalesOfflineState {}

class SalesOfflineLoaded extends SalesOfflineState {
  final List<Record> sales;

  SalesOfflineLoaded({@required this.sales}) : assert(sales != null);
  @override
  List<Object> get props => [sales];
}

class SalesOfflineFetchError extends SalesOfflineState {
  final String message;

  SalesOfflineFetchError({@required this.message}) : assert(message != null);
  @override
  List<Object> get props => [message];
}
