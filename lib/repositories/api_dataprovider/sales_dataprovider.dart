import 'package:trade_app/models/models.dart';
import 'package:trade_app/repositories/api_dataprovider/api_param.dart';
import 'dart:convert';
import 'package:meta/meta.dart';

class SalesDataProvider {
  final ApiParam apiParam;
  SalesDataProvider({@required this.apiParam}) : assert(apiParam != null);

  // insert
  Future<ApiSale> insert(ApiSale sale) async {
    final url = '${apiParam.baseUrl}/sales/add';
    final data = jsonEncode(sale.toMap());
    final response = await apiParam.httpClient.post(url, body: data, headers: {
      apiParam.headerName: apiParam.key
    }).timeout(Duration(seconds: apiParam.timeout));
    if (response.statusCode == 500) {
      throw Exception('A server error was encountered! Please try again');
    } else {
      final jsonResponse = jsonDecode(response.body);
      if (!jsonResponse['is_success']) {
        throw Exception(jsonResponse['message']);
      } else {
        return ApiSale.fromMap(jsonResponse['data']);
      }
    }
  }

  // insert batch
  Future<void> insertBatch(List<ApiSale> sales) async {
    final url = '${apiParam.baseUrl}/sales/add_batch';
    final data = jsonEncode(sales.map((s) => s.toMap()).toList());
    final response = await apiParam.httpClient.post(url, body: data, headers: {
      apiParam.headerName: apiParam.key
    }).timeout(Duration(seconds: apiParam.timeout));
    if (response.statusCode == 500) {
      throw Exception('A server error was encountered! Please try again');
    } else {
      final jsonResponse = jsonDecode(response.body);
      if (!jsonResponse['is_success']) {
        throw Exception(jsonResponse['message']);
      } 
    }
  }

  // get
  Future<ApiSale> get(int id) async {
    final url = '${apiParam.baseUrl}/sales/get/$id';
    final response = await apiParam.httpClient.get(url, headers: {
      apiParam.headerName: apiParam.key
    }).timeout(Duration(seconds: apiParam.timeout));
    if (response.statusCode == 500) {
      throw Exception('A server error was encountered! Please try again');
    } else {
      final jsonResponse = jsonDecode(response.body);
      if (!jsonResponse['is_success']) {
        throw Exception(jsonResponse['message']);
      } else {
        return ApiSale.fromMap(jsonResponse['data']);
      }
    }
  }

  // get all
  Future<SaleVM> getRecords({String month = '', bool inCartons=true}) async {
    final url = '${apiParam.baseUrl}/sales/get_records/' +
        (month == '' ? '' : Uri.encodeComponent(month));
    final response = await apiParam.httpClient.get(url, headers: {
      apiParam.headerName: apiParam.key
    }).timeout(Duration(seconds: apiParam.timeout));
    if (response.statusCode == 500) {
      throw Exception('A server error was encountered! Please try again');
    } else {
      final jsonResponse = jsonDecode(response.body);
      if (!jsonResponse['is_success']) {
        throw Exception(jsonResponse['message']);
      } else {
        return SaleVM.fromJson(jsonResponse['data'], inCartons: inCartons);
      }
    }
  }

  // update
  Future<void> update(ApiSale sale) async {
    final url = '${apiParam.baseUrl}/sales/update';
    final data = jsonEncode(sale.toMap());
    final response = await apiParam.httpClient.post(url, body: data, headers: {
      apiParam.headerName: apiParam.key
    }).timeout(Duration(seconds: apiParam.timeout));
    if (response.statusCode == 500) {
      throw Exception('A server error was encountered! Please try again');
    } else {
      final jsonResponse = jsonDecode(response.body);
      if (!jsonResponse['is_success']) {
        throw Exception(jsonResponse['message']);
      }
    }
  }

  Future<void> delete(int id) async {
    final url = '${apiParam.baseUrl}/sales/delete/$id';
    final response = await apiParam.httpClient.get(url, headers: {
      apiParam.headerName: apiParam.key
    }).timeout(Duration(seconds: apiParam.timeout));
    if (response.statusCode == 500) {
      throw Exception('A server error was encountered! Please try again');
    } else {
      final jsonResponse = jsonDecode(response.body);
      if (!jsonResponse['is_success']) {
        throw Exception(jsonResponse['message']);
      }
    }
  }
}
