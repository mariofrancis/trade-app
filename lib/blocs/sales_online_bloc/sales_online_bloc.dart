import 'dart:async';
import 'dart:io';
import 'package:meta/meta.dart';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:trade_app/blocs/blocs.dart';
import 'package:trade_app/models/models.dart';
import 'package:trade_app/repositories/repositories.dart';

part 'sales_online_event.dart';
part 'sales_online_state.dart';

class SalesOnlineBloc
    extends Bloc<SalesOnlineEvent, SalesOnlineState> {
  final ApiRepository apiRepo;
  final DbRepository dbRepo;
  final ProductsBloc productsBloc;
  final SettingsBloc settingsBloc;
  String month='';

  SalesOnlineBloc(
      {@required this.apiRepo,
      @required this.dbRepo,
      @required this.productsBloc,
      @required this.settingsBloc})
      : assert(apiRepo != null && dbRepo != null && productsBloc != null && settingsBloc!=null);

  @override
  SalesOnlineState get initialState => SalesOnlineEmpty();

  @override
  Stream<SalesOnlineState> mapEventToState(
    SalesOnlineEvent event,
  ) async* {
    if (event is FetchSalesOnline) {
      yield* _fetchSales(event);
    } else if (event is RefreshSalesOnline) {
      yield* _refreshSales(event);
    }
  }

// =============== private methods ==========================
  Stream<SalesOnlineState> _fetchSales(
      FetchSalesOnline event) async* {
    yield SalesOnlineLoading();
    try {
      final sales =
          await apiRepo.salesDataProvider.getRecords(month: event.month, inCartons: settingsBloc.setting.displayCarton==1?true:false);
      if (sales.dayRecords.isEmpty) {
        month='';
        yield SalesOnlineEmpty();
      } else {
        month = event.month;
        yield SalesOnlineLoaded(sales: sales);
      }
    } catch (ex) {
      if (ex is TimeoutException) {
        yield SalesOnlineFetchError(
            message:
                'Request timed out! Ensure you have a strong network signal');
      } else if (ex is SocketException) {
        yield SalesOnlineFetchError(
            message:
                'Unable to connect to the application server! Ensure you have entered the correct url in settings and also ensure you have a strong network signal');
      } else {
        yield SalesOnlineFetchError(message: ex.toString());
      }
    }
  }

  Stream<SalesOnlineState> _refreshSales(
      RefreshSalesOnline event) async* {
    yield SalesOnlineRefreshLoading();
    try {
      final sales =
          await apiRepo.salesDataProvider.getRecords(month: event.month, inCartons: settingsBloc.setting.displayCarton==1?true:false);
      if (sales.dayRecords.isEmpty) {
        month='';
        yield SalesOnlineEmpty();
      } else {
        month = event.month;
        yield SalesOnlineLoaded(sales: sales);
      }
    } catch (ex) {
      if (ex is TimeoutException) {
        yield SalesOnlineFetchError(
            message:
                'Request timed out! Ensure you have a strong network signal');
      } else if (ex is SocketException) {
        yield SalesOnlineFetchError(
            message:
                'Unable to connect to the application server! Ensure you have entered the correct url in settings and also ensure you have a strong network signal');
      } else {
        yield SalesOnlineFetchError(message: ex.toString());
      }
    }
  }
}
