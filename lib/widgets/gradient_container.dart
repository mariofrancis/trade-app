import 'package:flutter/material.dart';

class GradientContainer extends StatelessWidget {
  final Widget child;

  GradientContainer({@required this.child}) : assert(child != null);

  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: BoxDecoration(
            gradient: LinearGradient(
          begin: Alignment.bottomCenter,
          end: Alignment.topCenter,
          stops: [0.2, 0.4, 0.6, 0.8, 1.0],
          colors: [
            Theme.of(context).primaryColor.withOpacity(0.4),
            Theme.of(context).primaryColor.withOpacity(0.3),
            Theme.of(context).primaryColor.withOpacity(0.2),
            Theme.of(context).primaryColor.withOpacity(0.1),
            Theme.of(context).primaryColor.withOpacity(0.0)
          ],
        )),
        padding: EdgeInsets.fromLTRB(8, 0, 8, 0),
        child: child);
  }
}
