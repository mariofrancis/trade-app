part of 'theme_bloc.dart';

abstract class ThemeState extends Equatable {
  const ThemeState();

   @override
  List<Object> get props => [];
}

class ThemeInitial extends ThemeState {}
class ThemeUpdated extends ThemeState{
  final int themeId;

  ThemeUpdated({@required this.themeId}):assert(themeId!=null);

  @override
  List<Object> get props => [themeId];
}
