import 'dart:async';
import 'package:meta/meta.dart';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:trade_app/blocs/blocs.dart';
import 'package:trade_app/models/models.dart';
import 'package:trade_app/repositories/repositories.dart';

part 'sales_offline_event.dart';
part 'sales_offline_state.dart';

class SalesOfflineBloc extends Bloc<SalesOfflineEvent, SalesOfflineState> {
  final ApiRepository apiRepo;
  final DbRepository dbRepo;
  final ProductsBloc productsBloc;
  final SettingsBloc settingsBloc;

  SalesOfflineBloc(
      {@required this.apiRepo,
      @required this.dbRepo,
      @required this.productsBloc,
      @required this.settingsBloc})
      : assert(apiRepo != null && dbRepo != null && productsBloc != null && settingsBloc!=null);

  @override
  SalesOfflineState get initialState => SalesOfflineEmpty();

  @override
  Stream<SalesOfflineState> mapEventToState(
    SalesOfflineEvent event,
  ) async* {
    if (event is FetchSalesOffline) {
      yield* _fetchSales(event);
    } else if (event is RefreshSalesOffline) {
      yield* _refreshSales(event);
    }
  }

// =============== private methods ==========================
  Stream<SalesOfflineState> _fetchSales(FetchSalesOffline event) async* {
    yield SalesOfflineLoading();
    try {
      final sales = await dbRepo.salesDataProvider.getAll();
      if (sales.length == 0) {
        yield SalesOfflineEmpty();
      } else {
        var records = List<Record>();

        for (int i = 0; i < sales.length; i++) {
          var r = await Record.fromSale(sales[i], dbRepo, inCartons: settingsBloc.setting.displayCarton==1?true:false);
          records.add(r);
        }

        yield SalesOfflineLoaded(sales: records);
      }
    } catch (ex) {
      yield SalesOfflineFetchError(message: ex.toString());
    }
  }

  Stream<SalesOfflineState> _refreshSales(RefreshSalesOffline event) async* {
    yield SalesOfflineRefreshLoading();
    try {
      final sales = await dbRepo.salesDataProvider.getAll();
      if (sales.length == 0) {
        yield SalesOfflineEmpty();
      } else {
        var records = List<Record>();

        for (int i = 0; i < sales.length; i++) {
          var r = await Record.fromSale(sales[i], dbRepo, inCartons: settingsBloc.setting.displayCarton==1?true:false);
          records.add(r);
        }

        yield SalesOfflineLoaded(sales: records);
      }
    } catch (ex) {
      yield SalesOfflineFetchError(message: ex.toString());
    }
  }
}
