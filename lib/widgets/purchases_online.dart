import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:trade_app/blocs/blocs.dart';
import 'package:trade_app/utilities/utilities.dart';
import 'package:trade_app/widgets/widgets.dart';

class PurchasesOnline extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _PurchaseOnlineState();
}

class _PurchaseOnlineState extends State<PurchasesOnline> {
  var _month = '';
  Completer<void> _refreshCompleter;
  ScrollController _scrollController = new ScrollController();

  @override
  void initState() {
    super.initState();
    _handleScroll();
    _refreshCompleter = Completer<void>();
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocConsumer<PurchasesOnlineBloc, PurchasesOnlineState>(
          listener: (context, state) {
            if (state is PurchasesOnlineLoaded) {
              _month = state.purchases.month;
              _refreshCompleter?.complete();
              _refreshCompleter = Completer();
              BlocProvider.of<FloatButtonBloc>(context)
                  .add(SetButtonVisibility(isVisible: true));
            }
          },
          buildWhen: (prevState, newState) =>
              !(newState is PurchasesOnlineRefreshLoading),
          builder: (context, state) {
            if (state is PurchasesOnlineEmpty) {
              return EmptyPage(
                  icon:
                      Image.asset('assets/images/empty_record.png', width: 70),
                  message: 'No records found',
                  onRefresh: _fetchPurchases);
            } else if (state is PurchasesOnlineLoading) {
              return Center(child: CircularProgressIndicator());
            } else if (state is PurchasesOnlineFetchError) {
              return LoadError(
                  message: state.message, onTryAgain: _fetchPurchases);
            } else {
              var _state = state as PurchasesOnlineLoaded;
              return _buildBody(context, _state);
            }
          }),
    );
  }

  Widget _buildBody(BuildContext context, PurchasesOnlineLoaded state) {
    _month = state.purchases.month;
    return GradientContainer(
      child: Column(
        children: <Widget>[
          Card(
            elevation: 4,
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 4),
              child: DropdownButtonFormField<String>(
                isDense: true,
                isExpanded: true,
                value: _month,
                decoration: InputDecoration(
                    isDense: true,
                    enabledBorder: InputBorder.none,
                    contentPadding: EdgeInsets.only(left: 4)),
                items: state.purchases.months
                    .map((m) => DropdownMenuItem(
                          child: Text(m),
                          value: m,
                        ))
                    .toList(),
                style: Styles.getInputTextStyle(context).copyWith(
                    decorationColor:
                        Theme.of(context).primaryColorDark.withOpacity(.5)),
                onChanged: (val) {
                  BlocProvider.of<PurchasesOnlineBloc>(context)
                      .add(FetchPurchasesOnline(month: val));
                },
              ),
            ),
          ),
          Expanded(
              child: RefreshIndicator(
            onRefresh: _onRefresh,
            child: ListView.separated(
              controller: _scrollController,
              itemCount: state.purchases.dayRecords.length + 1,
              itemBuilder: _buildListItem,
              separatorBuilder: _separatorBuilder,
            ),
          ))
        ],
      ),
    );
  }

  Widget _separatorBuilder(BuildContext context, int itemIndex) {
    return Padding(padding: EdgeInsets.symmetric(vertical: 4));
  }

  Widget _buildListItem(BuildContext context, int itemIndex) {
    var state = BlocProvider.of<PurchasesOnlineBloc>(context).state
        as PurchasesOnlineLoaded;
    if (itemIndex == state.purchases.dayRecords.length) {
      return Card(
          elevation: 2,
          child: Row(
            children: <Widget>[
              Container(
                decoration: BoxDecoration(
                    color: Theme.of(context).primaryColor,
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(4),
                        bottomLeft: Radius.circular(4))),
                child: Padding(
                  padding: const EdgeInsets.symmetric(
                      vertical: 8.0, horizontal: 16.0),
                  child: Text(
                    'Total',
                    style: Theme.of(context).textTheme.subtitle1.copyWith(
                        color: Theme.of(context).primaryColorLight,
                        fontSize: 12),
                  ),
                ),
              ),
              Expanded(
                child: Padding(
                    padding:
                        EdgeInsets.symmetric(horizontal: 16.0, vertical: 4.0),
                    child: Text(state.purchases.total,
                        style: Theme.of(context).textTheme.subtitle1.copyWith(
                            color: Theme.of(context).textTheme.headline4.color,
                            fontWeight: FontWeight.bold,
                            fontSize: 12),
                        textAlign: TextAlign.end)),
              ),
            ],
          ));
    } else {
      return Card(
        elevation: 4,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Container(
              decoration: BoxDecoration(
                  color: Theme.of(context).primaryColor,
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(4),
                      topRight: Radius.circular(4))),
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  state.purchases.dayRecords[itemIndex].day,
                  style: Theme.of(context).textTheme.bodyText1.copyWith(
                      color: Theme.of(context).primaryColorLight, fontSize: 12),
                ),
              ),
            ),
            Scrollbar(
              child: SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: PurchasesDataTable(
                  dayRecord: state.purchases.dayRecords[itemIndex],
                  isOnline: true,
                  onItemOptionSelect: _onItemOptionSelect,
                ),
              ),
            ),
          ],
        ),
      );
    }
  }

  void _fetchPurchases() {
    BlocProvider.of<PurchasesOnlineBloc>(context).add(FetchPurchasesOnline(
        month: BlocProvider.of<PurchasesOnlineBloc>(context).month ?? ''));
  }

  Future<void> _onRefresh() async {
    BlocProvider.of<PurchasesOnlineBloc>(context)
        .add(RefreshPurchasesOnline(month: _month));
    return _refreshCompleter.future;
  }

  void _handleScroll() {
    _scrollController.addListener(() {
      Debouncer(milliseconds: 500).run(() {
        if (_scrollController.position.userScrollDirection ==
            ScrollDirection.reverse) {
          BlocProvider.of<FloatButtonBloc>(context)
              .add(SetButtonVisibility(isVisible: false));
        }
        if (_scrollController.position.userScrollDirection ==
            ScrollDirection.forward) {
          BlocProvider.of<FloatButtonBloc>(context)
              .add(SetButtonVisibility(isVisible: true));
        }
      });
    });
  }

  Future<void> _onItemOptionSelect(
      BuildContext _context, String optionValue) async {
    var isAuthorized = await Dialogs.getAuthDialog(context);
    if (!isAuthorized) {
      Dialogs.alertDialog(context, 'Access denied', type: AlertType.error);
    } else {
      var arr = optionValue.split('_');
      int id = int.parse(arr[1]);

      if (arr[0] == 'Edit') {
        var state = BlocProvider.of<PurchasesOnlineBloc>(context).state
            as PurchasesOnlineLoaded;
        var purchaseVM = state.purchases.dayRecords
            .singleWhere((d) => d.records.any((r) => r.id == id))
            .records
            .singleWhere((r) => r.id == id);
        var purchase = purchaseVM.toApiPurchase().toPurchase();
        var product = await BlocProvider.of<PurchasesOnlineBloc>(context)
            .dbRepo
            .productsDataProvider
            .get(purchase.pid);
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) {
            return EditPurchase(
              mainContext: _context,
              product: product,
              purchase: purchase,
              isOnline: true,
            );
          }),
        );
      } else if (arr[0] == 'Delete') {
        var connectivityState =
            BlocProvider.of<ConnectivityBloc>(context).state;
        if (connectivityState is ConnectivityOffline) {
          Dialogs.alertDialog(context,
              "You're currently offline! Ensure your mobile device has internet access and try again.",
              type: AlertType.info);
        } else {
          var proceed = await Dialogs.confirmDialog(
              context, 'Do you wish to continue?',
              subtitle:
                  'Deleting this record will also reduce the available quantity of the product associated with it.',
              buttonLabels: ['No', 'Yes']);
          if (proceed) {
            // BlocProvider.of<DeletePurchaseBloc>(context)
            //     .add(DeletePurchase(isOnline: true, purchaseId: id));
            _deletePurchase(id);
          }
        }
      }
    }
  }

  void _deletePurchase(int purchaseId) async {
    var loader =
        Dialogs.getLoaderDialog(context, initialMessage: 'Deleting record...');
    await loader.show();
    try {
      var apiRepo = BlocProvider.of<PurchasesOnlineBloc>(context).apiRepo;

      await apiRepo.purchasesDataProvider.delete(purchaseId);

      await loader.hide();
      _refreshViews();
      await Dialogs.alertDialog(context, 'Record was deleted successfully.',
          type: AlertType.success);
    } catch (ex) {
      var msg = ex is TimeoutException
          ? 'Request timed out! Ensure you have a strong network signal'
          : ex.toString();
      await loader.hide();
      await Dialogs.alertDialog(context, msg, type: AlertType.error);
    }
  }

  void _refreshViews() {
    BlocProvider.of<PurchasesOnlineBloc>(context).add(RefreshPurchasesOnline(
        month: BlocProvider.of<PurchasesOnlineBloc>(context).month));
    BlocProvider.of<ProductsBloc>(context)
        .add(RefreshProducts(isOnline: true, searchText: ''));
  }
}
