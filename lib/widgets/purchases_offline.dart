import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:trade_app/models/models.dart';
import 'package:trade_app/utilities/utilities.dart';
import 'package:trade_app/widgets/widgets.dart';
import 'package:trade_app/blocs/blocs.dart';

class PurchasesOffline extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _PurchasesOfflineState();
}

class _PurchasesOfflineState extends State<PurchasesOffline> {
  Completer<void> _refreshCompleter;
  ScrollController _scrollController = new ScrollController();

  @override
  void initState() {
    super.initState();
    _handleScroll();
    _refreshCompleter = Completer<void>();
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocConsumer<PurchasesOfflineBloc, PurchasesOfflineState>(
          listener: (context, state) {
            if (state is PurchasesOfflineLoaded) {
              _refreshCompleter?.complete();
              _refreshCompleter = Completer();
              BlocProvider.of<FloatButtonBloc>(context)
                  .add(SetButtonVisibility(isVisible: true));
            }
          },
          buildWhen: (prevState, newState) =>
              !(newState is PurchasesOfflineRefreshLoading),
          builder: (context, state) {
            if (state is PurchasesOfflineEmpty) {
              return EmptyPage(
                  icon:
                      Image.asset('assets/images/empty_record.png', width: 70),
                  message: 'No offline records found',
                  onRefresh: _fetchPurchases);
            } else if (state is PurchasesOfflineLoading) {
              return Center(child: CircularProgressIndicator());
            } else if (state is PurchasesOfflineFetchError) {
              return LoadError(
                  message: state.message, onTryAgain: _fetchPurchases);
            } else {
              var _state = state as PurchasesOfflineLoaded;
              return _buildBody(context, _state);
            }
          }),
    );
  }

  Widget _buildBody(BuildContext context, PurchasesOfflineLoaded state) {
    var dayRecord = DayRecord(day: '', records: state.purchases, subTotal: '');
    return GradientContainer(
      child: Column(
        children: <Widget>[
          Expanded(
              child: RefreshIndicator(
            onRefresh: _onRefresh,
            child: ListView(
              controller: _scrollController,
              children: <Widget>[
                Card(
                  elevation: 4,
                  child: Padding(
                    padding:
                        const EdgeInsets.symmetric(horizontal: 8, vertical: 4),
                    child: Scrollbar(
                      child: SingleChildScrollView(
                        scrollDirection: Axis.horizontal,
                        child: PurchasesDataTable(
                          dayRecord: dayRecord,
                          isOnline: false,
                          onItemOptionSelect: _onItemOptionSelect,
                        ),
                      ),
                    ),
                  ),
                )
              ],
            ),
          ))
        ],
      ),
    );
  }

  void _fetchPurchases() {
    BlocProvider.of<PurchasesOfflineBloc>(context).add(FetchPurchasesOffline());
  }

  Future<void> _onRefresh() async {
    BlocProvider.of<PurchasesOfflineBloc>(context)
        .add(RefreshPurchasesOffline());
    return _refreshCompleter.future;
  }

  void _handleScroll() {
    _scrollController.addListener(() {
      Debouncer(milliseconds: 500).run(() {
        if (_scrollController.position.userScrollDirection ==
            ScrollDirection.reverse) {
          BlocProvider.of<FloatButtonBloc>(context)
              .add(SetButtonVisibility(isVisible: false));
        }
        if (_scrollController.position.userScrollDirection ==
            ScrollDirection.forward) {
          BlocProvider.of<FloatButtonBloc>(context)
              .add(SetButtonVisibility(isVisible: true));
        }
      });
    });
  }

  Future<void> _onItemOptionSelect(
      BuildContext _context, String optionValue) async {
    var isAuthorized = await Dialogs.getAuthDialog(context);
    if (!isAuthorized) {
      Dialogs.alertDialog(context, 'Access denied', type: AlertType.error);
    } else {
      var arr = optionValue.split('_');
      int id = int.parse(arr[1]);

      if (arr[0] == 'Edit') {
        var state = BlocProvider.of<PurchasesOfflineBloc>(context).state
            as PurchasesOfflineLoaded;
        var record = state.purchases.singleWhere((r) => r.id == id);
        var purchase = record.toApiPurchase().toPurchase();
        var day = DateFormat('MMMM d, y', 'en_US').format(
            DateTime.fromMillisecondsSinceEpoch(
                Calc.getTimeStampInMilliseconds(purchase.dateCreated)));
        var month = DateFormat('MMMM, y', 'en_US').format(
            DateTime.fromMillisecondsSinceEpoch(
                Calc.getTimeStampInMilliseconds(purchase.dateCreated)));
        purchase = purchase.copyWith(day: day, month: month);

        var product = await BlocProvider.of<PurchasesOfflineBloc>(context)
            .dbRepo
            .productsDataProvider
            .get(purchase.pid);
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) {
            return EditPurchase(
              mainContext: _context,
              product: product,
              purchase: purchase,
              isOnline: false,
            );
          }),
        );
      } else if (arr[0] == 'Delete') {
        var proceed = await Dialogs.confirmDialog(
            context, 'Do you wish to continue?',
            subtitle:
                'Deleting this record will also reduce the available quantity of the product associated with it.',
            buttonLabels: ['No', 'Yes']);
        if (proceed) {
          // BlocProvider.of<DeletePurchaseBloc>(context)
          //     .add(DeletePurchase(isOnline: false, purchaseId: id));
          _deletePurchase(id);
        }
      }
    }
  }

  void _deletePurchase(int purchaseId) async {
    var loader =
        Dialogs.getLoaderDialog(context, initialMessage: 'Deleting record...');
    await loader.show();

    try {
      var dbRepo = BlocProvider.of<PurchasesOfflineBloc>(context).dbRepo;
      var prec = await dbRepo.purchasesDataProvider.get(purchaseId);
      if (prec == null) {
        await loader.hide();
        await Dialogs.alertDialog(context,
            'Specified offline record cannot be found! This record might have been sent to server. Kindly check under online records.',
            type: AlertType.error);
      } else {
        var product = await dbRepo.productsDataProvider.get(prec.pid);
        await dbRepo.purchasesDataProvider.delete(purchaseId);
        product = product.copyWith(qty: product.qty - prec.qty);
        await dbRepo.productsDataProvider.update(product);

        await loader.hide();
        _refreshViews();
        await Dialogs.alertDialog(context, 'Record was deleted successfully.',
            type: AlertType.success);
      }
    } catch (ex) {
      var msg = ex is TimeoutException
          ? 'Request timed out! Ensure you have a strong network signal'
          : ex.toString();

      await loader.hide();
      await Dialogs.alertDialog(context, msg, type: AlertType.error);
    }
  }

  void _refreshViews() {
    BlocProvider.of<PurchasesOfflineBloc>(context)
        .add(RefreshPurchasesOffline());
    var connected =
        BlocProvider.of<ConnectivityBloc>(context).state is ConnectivityOnline;
    BlocProvider.of<ProductsBloc>(context)
        .add(RefreshProducts(isOnline: connected, searchText: ''));
  }
}
