part of 'home_bloc.dart';

enum Page{
  Products,
  Purchases,
  Sales,
  Sync,
  Settings
}
abstract class HomeEvent extends Equatable {
  const HomeEvent();
}

class LoadPage extends HomeEvent{
  final Page page;

  LoadPage({@required this.page});

  @override
  List<Object> get props => [page];
}