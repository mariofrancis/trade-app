import 'package:sqflite/sqflite.dart';
import 'package:trade_app/models/models.dart';
import 'package:trade_app/repositories/db_dataprovider/app_database.dart';

class PurchasesDataProvider {
  // insert
  Future<int> insert(Purchase purchase) async {
    final db = await AppDatabase.instance.database;
    return db.insert(Purchase.table, purchase.toMap());
  }

  // get
  Future<Purchase> get(int id) async {
    final db = await AppDatabase.instance.database;
    List<Map> maps = await db
        .query(Purchase.table, where: '${Purchase.colId} = ?', whereArgs: [id]);
    if (maps.length > 0) {
      return Purchase.fromMap(maps.first);
    }
    return null;
  }

  // get all
  Future<List<Purchase>> getAll() async {
    final db = await AppDatabase.instance.database;
    List<Map> maps =
        await db.query(Purchase.table, orderBy: '${Purchase.colId} DESC');
    if (maps.length > 0) {
      return maps.map((row) => Purchase.fromMap(row)).toList();
    }
    return List<Purchase>();
  }

  // update
  Future<int> update(Purchase purchase) async {
    final db = await AppDatabase.instance.database;
    return db.update(
      Purchase.table,
      purchase.toMap(),
      where: '${Purchase.colId} = ?',
      whereArgs: [purchase.id],
    );
  }

  Future<int> delete(int id) async {
    final db = await AppDatabase.instance.database;
    return db
        .delete(Purchase.table, where: '${Purchase.colId} = ?', whereArgs: [id]);
  }

  Future<void> truncate() async {
    final db = await AppDatabase.instance.database;
    //return db.delete(Product.table, where: '1');
    Batch batch = db.batch();
    batch.execute('''
              DELETE FROM ${Purchase.table}
              ''');
    batch.execute('''
              UPDATE SQLITE_SEQUENCE SET SEQ=0 WHERE NAME='${Purchase.table}'
              ''');
    await batch.commit(noResult: true);
  }
}
