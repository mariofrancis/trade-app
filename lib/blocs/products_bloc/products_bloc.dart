import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:equatable/equatable.dart';
import 'package:trade_app/blocs/blocs.dart';
import 'package:trade_app/models/models.dart';
import 'package:trade_app/repositories/api_repository.dart';
import 'package:trade_app/repositories/db_repository.dart';

part 'products_event.dart';
part 'products_state.dart';

class ProductsBloc extends Bloc<ProductsEvent, ProductsState> {
  final ApiRepository apiRepo;
  final DbRepository dbRepo;
  final SettingsBloc settingsBloc;

  ProductsBloc(
      {@required this.apiRepo,
      @required this.dbRepo,
      @required this.settingsBloc});

  @override
  ProductsState get initialState => ProductsEmpty();

  @override
  Stream<ProductsState> mapEventToState(
    ProductsEvent event,
  ) async* {
    if (event is FetchProducts) {
      yield* _fetchProducts(event);
    } else if (event is SearchProducts) {
      yield* _searchProducts(event);
    } else if (event is RefreshProducts) {
      yield* _refreshProducts(event);
    }
  }

  // =============== private methods ==========================
  Stream<ProductsState> _fetchProducts(FetchProducts event) async* {
    yield ProductsLoading();
    try {
      if (event.isOnline) {
        final products =
            await apiRepo.productsDataProvider.getAll(); // get all from api
        await dbRepo.productsDataProvider.truncate(); // clear local products
        dbRepo.productsDataProvider.insertBatch(
            products.map((p) => p.toProduct()).toList()); // insert into local
        if (products.isEmpty) {
          yield ProductsEmpty();
        } else {
          var _products = products
              .map((p) => ProductVM.fromApiProduct(p,
                  inCartons:
                      settingsBloc.setting.displayCarton == 1 ? true : false))
              .toList();
          _products.sort((a, b) => a.pname.compareTo(b.pname));
          yield ProductsLoaded(products: _products);
        }
      } else {
        final products = await dbRepo.productsDataProvider.getAll();
        if (products.isEmpty) {
          yield ProductsEmpty();
        } else {
          var _products = products
              .map((p) => ProductVM.fromProduct(p,
                  inCartons:
                      settingsBloc.setting.displayCarton == 1 ? true : false))
              .toList();
          _products.sort((a, b) => a.pname.compareTo(b.pname));
          yield ProductsLoaded(products: _products);
        }
      }
    } catch (ex) {
      if (ex is TimeoutException) {
        yield ProductsFetchError(
            message:
                'Request timed out! Ensure you have a strong network signal');
      } else {
        yield ProductsFetchError(message: ex.toString());
      }
    }
  }

  Stream<ProductsState> _searchProducts(SearchProducts event) async* {
    yield ProductsLoading();
    try {
      List<Product> products;
      if (event.searchText.trim() == '') {
        products = await dbRepo.productsDataProvider.getAll();
      } else {
        products = await dbRepo.productsDataProvider.search(event.searchText);
      }
      if (products.isEmpty) {
        yield ProductsEmpty();
      } else {
        var _products = products
            .map((p) => ProductVM.fromProduct(p,
                inCartons:
                    settingsBloc.setting.displayCarton == 1 ? true : false))
            .toList();
        _products.sort((a, b) => a.pname.compareTo(b.pname));
        yield ProductsLoaded(products: _products);
      }
    } catch (ex) {
      yield ProductsFetchError(message: ex.toString());
    }
  }

  Stream<ProductsState> _refreshProducts(RefreshProducts event) async* {
    yield ProductsRefreshLoading();
    try {
      if (event.isOnline) {
        final products =
            await apiRepo.productsDataProvider.getAll(); // get all from api
        await dbRepo.productsDataProvider.truncate(); // clear local products
        dbRepo.productsDataProvider.insertBatch(
            products.map((p) => p.toProduct()).toList()); // insert into local
        if (products.isEmpty) {
          yield ProductsEmpty();
        } else {
          var _products = products.where((p) =>
              p.pname.toLowerCase().contains(event.searchText.toLowerCase()));
          var __products = _products
              .map((p) => ProductVM.fromApiProduct(p,
                  inCartons:
                      settingsBloc.setting.displayCarton == 1 ? true : false))
              .toList();
          __products.sort((a, b) => a.pname.compareTo(b.pname));
          yield ProductsLoaded(products: __products);
        }
      } else {
        final products = await dbRepo.productsDataProvider.getAll();
        if (products.isEmpty) {
          yield ProductsEmpty();
        } else {
          var _products = products.where((p) =>
              p.pname.toLowerCase().contains(event.searchText.toLowerCase()));
          var __products = _products
              .map((p) => ProductVM.fromProduct(p,
                  inCartons:
                      settingsBloc.setting.displayCarton == 1 ? true : false))
              .toList();
          __products.sort((a, b) => a.pname.compareTo(b.pname));
          yield ProductsLoaded(products: __products);
        }
      }
    } catch (ex) {
      if (ex is TimeoutException) {
        yield ProductsFetchError(
            message:
                'Request timed out! Ensure you have a strong network signal');
      } else {
        yield ProductsFetchError(message: ex.toString());
      }
    }
  }
}
