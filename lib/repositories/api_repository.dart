import 'package:trade_app/repositories/api_dataprovider/api_param.dart';
import 'package:trade_app/repositories/api_dataprovider/settings_dataprovider.dart';
import 'package:trade_app/repositories/api_dataprovider/products_dataprovider.dart';
import 'package:trade_app/repositories/api_dataprovider/purchases_dataprovider.dart';
import 'package:trade_app/repositories/api_dataprovider/sales_dataprovider.dart';

class ApiRepository {
  SettingsDataProvider settingsDataProvider;
  ProductsDataProvider productsDataProvider;
  PurchasesDataProvider purchasesDataProvider;
  SalesDataProvider salesDataProvider;

  ApiRepository(ApiParam param) {
    settingsDataProvider = SettingsDataProvider(apiParam: param);
    productsDataProvider = ProductsDataProvider(apiParam: param);
    purchasesDataProvider = PurchasesDataProvider(apiParam: param);
    salesDataProvider = SalesDataProvider(apiParam: param);
  }
}
