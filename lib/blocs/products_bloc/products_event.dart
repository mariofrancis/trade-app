part of 'products_bloc.dart';

abstract class ProductsEvent extends Equatable {
  const ProductsEvent();
}

class FetchProducts extends ProductsEvent {
  final bool isOnline;

  FetchProducts({@required this.isOnline}) : assert(isOnline != null);

  @override
  List<Object> get props => [isOnline];
}

class SearchProducts extends ProductsEvent {
  final bool isOnline;
  final String searchText;

  SearchProducts({@required this.isOnline, @required this.searchText})
      : assert(isOnline != null && searchText != null);

  @override
  List<Object> get props => [isOnline, searchText];
}

class RefreshProducts extends ProductsEvent {
  final bool isOnline;
  final String searchText;

  RefreshProducts({@required this.isOnline, @required this.searchText})
      : assert(isOnline != null && searchText != null);

  @override
  List<Object> get props => [isOnline, searchText];
}
