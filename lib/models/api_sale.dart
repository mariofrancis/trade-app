import 'package:equatable/equatable.dart';
import 'package:trade_app/models/models.dart';

class ApiSale extends Equatable {
  final int id;
  final int pid;
  final double price;
  final int qty;
  final String day;
  final String month;
  final int dateCreated;

  ApiSale(
      {this.id,
      this.pid,
      this.price,
      this.qty,
      this.day,
      this.month,
      this.dateCreated});

  @override
  List<Object> get props => [id, pid, price, qty, day, month, dateCreated];

  static ApiSale fromMap(Map<String, dynamic> map) {
    return ApiSale(
        id: int.parse(map['id'].toString()),
        pid: int.parse(map['pid'].toString()),
        price: double.parse(map['price'].toString()),
        qty: int.parse(map['qty'].toString()),
        day: map['day'],
        month: map['month'],
        dateCreated: int.parse(map['date_created'].toString()));
  }

  static ApiSale fromSale(Sale s) {
    return ApiSale(
        id: s.id,
        pid: s.pid,
        price: s.price,
        qty: s.qty,
        day: s.day,
        month: s.month,
        dateCreated: s.dateCreated);
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'pid': pid,
      'price': price,
      'qty': qty,
      'day': day,
      'month': month,
      'date_created': dateCreated
    };
  }

  Sale toSale() {
    return Sale(
        id: id,
        pid: pid,
        price: price,
        qty: qty,
        dateCreated: dateCreated,
        day: day,
        month: month);
  }

  @override
  String toString() {
    return '{id:$id, pid:$pid, price:$price, qty:$qty, day:$day, month:$month, dateCreated:$dateCreated}';
  }
}
