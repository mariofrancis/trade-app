part of 'float_button_bloc.dart';

@immutable
abstract class FloatButtonEvent extends Equatable {
  @override
  List<Object> get props=>[];
}

class SetButtonVisibility extends FloatButtonEvent{
final bool isVisible;

SetButtonVisibility({@required this.isVisible}):assert(isVisible!=null);

@override
  List<Object> get props=>[isVisible];
}