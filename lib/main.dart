import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:trade_app/blocs/blocs.dart' as blocs;
import 'package:trade_app/models/models.dart';
import 'package:trade_app/repositories/api_dataprovider/api_param.dart';
import 'package:trade_app/repositories/api_repository.dart';
import 'package:trade_app/repositories/db_repository.dart';
import 'package:trade_app/utilities/utilities.dart';
import 'package:trade_app/widgets/widgets.dart';
import 'package:http/http.dart' as http;

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  var dbRepo = DbRepository();
  var setting = await dbRepo.settingsDataProvider.get(1);
  var apiParam = ApiParam(
      httpClient: http.Client(),
      //baseUrl: 'http://192.168.43.198/trademanager/api',
      //baseUrl: 'http://192.168.8.112/trademanager/api',
      baseUrl: setting == null ? '' : setting.url,
      headerName: 'Api-Key',
      //key: 'VHJhZGVBcHBBcGlLZXlfTWF5XzI2XzIwMjA=',
      key: setting == null ? '' : setting.key,
      timeout: 60);
  var apiRepo = ApiRepository(apiParam);
  var isConnected = await blocs.ConnectivityBloc(
          connectivity: Connectivity(), apiRepo: apiRepo, dbRepo: dbRepo)
      .isConnected();
  var settingsBloc =
      blocs.SettingsBloc(dbRepo: dbRepo, apiRepo: apiRepo, setting: setting)
        ..add(blocs.FetchSettings());
  runApp(MultiBlocProvider(
    providers: [
      BlocProvider<blocs.ConnectivityBloc>(
          create: (context) => blocs.ConnectivityBloc(
              connectivity: Connectivity(), apiRepo: apiRepo, dbRepo: dbRepo)
            ..add(blocs.FetchConnectivityStatus())),
      BlocProvider<blocs.HomeBloc>(
          create: (context) =>
              blocs.HomeBloc()..add(blocs.LoadPage(page: blocs.Page.Products))),
      BlocProvider<blocs.ProductsBloc>(
          create: (context) => blocs.ProductsBloc(
              dbRepo: dbRepo, apiRepo: apiRepo, settingsBloc: settingsBloc)
            ..add(blocs.FetchProducts(isOnline: isConnected))),
      BlocProvider<blocs.FloatButtonBloc>(
          create: (context) => blocs.FloatButtonBloc()),
      BlocProvider<blocs.SettingsBloc>(create: (context) => settingsBloc),
      BlocProvider<blocs.ThemeBloc>(
          create: (context) => blocs.ThemeBloc(
              initialThemeId: setting == null ? 1 : setting.theme)
            ..add(blocs.UpdateTheme(
                themeId: setting == null ? 1 : setting.theme)))
    ],
    child: MyApp(
        dbRepo: dbRepo,
        apiRepo: apiRepo,
        isConnected: isConnected,
        settingsBloc: settingsBloc,
        setting: setting),
  ));
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  final DbRepository dbRepo;
  final ApiRepository apiRepo;
  final bool isConnected;
  final Setting setting;
  final blocs.SettingsBloc settingsBloc;

  MyApp(
      {@required this.dbRepo,
      @required this.apiRepo,
      @required this.isConnected,
      @required this.setting,
      @required this.settingsBloc})
      : assert(dbRepo != null && apiRepo != null);

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
        providers: [
          BlocProvider<blocs.PurchasesOnlineBloc>(
              create: (context) => blocs.PurchasesOnlineBloc(
                  settingsBloc: settingsBloc,
                  apiRepo: apiRepo,
                  dbRepo: dbRepo,
                  productsBloc: BlocProvider.of<blocs.ProductsBloc>(context))
                ..add(blocs.FetchPurchasesOnline(month: ''))),
          BlocProvider<blocs.PurchasesOfflineBloc>(
              create: (context) => blocs.PurchasesOfflineBloc(
                  settingsBloc: settingsBloc,
                  apiRepo: apiRepo,
                  dbRepo: dbRepo,
                  productsBloc: BlocProvider.of<blocs.ProductsBloc>(context))
                ..add(blocs.FetchPurchasesOffline())),
          BlocProvider<blocs.SalesOnlineBloc>(
              create: (context) => blocs.SalesOnlineBloc(
                  settingsBloc: settingsBloc,
                  apiRepo: apiRepo,
                  dbRepo: dbRepo,
                  productsBloc: BlocProvider.of<blocs.ProductsBloc>(context))
                ..add(blocs.FetchSalesOnline(month: ''))),
          BlocProvider<blocs.SalesOfflineBloc>(
              create: (context) => blocs.SalesOfflineBloc(
                  settingsBloc: settingsBloc,
                  apiRepo: apiRepo,
                  dbRepo: dbRepo,
                  productsBloc: BlocProvider.of<blocs.ProductsBloc>(context))
                ..add(blocs.FetchSalesOffline())),
        ],
        child: BlocBuilder<blocs.ThemeBloc, blocs.ThemeState>(
          builder: (context, state) {
            return MaterialApp(
                title: 'Trade Manager',
                theme: CustomThemes.getThemeData(
                    context, (state as blocs.ThemeUpdated).themeId),
                home: setting == null
                    ? Setup(
                        apiRepo: apiRepo,
                        dbRepo: dbRepo,
                      )
                    : Home());
          },
        ));
  }
}
