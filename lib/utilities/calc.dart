class Calc {
  static String getQuantityLeftInCartons(int qty, int pcs) {
    var ctns = qty ~/ pcs;
    var ps = qty % pcs;
    if (ps == 0) {
      return (ctns.toString() + (ctns > 1 ? ' cartons' : ' carton') + ' left');
    } else if (ctns == 0) {
      return (ps.toString() + (ps > 1 ? ' pieces' : ' piece') + ' left');
    } else {
      return ctns.toString() +
          (ctns > 1 ? ' cartons, ' : ' carton, ') +
          ps.toString() +
          (ps > 1 ? ' pieces' : ' piece') +
          ' left';
    }
  }

  static String getQuantityInCartons(int qty, int pcs) {
    var ctns = qty ~/ pcs;
    var ps = qty % pcs;
    if (ps == 0) {
      return (ctns.toString() + (ctns > 1 ? ' cartons' : ' carton'));
    } else if (ctns == 0) {
      return (ps.toString() + (ps > 1 ? ' pieces' : ' piece'));
    } else {
      return ctns.toString() +
          (ctns > 1 ? ' cartons, ' : ' carton, ') +
          ps.toString() +
          (ps > 1 ? ' pieces' : ' piece');
    }
  }

  static int getTimeStampInMilliseconds(int timeStamp) {
    var currentYear = DateTime.now().year;
    var year = DateTime.fromMillisecondsSinceEpoch(timeStamp * 1000).year;
    if (year > currentYear) {
      return timeStamp;
    } else {
      return timeStamp * 1000;
    }
  }
}
