import 'package:trade_app/models/models.dart';
import 'package:trade_app/repositories/api_dataprovider/api_param.dart';
import 'dart:convert';
import 'package:meta/meta.dart';

class SettingsDataProvider {
  final ApiParam apiParam;

  SettingsDataProvider({@required this.apiParam}) : assert(apiParam != null);
  // insert
  Future<void> update(ApiSetting setting) async {
    final url = '${apiParam.baseUrl}/settings/update';
    final data = jsonEncode(setting.toMap());
    final response = await apiParam.httpClient.post(url, body: data, headers: {
      apiParam.headerName: apiParam.key
    }).timeout(Duration(seconds: apiParam.timeout));
    if (response.statusCode == 500) {
      throw Exception('A server error was encountered! Please try again');
    } else {
      final jsonResponse = jsonDecode(response.body);
      if (!jsonResponse['is_success']) {
        throw Exception(jsonResponse['message']);
      }
    }
  }

  // get
  Future<ApiSetting> get() async {
    final url = '${apiParam.baseUrl}/settings/get';
    final response = await apiParam.httpClient.get(url, headers: {
      apiParam.headerName: apiParam.key
    }).timeout(Duration(seconds: apiParam.timeout));
    if (response.statusCode == 500) {
      throw Exception('A server error was encountered! Please try again');
    } else {
      final jsonResponse = jsonDecode(response.body);
      if (!jsonResponse['is_success']) {
        throw Exception(jsonResponse['message']);
      } else {
        return ApiSetting.fromMap(jsonResponse['data']);
      }
    }
  }
}
