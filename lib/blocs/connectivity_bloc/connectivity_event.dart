part of 'connectivity_bloc.dart';

@immutable
abstract class ConnectivityEvent extends Equatable {
  @override
  List<Object> get props=> [];
}
class FetchConnectivityStatus extends ConnectivityEvent{}
class NotifyConnectivityChange extends ConnectivityEvent{
  final ConnectivityResult result;

  NotifyConnectivityChange({@required this.result}):assert(result!=null);

  List<Object> get props=>[result];
}
class SyncOfflineData extends ConnectivityEvent{}
