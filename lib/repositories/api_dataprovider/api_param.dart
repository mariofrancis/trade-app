import 'package:meta/meta.dart';
import 'package:http/http.dart' as http;

class ApiParam {
  String baseUrl;
  String key;
  final String headerName;
  final http.Client httpClient;
  final int timeout;

  ApiParam(
      {@required this.httpClient,
      @required this.baseUrl,
      @required this.headerName,
      @required this.key,
      @required this.timeout})
      : assert(httpClient != null &&
            baseUrl != null &&
            headerName != null &&
            key != null &&
            timeout != null);
}
