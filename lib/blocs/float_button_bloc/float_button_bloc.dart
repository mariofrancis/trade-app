import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

part 'float_button_event.dart';
part 'float_button_state.dart';

class FloatButtonBloc extends Bloc<FloatButtonEvent, FloatButtonState> {
  @override
  FloatButtonState get initialState => FloatButtonShown();

  @override
  Stream<FloatButtonState> mapEventToState(
    FloatButtonEvent event,
  ) async* {
    if ((event as SetButtonVisibility).isVisible) {
      yield FloatButtonShown();
    } else {
      yield FloatButtonHidden();
    }
  }
}
