import 'package:trade_app/models/models.dart';
import 'package:trade_app/repositories/api_dataprovider/api_param.dart';
import 'dart:convert';
import 'package:meta/meta.dart';

class PurchasesDataProvider {
  final ApiParam apiParam;
  PurchasesDataProvider({@required this.apiParam}) : assert(apiParam != null);

  // insert
  Future<ApiPurchase> insert(ApiPurchase purchase) async {
    final url = '${apiParam.baseUrl}/purchases/add';
    final data = jsonEncode(purchase.toMap());
    final response = await apiParam.httpClient.post(url, body: data, headers: {
      apiParam.headerName: apiParam.key
    }).timeout(Duration(seconds: apiParam.timeout));
    if (response.statusCode == 500) {
      throw Exception('A server error was encountered! Please try again');
    } else {
      final jsonResponse = jsonDecode(response.body);
      if (!jsonResponse['is_success']) {
        throw Exception(jsonResponse['message']);
      } else {
        return ApiPurchase.fromMap(jsonResponse['data']);
      }
    }
  }

  // insert batch
  Future<void> insertBatch(List<ApiPurchase> purchases) async {
    final url = '${apiParam.baseUrl}/purchases/add_batch';
    final data = jsonEncode(purchases.map((p)=>p.toMap()).toList());
    final response = await apiParam.httpClient.post(url, body: data, headers: {
      apiParam.headerName: apiParam.key
    }).timeout(Duration(seconds: apiParam.timeout));
    if (response.statusCode == 500) {
      throw Exception('A server error was encountered! Please try again');
    } else {
      final jsonResponse = jsonDecode(response.body);
      if (!jsonResponse['is_success']) {
        throw Exception(jsonResponse['message']);
      }
    }
  }

  // get
  Future<ApiPurchase> get(int id) async {
    final url = '${apiParam.baseUrl}/purchases/get/$id';
    final response = await apiParam.httpClient.get(url, headers: {
      apiParam.headerName: apiParam.key
    }).timeout(Duration(seconds: apiParam.timeout));
    if (response.statusCode == 500) {
      throw Exception('A server error was encountered! Please try again');
    } else {
      final jsonResponse = jsonDecode(response.body);
      if (!jsonResponse['is_success']) {
        throw Exception(jsonResponse['message']);
      } else {
        return ApiPurchase.fromMap(jsonResponse['data']);
      }
    }
  }

  // get all
  Future<PurchaseVM> getRecords({String month = '', bool inCartons = true}) async {
    final url = '${apiParam.baseUrl}/purchases/get_records/' +
        (month == '' ? '' : Uri.encodeComponent(month));
    final response = await apiParam.httpClient.get(url, headers: {
      apiParam.headerName: apiParam.key
    }).timeout(Duration(seconds: apiParam.timeout));
    if (response.statusCode == 500) {
      throw Exception('A server error was encountered! Please try again');
    } else {
      final jsonResponse = jsonDecode(response.body);
      if (!jsonResponse['is_success']) {
        throw Exception(jsonResponse['message']);
      } else {
        return PurchaseVM.fromJson(jsonResponse['data'], inCartons);
      }
    }
  }

  // update
  Future<void> update(ApiPurchase purchase) async {
    final url = '${apiParam.baseUrl}/purchases/update';
    final data = jsonEncode(purchase.toMap());
    final response = await apiParam.httpClient.post(url, body: data, headers: {
      apiParam.headerName: apiParam.key
    }).timeout(Duration(seconds: apiParam.timeout));
    if (response.statusCode == 500) {
      throw Exception('A server error was encountered! Please try again');
    } else {
      final jsonResponse = jsonDecode(response.body);
      if (!jsonResponse['is_success']) {
        throw Exception(jsonResponse['message']);
      }
    }
  }

  Future<void> delete(int id) async {
    final url = '${apiParam.baseUrl}/purchases/delete/$id';
    final response = await apiParam.httpClient.get(url, headers: {
      apiParam.headerName: apiParam.key
    }).timeout(Duration(seconds: apiParam.timeout));
    if (response.statusCode == 500) {
      throw Exception('A server error was encountered! Please try again');
    } else {
      final jsonResponse = jsonDecode(response.body);
      if (!jsonResponse['is_success']) {
        throw Exception(jsonResponse['message']);
      }
    }
  }
}
