import 'package:equatable/equatable.dart';
import 'package:trade_app/models/models.dart';

class ApiPurchase extends Equatable {
  final int id;
  final int pid;
  final double price;
  final int qty;
  final String day;
  final String month;
  final int dateCreated;

  ApiPurchase(
      {this.id,
      this.pid,
      this.price,
      this.qty,
      this.day,
      this.month,
      this.dateCreated});

  @override
  List<Object> get props => [id, pid, price, qty, day, month, dateCreated];

  static ApiPurchase fromMap(Map<String, dynamic> map) {
    return ApiPurchase(
        id: int.parse(map['id'].toString()),
        pid: int.parse(map['pid'].toString()),
        price: double.parse(map['price'].toString()),
        qty: int.parse(map['qty'].toString()),
        day: map['day'],
        month: map['month'],
        dateCreated: int.parse(map['date_created'].toString()));
  }

  static ApiPurchase fromPurchase(Purchase p) {
    return ApiPurchase(
        id: p.id,
        pid: p.pid,
        price: p.price,
        qty: p.qty,
        day: p.day,
        month: p.month,
        dateCreated: p.dateCreated);
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'pid': pid,
      'price': price,
      'qty': qty,
      'day': day,
      'month': month,
      'date_created': dateCreated
    };
  }

  Purchase toPurchase() {
    return Purchase(
        id: id,
        pid: pid,
        price: price,
        qty: qty,
        dateCreated: dateCreated,
        day: day,
        month: month);
  }

  @override
  String toString() {
    return '{id:$id, pid:$pid, price:$price, qty:$qty, day:$day, month:$month, dateCreated:$dateCreated}';
  }
}
