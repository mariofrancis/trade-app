import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:trade_app/blocs/blocs.dart';
import 'package:trade_app/models/models.dart';
import 'package:trade_app/utilities/utilities.dart';
import 'package:trade_app/widgets/widgets.dart';

class NewPurchase extends StatefulWidget {
  final BuildContext mainContext;
  final List<Product> products;
  NewPurchase({@required this.mainContext, @required this.products});
  @override
  _NewPurchaseState createState() =>
      _NewPurchaseState(mainContext: mainContext, products: products);
}

class _NewPurchaseState extends State<NewPurchase> {
  final List<Product> products;
  final BuildContext mainContext;
  final _formKey = GlobalKey<FormState>();
  final _priceController = new TextEditingController();
  final _qtyController = new TextEditingController();
  String _qtyUnit = 'piece';
  Product _product;

  _NewPurchaseState({@required this.mainContext, @required this.products});

  @override
  void dispose() {
    _priceController.dispose();
    _qtyController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('New Purchase Record'),
      ),
      body: Column(
        children: <Widget>[
          ConnectivityOfflineBanner(),
          Expanded(
            child: GradientContainer(
                child: Padding(
              padding:
                  const EdgeInsets.symmetric(horizontal: 4.0, vertical: 4.0),
              child: _buildForm(context),
            )),
          ),
        ],
      ),
    );
  }

  // get form
  Widget _buildForm(BuildContext context) {
    return Form(
        key: _formKey,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: <Widget>[
            Expanded(
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Padding(padding: EdgeInsets.only(top: 32)),
                    FormFieldCard(
                        child: ProductSearch(
                      products: products,
                      selectedItem: _product,
                      onChange: (_p) {
                        setState(() {
                          _product = _p;
                        });
                      },
                    )),
                    Padding(padding: EdgeInsets.symmetric(vertical: 12)),

                    Padding(
                      padding: const EdgeInsets.only(bottom: 4, left: 4),
                      child: Text(
                        'Quantity Purchased',
                        style: Theme.of(context).textTheme.headline4.copyWith(
                              fontSize: 12,
                            ),
                      ),
                    ),
                    Row(
                      children: <Widget>[
                        Expanded(
                          flex: 1,
                          child: FormFieldCard(
                            child: DropdownButtonFormField<String>(
                              value: _qtyUnit,
                              decoration: Styles.getInputDecoration(context,
                                  label: 'Qty Unit *'),
                              items: [
                                DropdownMenuItem(
                                  child: Text('Piece'),
                                  value: 'piece',
                                ),
                                DropdownMenuItem(
                                  child: Text('Carton'),
                                  value: 'carton',
                                )
                              ],
                              style: Styles.getInputTextStyle(context).copyWith(
                                  decorationColor: Theme.of(context)
                                      .primaryColorDark
                                      .withOpacity(.5)),
                              onChanged: (val) {
                                setState(() {
                                  _qtyUnit = val;
                                });
                              },
                            ),
                          ),
                        ),
                        Padding(padding: EdgeInsets.symmetric(horizontal: 4)),
                        Expanded(
                          flex: 2,
                          child: FormFieldCard(
                            child: TextFormField(
                                controller: _qtyController,
                                decoration: Styles.getInputDecoration(context,
                                    label: 'Quantity *',
                                    hint: 'Quantity in pieces/cartons'),
                                style: Styles.getInputTextStyle(context)
                                    .copyWith(
                                        decorationColor: Theme.of(context)
                                            .primaryColorDark
                                            .withOpacity(.5)),
                                keyboardType: TextInputType.numberWithOptions(decimal: false, signed: false),
                                validator: (value) {
                                  if (value.trim().isEmpty) {
                                    return 'Quantity is required';
                                  }else if(int.parse(value) < 1){
                                    return 'Quantity cannot be less than 1';
                                  }
                                  return null;
                                }),
                          ),
                        )
                      ],
                    ),
                    Padding(padding: EdgeInsets.symmetric(vertical: 12)),
                    // Price Purchases
                    FormFieldCard(
                      child: TextFormField(
                          controller: _priceController,
                          decoration: Styles.getInputDecoration(context,
                              label: 'Total price purchased *',
                              //hint: 'What is the price for a carton/piece?'),
                              hint: 'What is the total price?'),
                          style: Styles.getInputTextStyle(context).copyWith(
                              decorationColor: Theme.of(context)
                                  .primaryColorDark
                                  .withOpacity(.5)),
                          keyboardType: TextInputType.number,
                          validator: (value) {
                            if (value.trim().isEmpty) {
                              return 'Price is required';
                            }
                            return null;
                          }),
                    ),

                    Padding(padding: EdgeInsets.symmetric(vertical: 12)),
                  ],
                ),
              ),
            ),
            Divider(
              height: 2,
            ),
            RaisedButton.icon(
              onPressed: () {
                _onSubmit(context);
              },
              icon: Icon(
                Icons.check_circle_outline,
                size: 16,
              ),
              label: Text('Add New Record'),
              colorBrightness: Theme.of(context).accentColorBrightness,
            )
          ],
        ));
  }

  Future<void> _onSubmit(BuildContext _context) async {
    if (_formKey.currentState.validate()) {
      var qty = int.parse(_qtyController.text);
      var price = double.parse(_priceController.text);
      var day = DateFormat('MMMM d, y', 'en_US').format(DateTime.now());
      var month = DateFormat('MMMM, y', 'en_US').format(DateTime.now());
      int timeStamp = int.parse(
          (DateTime.now().millisecondsSinceEpoch / 1000).round().toString());
      var purchase = ApiPurchase(
          pid: _product.pid,
          price: _qtyUnit == 'piece' ? (price/qty) : ((price / qty)/ _product.pcs),
          qty: _qtyUnit == 'piece' ? qty : (qty * _product.pcs),
          day: day,
          month: month,
          dateCreated: timeStamp);

      var connectivityState =
          BlocProvider.of<ConnectivityBloc>(mainContext).state;
      // BlocProvider.of<NewPurchaseBloc>(_context).add(AddNewPurchase(
      //     purchase: purchase,
      //     isOnline: connectivityState is ConnectivityOnline));
      _addNewPurchaseRecord(purchase, connectivityState is ConnectivityOnline);
    }
  }

  void _addNewPurchaseRecord(ApiPurchase purchase, bool isOnline) async {
    var loader = Dialogs.getLoaderDialog(context,
        initialMessage: 'Adding new purchase record...');
    await loader.show();
    try {
      var apiRepo = BlocProvider.of<PurchasesOnlineBloc>(context).apiRepo;
      var dbRepo = BlocProvider.of<PurchasesOnlineBloc>(context).dbRepo;

      var product = await dbRepo.productsDataProvider.get(purchase.pid);
      // if (event.purchase.qty > product.qty) {
      //   yield NewPurchaseError(
      //       message:
      //           'Specified quantity exceeds available quantity of ${Calc.getQuantityInCartons(product.qty, product.pcs)}!');
      // } else {
      if (isOnline) {
        await apiRepo.purchasesDataProvider.insert(purchase);
        BlocProvider.of<ProductsBloc>(context)
            .add(RefreshProducts(isOnline: true, searchText: ''));
      } else {
        await dbRepo.purchasesDataProvider.insert(purchase.toPurchase());
        product =
            product.copyWith(qty: product.qty + purchase.qty); // update product
        dbRepo.productsDataProvider.update(product); // update product
        BlocProvider.of<ProductsBloc>(context)
            .add(RefreshProducts(isOnline: false, searchText: ''));
      }
      await loader.hide();
      var setting = BlocProvider.of<SettingsBloc>(context).setting;
      var msg = (isOnline
              ? 'Purchase record was added successfully.'
              : 'Purchase record was saved offline.') +
          ' Quantity left is ${(setting.displayCarton == 1 ? Calc.getQuantityInCartons(product.qty, product.pcs) : product.qty.toString() + ' pieces')}.';
      await Dialogs.alertDialog(context, msg, type: AlertType.success);
      _clearFields();
      //}
    } catch (ex) {
      var msg = ex is TimeoutException
          ? 'Request timed out! Ensure you have a strong network signal'
          : ex.toString();
      await loader.hide();
      await Dialogs.alertDialog(context, msg, type: AlertType.error);
    }
  }

  void _clearFields() {
    _priceController.clear();
    _qtyController.clear();
    setState(() {
      //_qtyUnit = 'piece';
      _product = null;
    });
  }
}
