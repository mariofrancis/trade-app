import 'package:equatable/equatable.dart';
import 'package:intl/intl.dart';
import 'package:trade_app/models/api_purchase.dart';
import 'package:trade_app/models/models.dart';
import 'package:trade_app/repositories/repositories.dart';
import 'package:trade_app/utilities/utilities.dart';

class Record extends Equatable {
  final int id;
  final int pid;
  final double price;
  final String displayPrice;
  final int qty;
  final String displayQty;
  final int dateCreated;
  final String time;
  final String pname;
  final double pprice;
  final String displayPprice;
  final int pcs;
  final String total;

  Record(
      {this.id,
      this.pid,
      this.price,
      this.displayPrice,
      this.qty,
      this.displayQty,
      this.dateCreated,
      this.time,
      this.pname,
      this.pprice,
      this.displayPprice,
      this.pcs,
      this.total});

  @override
  List<Object> get props => [
        id,
        pid,
        price,
        displayPrice,
        qty,
        displayQty,
        dateCreated,
        time,
        pname,
        pprice,
        displayPprice,
        pcs,
        total
      ];

  static Record fromJson(dynamic json, {bool inCartons = true}) {
    var qty = int.parse(json['qty']);
    var pcs = int.parse(json['pcs']);
    return Record(
        id: int.parse(json['id']),
        pid: int.parse(json['pid']),
        price: double.parse(json['price']),
        displayPrice: NumberFormat.simpleCurrency(name: 'NGN', decimalDigits: 2)
            .format(double.parse(json['price'])),
        qty: qty,
        displayQty:
            inCartons ? Calc.getQuantityInCartons(qty, pcs) : (qty > 1?qty.toString()+' pieces':qty.toString()+' piece'),
        dateCreated: int.parse(json['date_created']),
        time: DateFormat('jm', 'en_US').format(
            DateTime.fromMillisecondsSinceEpoch(Calc.getTimeStampInMilliseconds(
                int.parse(json['date_created'])))),
        pname: json['pname'],
        pprice: double.parse(json['pprice']),
        displayPprice:
            NumberFormat.simpleCurrency(name: 'NGN', decimalDigits: 2)
                .format(double.parse(json['pprice'])),
        pcs: int.parse(json['pcs']),
        total: NumberFormat.simpleCurrency(name: 'NGN', decimalDigits: 2)
            .format(double.parse(json['total'].toString())));
  }

  static Future<Record> fromPurchase(Purchase purchase, DbRepository dbRepo,
      {bool inCartons = true}) async {
    var product = await dbRepo.productsDataProvider.get(purchase.pid);
    return Record(
        id: purchase.id,
        pid: purchase.pid,
        price: purchase.price,
        displayPrice: NumberFormat.simpleCurrency(name: 'NGN', decimalDigits: 2)
            .format(purchase.price),
        qty: purchase.qty,
        displayQty: inCartons
            ? Calc.getQuantityInCartons(purchase.qty, product.pcs)
            : (purchase.qty > 1?purchase.qty.toString()+' pieces':purchase.qty.toString()+' piece'),
        dateCreated: purchase.dateCreated,
        time: DateFormat('MMM d, y', 'en_US').add_jm().format(
            DateTime.fromMillisecondsSinceEpoch(
                Calc.getTimeStampInMilliseconds(purchase.dateCreated))),
        pname: product.pname,
        pprice: product.price,
        displayPprice:
            NumberFormat.simpleCurrency(name: 'NGN', decimalDigits: 2)
                .format(product.price),
        pcs: product.pcs,
        total: NumberFormat.simpleCurrency(name: 'NGN', decimalDigits: 2)
            .format(purchase.price * purchase.qty));
  }

  static Future<Record> fromSale(Sale sale, DbRepository dbRepo,
      {bool inCartons = true}) async {
    var product = await dbRepo.productsDataProvider.get(sale.pid);
    return Record(
        id: sale.id,
        pid: sale.pid,
        price: sale.price,
        displayPrice: NumberFormat.simpleCurrency(name: 'NGN', decimalDigits: 2)
            .format(sale.price),
        qty: sale.qty,
        displayQty: inCartons
            ? Calc.getQuantityInCartons(sale.qty, product.pcs)
            : (sale.qty > 1?sale.qty.toString()+' pieces':sale.qty.toString()+' piece'),
        dateCreated: sale.dateCreated,
        time: DateFormat('MMM d, y', 'en_US').add_jm().format(
            DateTime.fromMillisecondsSinceEpoch(
                Calc.getTimeStampInMilliseconds(sale.dateCreated))),
        pname: product.pname,
        pprice: product.price,
        displayPprice:
            NumberFormat.simpleCurrency(name: 'NGN', decimalDigits: 2)
                .format(product.price),
        pcs: product.pcs,
        total: NumberFormat.simpleCurrency(name: 'NGN', decimalDigits: 2)
            .format(sale.price * sale.qty));
  }

  ApiPurchase toApiPurchase() {
    return ApiPurchase(
        id: id, pid: pid, price: price, qty: qty, dateCreated: dateCreated);
  }

  ApiSale toApiSale() {
    return ApiSale(
        id: id, pid: pid, price: price, qty: qty, dateCreated: dateCreated);
  }

}

class DayRecord extends Equatable {
  final String day;
  final List<Record> records;
  final String subTotal;

  DayRecord({this.day, this.records, this.subTotal});

  @override
  List<Object> get props => [day, records, subTotal];

  static DayRecord fromJson(dynamic json, {bool inCartons=true}) {
    var jsonRecs = json['records'] as List;
    var recs = jsonRecs.map((e) => Record.fromJson(e, inCartons: inCartons)).toList();
    return DayRecord(
        day: json['day'],
        records: recs,
        subTotal: NumberFormat.simpleCurrency(name: 'NGN', decimalDigits: 2)
            .format(double.parse(json['sub_total'].toString())));
  }
}
