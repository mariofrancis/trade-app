import 'package:trade_app/repositories/db_dataprovider/settings_dataprovider.dart';
import 'package:trade_app/repositories/db_dataprovider/products_dataprovider.dart';
import 'package:trade_app/repositories/db_dataprovider/purchases_dataprovider.dart';
import 'package:trade_app/repositories/db_dataprovider/sales_dataprovider.dart';

class DbDataProvider {
  SettingsDataProvider get settingsDataProvider => SettingsDataProvider();
  ProductsDataProvider get productsDataProvider => ProductsDataProvider();
  PurchasesDataProvider get purchasesDataProvider => PurchasesDataProvider();
  SalesDataProvider get salesDataProvider => SalesDataProvider();
}
