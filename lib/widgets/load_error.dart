import 'package:flutter/material.dart';

class LoadError extends StatelessWidget {
  final String message;
  final void Function() onTryAgain;

  LoadError({@required this.message, @required this.onTryAgain});

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
          child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Padding(padding: EdgeInsets.only(top: 100)),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
                child: Icon(
                  Icons.error_outline,
                  color: Theme.of(context).disabledColor.withOpacity(.2),
                  size: 70,
                ),
              ),
              Text(
                'Something went wrong',
                style: Theme.of(context).textTheme.headline4.copyWith(
                    fontSize: 18,
                    color: Theme.of(context).textTheme.headline4.color.withOpacity(.5)),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
                child: Text(
                  'Error Message: ' + message,
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.headline4.copyWith(
                      fontSize: 12,
                      color: Theme.of(context).textTheme.headline4.color.withOpacity(.5)),
                ),
              ),
              Padding(
                  padding: EdgeInsets.symmetric(horizontal: 16, vertical: 8),
                  child: FlatButton(
                    child: Text('Try again!'),
                    color: Theme.of(context).primaryColorLight,
                    textColor: Theme.of(context).textTheme.headline4.color.withOpacity(.6),
                    onPressed: onTryAgain,
                  ))
            ],
          ),
        ),
    );
  }
}