import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:trade_app/blocs/blocs.dart';
import 'package:trade_app/models/models.dart';
import 'package:trade_app/utilities/utilities.dart';
import 'package:trade_app/widgets/widgets.dart';

class Settings extends StatefulWidget {
  final Setting setting;

  Settings({@required this.setting});
  @override
  _SettingsState createState() => _SettingsState(setting: setting);
}

class _SettingsState extends State<Settings> {
  final Setting setting;
  final _formKey = GlobalKey<FormState>();
  final _pswdController = new TextEditingController();
  final _cpswdController = new TextEditingController();
  final _saController = new TextEditingController();
  final _urlController = new TextEditingController();
  final _keyController = new TextEditingController();
  bool _displayInCartons;
  int _themeId;
  String _sq = '';

  final questions = [
    'What is your nickname?',
    'What is your favourite color?',
    'What is the name of your best friend?',
    'What is your favourite food?',
    'In which month of the year were you born?'
  ];

  _SettingsState({@required this.setting});

  @override
  void initState() {
    super.initState();
    print(setting.toString());
    _saController.text = setting.sans;
    _sq = setting.sqstn;
    _urlController.text = setting.url;
    _keyController.text = setting.key;
    _displayInCartons = setting.displayCarton == 1 ? true : false;
    _themeId = setting.theme;
  }

  @override
  void dispose() {
    _pswdController.dispose();
    _cpswdController.dispose();
    _saController.dispose();
    _urlController.dispose();
    _keyController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GradientContainer(
          child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 4.0),
        child: _buildForm(context),
      )),
    );
  }

  // get form
  Widget _buildForm(BuildContext context) {
    return Form(
        key: _formKey,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: <Widget>[
            Expanded(
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Padding(padding: EdgeInsets.only(top: 8)),
                    Text(
                      'Security Settings',
                      style: Theme.of(context)
                          .textTheme
                          .headline4
                          .copyWith(fontSize: 12),
                    ),
                    Divider(),
                    // product name
                    FormFieldCard(
                      child: TextFormField(
                        controller: _pswdController,
                        obscureText: true,
                        decoration: Styles.getInputDecoration(context,
                            label: 'New Password', hint: 'Enter password?'),
                        style: Styles.getInputTextStyle(context).copyWith(
                            decorationColor: Theme.of(context)
                                .primaryColorDark
                                .withOpacity(.5)),
                        validator: (value) {
                          if (value.isNotEmpty) {
                            if (value.length < 8) {
                              return 'Password should be up to eight(8) characters in length';
                            }
                          }
                          return null;
                        },
                      ),
                    ),
                    Padding(padding: EdgeInsets.symmetric(vertical: 4)),
                    // confirm password
                    FormFieldCard(
                      child: TextFormField(
                          controller: _cpswdController,
                          obscureText: true,
                          decoration: Styles.getInputDecoration(context,
                              label: 'Confirm New Password',
                              hint: 'Enter password again to confirm?'),
                          style: Styles.getInputTextStyle(context).copyWith(
                              decorationColor: Theme.of(context)
                                  .primaryColorDark
                                  .withOpacity(.5)),
                          validator: (value) {
                            if (_pswdController.text.isNotEmpty) {
                              if (value.isEmpty) {
                                return 'Confirm password is required';
                              } else if (value != _pswdController.text) {
                                return 'Passwords do not match';
                              }
                            }
                            return null;
                          }),
                    ),
                    Padding(padding: EdgeInsets.symmetric(vertical: 4)),
                    // Security question
                    FormFieldCard(
                      child: DropdownButtonFormField<String>(
                        isExpanded: true,
                        value: _sq,
                        style: Styles.getInputTextStyle(context).copyWith(
                            decorationColor: Theme.of(context)
                                .primaryColorDark
                                .withOpacity(.5)),
                        decoration: Styles.getInputDecoration(context,
                            label: 'Security Question *'),
                        items: questions
                            .map((q) => DropdownMenuItem(
                                  child: Text(q),
                                  value: q,
                                ))
                            .toList()
                              ..insert(
                                  0,
                                  DropdownMenuItem(
                                    child:
                                        Text('-- Select security question --'),
                                    value: '',
                                  )),
                        onChanged: (val) {
                          setState(() {
                            _sq = val;
                          });
                        },
                        validator: (value) =>
                            _sq == '' ? 'Security question is required' : null,
                      ),
                    ),

                    Padding(padding: EdgeInsets.symmetric(vertical: 4)),
                    // Security answer
                    FormFieldCard(
                      child: TextFormField(
                          controller: _saController,
                          decoration: Styles.getInputDecoration(context,
                              label: 'Security Answer *',
                              hint:
                                  'Enter an answer to the above selected question'),
                          style: Styles.getInputTextStyle(context).copyWith(
                              decorationColor: Theme.of(context)
                                  .primaryColorDark
                                  .withOpacity(.5)),
                          validator: (value) {
                            if (value.trim().isEmpty) {
                              return 'Security answer is required';
                            }
                            return null;
                          }),
                    ),
                    Padding(padding: EdgeInsets.symmetric(vertical: 12)),
                    Text(
                      'API Settings',
                      style: Theme.of(context)
                          .textTheme
                          .headline4
                          .copyWith(fontSize: 12),
                    ),
                    Divider(),
                    FormFieldCard(
                      child: TextFormField(
                          controller: _urlController,
                          decoration: Styles.getInputDecoration(context,
                              label: 'API URL *', hint: 'Enter API URL'),
                          style: Styles.getInputTextStyle(context).copyWith(
                              decorationColor: Theme.of(context)
                                  .primaryColorDark
                                  .withOpacity(.5)),
                          validator: (value) {
                            if (value.trim().isEmpty) {
                              return 'API URL is required';
                            }
                            return null;
                          }),
                    ),
                    Padding(padding: EdgeInsets.symmetric(vertical: 4)),
                    FormFieldCard(
                      child: TextFormField(
                          controller: _keyController,
                          decoration: Styles.getInputDecoration(context,
                              label: 'API key *', hint: 'Enter API key'),
                          style: Styles.getInputTextStyle(context).copyWith(
                              decorationColor: Theme.of(context)
                                  .primaryColorDark
                                  .withOpacity(.5)),
                          validator: (value) {
                            if (value.trim().isEmpty) {
                              return 'API key is required';
                            }
                            return null;
                          }),
                    ),
                    Padding(padding: EdgeInsets.symmetric(vertical: 12)),
                    Text(
                      'Display Settings',
                      style: Theme.of(context)
                          .textTheme
                          .headline4
                          .copyWith(fontSize: 12),
                    ),
                    Divider(),
                    FormFieldCard(
                      child: Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 8, vertical: 4),
                        child: Row(
                          children: <Widget>[
                            Expanded(
                                child: Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 4),
                              child: Text(
                                'Display products\' available quantity in cartons',
                                style: Theme.of(context)
                                    .textTheme
                                    .headline4
                                    .copyWith(fontSize: 14, height: 1.3),
                              ),
                            )),
                            Switch(
                                activeColor: Theme.of(context).primaryColor,
                                value: _displayInCartons,
                                onChanged: (val) {
                                  setState(() {
                                    _displayInCartons = val;
                                  });
                                })
                          ],
                        ),
                      ),
                    ),
                    Padding(padding: EdgeInsets.symmetric(vertical: 4)),
                    FormFieldCard(
                      child: DropdownButtonFormField<int>(
                        isExpanded: true,
                        value: _themeId,
                        style: Styles.getInputTextStyle(context).copyWith(
                            decorationColor: Theme.of(context)
                                .primaryColorDark
                                .withOpacity(.5)),
                        decoration: Styles.getInputDecoration(context,
                            label: 'App Theme'),
                        items: CustomThemes.themes
                            .map((t) => DropdownMenuItem(
                                  child: Row(
                                    children: <Widget>[
                                      Icon(
                                        Icons.check_circle,
                                        color: t.color,
                                        size: 20,
                                      ),
                                      Padding(
                                          padding: EdgeInsets.symmetric(
                                              horizontal: 4)),
                                      Padding(
                                        padding: const EdgeInsets.symmetric(
                                            horizontal: 4),
                                        child: Text(
                                          t.name,
                                          style: Theme.of(context)
                                              .textTheme
                                              .headline4
                                              .copyWith(
                                                  fontSize: 14, height: 1.3),
                                        ),
                                      ),
                                    ],
                                  ),
                                  value: t.id,
                                ))
                            .toList(),
                        onChanged: (val) {
                          setState(() {
                            _themeId = val;
                          });
                        },
                        validator: (value) =>
                            _sq == '' ? 'Security question is required' : null,
                      ),
                    ),
                    Padding(padding: EdgeInsets.symmetric(vertical: 12)),
                  ],
                ),
              ),
            ),
            Divider(
              height: 2,
            ),
            RaisedButton.icon(
              onPressed: () {
                _onSubmit(context);
              },
              icon: Icon(
                Icons.check_circle_outline,
                size: 16,
              ),
              label: Text('Save Settings'),
              colorBrightness: Theme.of(context).accentColorBrightness,
            )
          ],
        ));
  }

  Future<void> _onSubmit(BuildContext _context) async {
    if (_formKey.currentState.validate()) {
      var pswd = _pswdController.text;
      var sa = _saController.text;
      var url = _urlController.text;
      var key = _keyController.text;
      var _setting = setting.copyWith(
          id: setting.id,
          pswd: pswd.isEmpty ? null : pswd,
          sqstn: _sq,
          sans: sa,
          url: url,
          key: key,
          displayCarton: _displayInCartons ? 1 : 0,
          theme: _themeId);
      _updateSetting(_setting);
    }
  }

  void _updateSetting(Setting _setting) async {
    var loader =
        Dialogs.getLoaderDialog(context, initialMessage: 'Saving settings...')
          ..show();
    try {
      var dbRepo = BlocProvider.of<SettingsBloc>(context).dbRepo;
      await dbRepo.settingsDataProvider.update(_setting);
      await _updateApp(_setting);
      await loader.hide();
      await Dialogs.alertDialog(context, 'Setup was completed successfully.',
          type: AlertType.success);
      //navigate to home
      Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (context) => Home()));
    } catch (ex) {
      var msg = ex.toString();
      await loader.hide();
      await Dialogs.alertDialog(context, msg, type: AlertType.error);
    }
  }

  Future<void> _updateApp(Setting _setting) async {
    // update ApiParams
    BlocProvider.of<ConnectivityBloc>(context)
        .apiRepo
        .productsDataProvider
        .apiParam
        .baseUrl = _setting.url;
    BlocProvider.of<ConnectivityBloc>(context)
        .apiRepo
        .productsDataProvider
        .apiParam
        .key = _setting.key;
    BlocProvider.of<SettingsBloc>(context).setting = _setting;

    var connected =
        BlocProvider.of<ConnectivityBloc>(context).state is ConnectivityOnline;
    BlocProvider.of<SettingsBloc>(context).add(FetchSettings());
    BlocProvider.of<ThemeBloc>(context)
        .add(UpdateTheme(themeId: _setting.theme));
    BlocProvider.of<ProductsBloc>(context)
        .add(RefreshProducts(isOnline: connected, searchText: ''));
    if (connected) {
      BlocProvider.of<PurchasesOnlineBloc>(context)
          .add(RefreshPurchasesOnline(month: BlocProvider.of<PurchasesOnlineBloc>(context).month));
      BlocProvider.of<SalesOnlineBloc>(context)
          .add(RefreshSalesOnline(month: BlocProvider.of<SalesOnlineBloc>(context).month));
    } else {
      BlocProvider.of<PurchasesOfflineBloc>(context)
          .add(RefreshPurchasesOffline());
      BlocProvider.of<SalesOfflineBloc>(context).add(RefreshSalesOffline());
    }
  }
}
