part of 'float_button_bloc.dart';

@immutable
abstract class FloatButtonState {}

class FloatButtonHidden extends FloatButtonState{}
class FloatButtonShown extends FloatButtonState{}