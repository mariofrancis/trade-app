import 'package:flutter/material.dart';
import 'package:trade_app/models/models.dart';

class PurchasesDataTable extends StatelessWidget {
  final DayRecord dayRecord;
  final bool isOnline;
  final void Function(BuildContext, String) onItemOptionSelect;

  PurchasesDataTable({@required this.dayRecord, @required this.onItemOptionSelect, this.isOnline})
      : assert(dayRecord != null && onItemOptionSelect!=null);

  @override
  Widget build(BuildContext context) {
    return DataTable(
        horizontalMargin: 12,
        columnSpacing: 12,
        dataRowHeight: 28,
        headingRowHeight: 32,
        columns: <DataColumn>[
    DataColumn(
      label: Text(
        'Product',
        style: Theme.of(context).textTheme.subtitle2.copyWith(
            fontWeight: FontWeight.w500,
            color: Theme.of(context).textTheme.headline4.color,
            fontSize: 12),
      ),
    ),
    DataColumn(
      label: Text('Quantity',
          style: Theme.of(context).textTheme.subtitle2.copyWith(
              fontWeight: FontWeight.w500,
              color: Theme.of(context).textTheme.headline4.color, fontSize: 12)),
    ),
    DataColumn(
      label: Text('Unit Price',
          style: Theme.of(context).textTheme.subtitle2.copyWith(
              fontWeight: FontWeight.w500,
              color: Theme.of(context).textTheme.headline4.color, fontSize: 12)),
    ),
    DataColumn(
      label: Text('Price',
          style: Theme.of(context).textTheme.subtitle2.copyWith(
              fontWeight: FontWeight.w500,
              color: Theme.of(context).textTheme.headline4.color, fontSize: 12)),
    ),
    DataColumn(
      label: Text(isOnline?'Time':'Date',
          style: Theme.of(context).textTheme.subtitle2.copyWith(
              fontWeight: FontWeight.w500,
              color: Theme.of(context).textTheme.headline4.color, fontSize: 12)),
    ),
    DataColumn(
      label: Text(' ',
          style: Theme.of(context).textTheme.subtitle2.copyWith(
              fontWeight: FontWeight.w500,
              color: Theme.of(context).textTheme.headline4.color, fontSize: 12)),
    ),
        ],
        rows: dayRecord.records
      .map((r) => DataRow(
            cells: <DataCell>[
              DataCell(Text(r.pname,
                  style: Theme.of(context).textTheme.bodyText2.copyWith(
                      color: Theme.of(context).textTheme.headline4.color, fontSize: 12))),
              DataCell(Text(r.displayQty,
                  style: Theme.of(context).textTheme.bodyText2.copyWith(
                      color: Theme.of(context).textTheme.headline4.color, fontSize: 12))),
              DataCell(Text(r.displayPrice,
                  style: Theme.of(context).textTheme.bodyText2.copyWith(
                      color: Theme.of(context).textTheme.headline4.color, fontSize: 12))),
              DataCell(Text(r.total,
                  style: Theme.of(context).textTheme.bodyText2.copyWith(
                      color: Theme.of(context).textTheme.headline4.color, fontSize: 12))),
              DataCell(Text(r.time,
                  style: Theme.of(context).textTheme.bodyText2.copyWith(
                      color: Theme.of(context).textTheme.headline4.color, fontSize: 12))),
              DataCell(PopupMenuButton(
                child: Icon(Icons.more_vert,
                    size: 20,
                    color: Theme.of(context).textTheme.headline4.color),
                itemBuilder: (BuildContext context) => ['Edit', 'Delete']
                    .map((m) => PopupMenuItem<String>(
                          height: 35,
                          value: '${m}_${r.id}',
                          child: Row(
                            children: <Widget>[
                              Icon(m == 'Edit' ? Icons.edit : Icons.delete,
                                  size: 14,
                                  color:
                                      Theme.of(context).textTheme.headline4.color),
                              Padding(
                                  padding:
                                      EdgeInsets.symmetric(horizontal: 4)),
                              Text(m, style: Theme.of(context).textTheme.bodyText2.copyWith(
                      color: Theme.of(context).textTheme.headline4.color, fontSize: 14),),
                            ],
                          ),
                        ))
                    .toList(),
                onSelected: (val) {
                  onItemOptionSelect(context, val);
                },
              )),
            ],
          ))
      .toList()
        ..add(DataRow(
          cells: <DataCell>[
            DataCell(Text(''),),
            DataCell(Text('')),
            DataCell(Text(isOnline?'Total:':'',
                style: Theme.of(context).textTheme.bodyText2.copyWith(
                    color: Theme.of(context).textTheme.headline4.color,
                    fontWeight: FontWeight.bold, fontSize: 12))),
            DataCell(Text(dayRecord.subTotal,
                style: Theme.of(context)
                    .textTheme
                    .bodyText2
                    .copyWith(color: Theme.of(context).textTheme.headline4.color, fontSize: 12))),
            DataCell(Text('')),
            DataCell(Text('')),
          ],
        ))
        ..removeWhere((e) => !isOnline && (e.cells[2].child as Text).data==''),
      );
  }
  
}
