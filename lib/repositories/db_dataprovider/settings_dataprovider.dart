import 'package:trade_app/models/models.dart';
import 'package:trade_app/repositories/db_dataprovider/app_database.dart';

class SettingsDataProvider {
  // insert
  Future<int> insert(Setting setting) async {
    final db = await AppDatabase.instance.database;
    return db.insert(Setting.table, setting.toMap());
  }

  // get
  Future<Setting> get(int id) async {
    final db = await AppDatabase.instance.database;
    List<Map> maps = await db
        .query(Setting.table, where: '${Setting.colId} = ?', whereArgs: [id]);
    if (maps.length > 0) {
      return Setting.fromMap(maps.first);
    }
    return null;
  }

  // get all
  Future<List<Setting>> getAll() async {
    final db = await AppDatabase.instance.database;
    List<Map> maps =
        await db.query(Setting.table, orderBy: '${Setting.colId} DESC');
    if (maps.length > 0) {
      return maps.map((row) => Setting.fromMap(row)).toList();
    }
    return List<Setting>();
  }

  // update
  Future<int> update(Setting setting) async {
    final db = await AppDatabase.instance.database;
    return db.update(
      Setting.table,
      setting.toMap(),
      where: '${Setting.colId} = ?',
      whereArgs: [setting.id],
    );
  }

  Future<int> deleteSetting(int id) async {
    final db = await AppDatabase.instance.database;
    return db
        .delete(Setting.table, where: '${Setting.colId} = ?', whereArgs: [id]);
  }
}
