import 'package:sqflite/sqflite.dart';
import 'package:trade_app/models/models.dart';
import 'package:trade_app/repositories/db_dataprovider/app_database.dart';

class ProductsDataProvider {
  // insert
  Future<int> insert(Product product) async {
    final db = await AppDatabase.instance.database;
    return db.insert(Product.table, product.toMap());
  }

  // insert batch
  Future insertBatch(List<Product> products) async {
    final db = await AppDatabase.instance.database;
    final batch = db.batch();
    products.forEach((p) => {batch.insert(Product.table, p.toMap())});
    return batch.commit(noResult: true);
  }

  // get
  Future<Product> get(int id) async {
    final db = await AppDatabase.instance.database;
    List<Map> maps = await db
        .query(Product.table, where: '${Product.colPid} = ?', whereArgs: [id]);
    if (maps.length > 0) {
      return Product.fromMap(maps.first);
    }
    return null;
  }

  // is product with name exist
  Future<bool> isProductNameExist(String name) async {
    final db = await AppDatabase.instance.database;
    List<Map> maps = await db.query(Product.table,
        where: 'LOWER(${Product.colPname}) = ?',
        whereArgs: [name.toLowerCase()]);
    return maps.length > 0;
  }

  // get all
  Future<List<Product>> getAll() async {
    final db = await AppDatabase.instance.database;
    List<Map> maps =
        await db.query(Product.table, orderBy: '${Product.colPid} DESC');
    if (maps.length > 0) {
      return maps.map((row) => Product.fromMap(row)).toList();
    }
    return List<Product>();
  }

  Future<List<Product>> search(String searchText) async {
    final db = await AppDatabase.instance.database;
    List<Map> maps = await db.query(Product.table,
        where: '${Product.colPname} LIKE ?',
        orderBy: '${Product.colPid} DESC',
        whereArgs: ['%$searchText%']);
    if (maps.length > 0) {
      return maps.map((row) => Product.fromMap(row)).toList();
    }
    return List<Product>();
  }

  // update
  Future<int> update(Product product) async {
    product = product.copyWith(qty: product.qty < 0 ? 0 : product.qty);
    final db = await AppDatabase.instance.database;
    var prod = product.toMap();
    prod.remove(Product.colDateCreated);
    return db.update(
      Product.table,
      prod,
      where: '${Product.colPid} = ?',
      whereArgs: [product.pid],
    );
  }

  Future<int> delete(int id) async {
    final db = await AppDatabase.instance.database;
    return db
        .delete(Product.table, where: '${Product.colPid} = ?', whereArgs: [id]);
  }

  Future<void> truncate() async {
    final db = await AppDatabase.instance.database;
    //return db.delete(Product.table, where: '1');
    Batch batch = db.batch();
    batch.execute('''
              DELETE FROM ${Product.table}
              ''');
    batch.execute('''
              UPDATE SQLITE_SEQUENCE SET SEQ=0 WHERE NAME='${Product.table}'
              ''');
    await batch.commit(noResult: true);
  }
}
