import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:trade_app/blocs/connectivity_bloc/connectivity_bloc.dart';

class ConnectivityOfflineBanner extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ConnectivityBloc, ConnectivityState>(
        builder: (context, state) {
      if (state is ConnectivityOffline) {
        return Card(
          elevation: 4,
          shadowColor: Colors.black,
          shape: ContinuousRectangleBorder(),
          margin: EdgeInsets.fromLTRB(0,0,0,4),
                  child: Container(
            padding: EdgeInsets.symmetric(horizontal: 8, vertical: 4),
            child: Center(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(right: 8),
                    child: Icon(Icons.signal_wifi_off,
                        size: 14,
                        color: Theme.of(context).errorColor.withOpacity(0.8)),
                  ),
                  Text("No internet access",
                      style: Theme.of(context).textTheme.bodyText2.copyWith(
                          color: Theme.of(context).errorColor.withOpacity(.8), fontSize: 12))
                ],
              ),
            ),
          ),
        );
      } else {
        return Padding(padding: EdgeInsets.zero);
      }
    });
  }
}
