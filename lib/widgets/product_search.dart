import 'package:flutter/material.dart';
import 'package:trade_app/models/models.dart';
import 'package:trade_app/utilities/utilities.dart';
import 'package:trade_app/widgets/widgets.dart';

class ProductSearch extends StatelessWidget {
  final Product selectedItem;
  final List<Product> products;
  final Function(Product) onChange;

  ProductSearch({@required this.selectedItem, @required this.products, @required this.onChange});

  @override
  Widget build(BuildContext context) {
    return DropdownSearch<Product>(
      validator: (v) => v == null ? "Product is required" : null,
      hint: "Select a product *",
      mode: Mode.BOTTOM_SHEET,
      showSelectedItem: true,
      showSearchBox: true,
      selectedItem: selectedItem,
      dropDownButton: Icon(
        Icons.arrow_drop_down,
        color: Theme.of(context).textTheme.headline4.color,
      ),
      popupItemBuilder: (context, product, isSelected) {
        return Container(
          color:
              isSelected ? Theme.of(context).primaryColor : Colors.transparent,
          child: ListTile(
            title: Text(
              product.pname,
              style:
                  Theme.of(context).textTheme.headline4.copyWith(fontSize: 14),
            ),
            dense: true,
            trailing: Container(
                padding: EdgeInsets.fromLTRB(4, 2, 4, 0),
                decoration: BoxDecoration(
                    border: Border.all(
                        color: Theme.of(context)
                            .textTheme
                            .headline4
                            .color
                            .withOpacity(0.7)),
                    borderRadius: BorderRadius.all(Radius.circular(5))),
                child: Text(ProductVM.fromProduct(product).displayPrice,
                    style: Theme.of(context)
                        .textTheme
                        .headline4
                        .copyWith(fontSize: 12))),
          ),
        );
      },
      dropdownBuilder: (context, product, selectedVal) {
        return product == null
            ? Text('Select product *',
                style: TextStyle(
                    color: Theme.of(context)
                        .textTheme
                        .headline4
                        .color
                        .withOpacity(.5)))
            : Text(product.pname, style: TextStyle(
                    color: Theme.of(context)
                        .textTheme
                        .headline4
                        .color
                        .withOpacity(.7)),);
      },
      searchBoxDecoration: Styles.getSearchBoxDecoration(context,
              showClearButton: false)
          .copyWith(
              enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(
                      color: Theme.of(context)
                          .textTheme
                          .headline4
                          .color
                          .withOpacity(.6))),
              focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Theme.of(context).accentColor)),
              contentPadding: EdgeInsets.symmetric(horizontal: 8, vertical: 8),
              prefixIcon: Icon(Icons.search),
              prefixIconConstraints:
                  BoxConstraints(maxHeight: 20, minWidth: 32)),
      dropdownSearchDecoration: Styles.getInputDecoration(context).copyWith(
          contentPadding: EdgeInsets.only(left: 16),
          isDense: true,
          hintStyle:
              Theme.of(context).textTheme.headline4.copyWith(fontSize: 14)),
      items: products,
      itemAsString: (p) => p.pname,
      compareFn: (p1, p2) => p1 == p2,
      onChanged: onChange,
    );
  }
}
