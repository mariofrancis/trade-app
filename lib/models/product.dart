import 'package:equatable/equatable.dart';

class Product extends Equatable {
  final int id;
  final int pid;
  final String pname;
  final double price;
  final int qty;
  final int pcs;
  final int dateCreated;

  // constructor
  Product(
      {this.id,
      this.pid,
      this.pname,
      this.price,
      this.qty,
      this.pcs,
      this.dateCreated});

  @override
  List<Object> get props => [id, pid, pname, price, qty, pcs, dateCreated];

  // constants
  static const table = 'products';
  static const colId = 'id';
  static const colPid = 'pid';
  static const colPname = 'pname';
  static const colPrice = 'price';
  static const colQty = 'qty';
  static const colPcs = 'pcs';
  static const colDateCreated = 'date_created';

  static Product fromMap(Map<String, dynamic> map) {
    return Product(
        id: int.parse(map[colId].toString()),
        pid: int.parse(map[colPid].toString()),
        pname: map[colPname],
        price: double.parse(map[colPrice].toString()),
        qty: int.parse(map[colQty].toString()),
        pcs: int.parse(map[colPcs].toString()),
        dateCreated: int.parse(map[colDateCreated].toString()));
  }

  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{
      colPid: pid,
      colPname: pname,
      colPrice: price,
      colQty: qty,
      colPcs: pcs,
      colDateCreated: dateCreated
    };
    if (id != null) {
      map[colId] = id;
    }
    return map;
  }

  Product copyWith(
      {int id,
      int pid,
      String pname,
      double price,
      int qty,
      int pcs,
      int dateCreated}) {
    return Product(
        id: id ?? this.id,
        pid: pid ?? this.pid,
        pname: pname ?? this.pname,
        price: price ?? this.price,
        qty: qty ?? this.qty,
        pcs: pcs ?? this.pcs,
        dateCreated: dateCreated ?? this.dateCreated);
  }
}
