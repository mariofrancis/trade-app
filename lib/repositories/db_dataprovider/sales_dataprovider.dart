import 'package:sqflite/sqflite.dart';
import 'package:trade_app/models/models.dart';
import 'package:trade_app/repositories/db_dataprovider/app_database.dart';

class SalesDataProvider {
  // insert
  Future<int> insert(Sale sale) async {
    final db = await AppDatabase.instance.database;
    return db.insert(Sale.table, sale.toMap());
  }

  // get
  Future<Sale> get(int id) async {
    final db = await AppDatabase.instance.database;
    List<Map> maps =
        await db.query(Sale.table, where: '${Sale.colId} = ?', whereArgs: [id]);
    if (maps.length > 0) {
      return Sale.fromMap(maps.first);
    }
    return null;
  }

  // get all
  Future<List<Sale>> getAll() async {
    final db = await AppDatabase.instance.database;
    List<Map> maps = await db.query(Sale.table, orderBy: '${Sale.colId} DESC');
    if (maps.length > 0) {
      return maps.map((row) => Sale.fromMap(row)).toList();
    }
    return List<Sale>();
  }

  // update
  Future<int> update(Sale sale) async {
    final db = await AppDatabase.instance.database;
    return db.update(
      Sale.table,
      sale.toMap(),
      where: '${Sale.colId} = ?',
      whereArgs: [sale.id],
    );
  }

  Future<int> delete(int id) async {
    final db = await AppDatabase.instance.database;
    return db.delete(Sale.table, where: '${Sale.colId} = ?', whereArgs: [id]);
  }

  Future<void> truncate() async {
    final db = await AppDatabase.instance.database;
    //return db.delete(Product.table, where: '1');
    Batch batch = db.batch();
    batch.execute('''
              DELETE FROM ${Sale.table}
              ''');
    batch.execute('''
              UPDATE SQLITE_SEQUENCE SET SEQ=0 WHERE NAME='${Sale.table}'
              ''');
    await batch.commit(noResult: true);
  }
}
