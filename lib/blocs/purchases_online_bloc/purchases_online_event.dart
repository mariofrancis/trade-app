part of 'purchases_online_bloc.dart';

abstract class PurchasesOnlineEvent extends Equatable {
  const PurchasesOnlineEvent();
}
class FetchPurchasesOnline extends PurchasesOnlineEvent{
  final String month;

  FetchPurchasesOnline({@required this.month}):assert(month!=null);

  @override
  List<Object> get props => [month];
}
class RefreshPurchasesOnline extends PurchasesOnlineEvent{
  final String month;

  RefreshPurchasesOnline({@required this.month}):assert(month!=null);

  @override
  List<Object> get props => [month];
}