import 'dart:async';
import 'dart:io';
import 'package:meta/meta.dart';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:trade_app/blocs/blocs.dart';
import 'package:trade_app/models/models.dart';
import 'package:trade_app/repositories/repositories.dart';

part 'purchases_online_event.dart';
part 'purchases_online_state.dart';

class PurchasesOnlineBloc
    extends Bloc<PurchasesOnlineEvent, PurchasesOnlineState> {
  final ApiRepository apiRepo;
  final DbRepository dbRepo;
  final ProductsBloc productsBloc;
  final SettingsBloc settingsBloc;
  String month='';

  PurchasesOnlineBloc(
      {@required this.apiRepo,
      @required this.dbRepo,
      @required this.productsBloc,
      @required this.settingsBloc})
      : assert(apiRepo != null && dbRepo != null && productsBloc != null && settingsBloc!=null);

  @override
  PurchasesOnlineState get initialState => PurchasesOnlineEmpty();

  @override
  Stream<PurchasesOnlineState> mapEventToState(
    PurchasesOnlineEvent event,
  ) async* {
    if (event is FetchPurchasesOnline) {
      yield* _fetchPurchases(event);
    } else if (event is RefreshPurchasesOnline) {
      yield* _refreshPurchases(event);
    }
  }

// =============== private methods ==========================
  Stream<PurchasesOnlineState> _fetchPurchases(
      FetchPurchasesOnline event) async* {
    yield PurchasesOnlineLoading();
    try {
      final purchases =
          await apiRepo.purchasesDataProvider.getRecords(month: event.month, inCartons: settingsBloc.setting.displayCarton==1?true:false);
      if (purchases.dayRecords.isEmpty) {
        month='';
        yield PurchasesOnlineEmpty();
      } else {
        month = event.month;
        yield PurchasesOnlineLoaded(purchases: purchases);
      }
    } catch (ex) {
      if (ex is TimeoutException) {
        yield PurchasesOnlineFetchError(
            message:
                'Request timed out! Ensure you have a strong network signal');
      } else if (ex is SocketException) {
        yield PurchasesOnlineFetchError(
            message:
                'Unable to connect to the application server! Ensure you have entered the correct url in settings and also ensure you have a strong network signal');
      } else {
        yield PurchasesOnlineFetchError(message: ex.toString());
      }
    }
  }

  Stream<PurchasesOnlineState> _refreshPurchases(
      RefreshPurchasesOnline event) async* {
    yield PurchasesOnlineRefreshLoading();
    try {
      final purchases =
          await apiRepo.purchasesDataProvider.getRecords(month: event.month, inCartons: settingsBloc.setting.displayCarton==1?true:false);
      if (purchases.dayRecords.isEmpty) {
        month='';
        yield PurchasesOnlineEmpty();
      } else {
        month = event.month;
        yield PurchasesOnlineLoaded(purchases: purchases);
      }
    } catch (ex) {
      if (ex is TimeoutException) {
        yield PurchasesOnlineFetchError(
            message:
                'Request timed out! Ensure you have a strong network signal');
      } else if (ex is SocketException) {
        yield PurchasesOnlineFetchError(
            message:
                'Unable to connect to the application server! Ensure you have entered the correct url in settings and also ensure you have a strong network signal');
      } else {
        yield PurchasesOnlineFetchError(message: ex.toString());
      }
    }
  }
}
