import 'package:flutter/material.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:trade_app/widgets/widgets.dart';

enum AlertType { info, error, success }

class Dialogs {
  static Future<void> alertDialog(BuildContext context, String message,
      {AlertType type}) async {
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Row(
              children: <Widget>[
                _getIcon(
                    type: type,
                    color: Theme.of(context).textTheme.headline4.color),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 8),
                  child: Text(
                    _getTitle(type: type),
                    style: Theme.of(context).textTheme.headline4.copyWith(
                        fontSize: 16,
                        color: Theme.of(context).textTheme.headline4.color),
                  ),
                )
              ],
            ),
            content: Text(
              message,
              style:
                  Theme.of(context).textTheme.headline4.copyWith(fontSize: 14),
            ),
            actions: <Widget>[
              SizedBox(
                  height: 25,
                  child: RaisedButton(
                    child: Text('OK', style: TextStyle(fontSize: 10)),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  ))
            ],
          );
        });
  }

  static Future<bool> confirmDialog(BuildContext context, String message,
      {String title, String subtitle, List<String> buttonLabels}) {
    return showDialog<bool>(
        context: context,
        builder: (context) {
          return AlertDialog(
            contentPadding: EdgeInsets.symmetric(horizontal: 28, vertical: 8),
            title: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Icon(Icons.help_outline,
                        color: Theme.of(context).textTheme.headline4.color),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 8),
                      child: Text(
                        title ?? 'Confirm Action',
                        style: Theme.of(context)
                            .textTheme
                            .headline4
                            .copyWith(fontSize: 16),
                      ),
                    )
                  ],
                ),
                Padding(
                  padding: EdgeInsets.only(top: 8, left: 4),
                  child: subtitle != null
                      ? Text(
                          subtitle,
                          style: Theme.of(context).textTheme.headline4.copyWith(
                              fontSize: 12,
                              height: 1.3,
                              color: Theme.of(context)
                                  .textTheme
                                  .headline4
                                  .color
                                  .withOpacity(.8)),
                        )
                      : Container(),
                )
              ],
            ),
            content: Text(
              message,
              style: Theme.of(context).textTheme.subtitle1.copyWith(
                  color: Theme.of(context).textTheme.headline4.color,
                  fontSize: 14),
            ),
            actions: <Widget>[
              FlatButton(
                child: Text(buttonLabels != null ? buttonLabels[0] : 'Cancel',
                    style: TextStyle(fontSize: 10)),
                onPressed: () {
                  Navigator.of(context).pop(false);
                },
              ),
              SizedBox(
                  height: 25,
                  child: RaisedButton(
                    child: Text(buttonLabels != null ? buttonLabels[1] : 'OK',
                        style: TextStyle(fontSize: 10)),
                    onPressed: () {
                      Navigator.of(context).pop(true);
                    },
                  ))
            ],
          );
        });
  }

  static ProgressDialog getLoaderDialog(BuildContext context,
      {String initialMessage}) {
    var pr = ProgressDialog(context, isDismissible: false);
    pr.style(
        backgroundColor: Theme.of(context).cardColor,
        message: initialMessage ?? 'Loading...',
        progressWidget: Padding(
          padding: const EdgeInsets.all(12.0),
          child: CircularProgressIndicator(),
        ),
        messageTextStyle: Theme.of(context).textTheme.headline4.copyWith(
            fontSize: 14, color: Theme.of(context).textTheme.headline4.color));
    return pr;
  }

  static Future<bool> getAuthDialog(BuildContext context) {
    return showDialog<bool>(
        context: context, builder: (context) => Authenticate());
  }

  static Widget _getIcon({AlertType type, Color color}) {
    Icon icon;
    switch (type) {
      case AlertType.success:
        icon = Icon(Icons.check_circle_outline, color: color);
        break;
      case AlertType.error:
        icon = Icon(Icons.error_outline, color: color);
        break;
      default:
        icon = Icon(Icons.info_outline, color: color);
        break;
    }
    return icon;
  }

  static String _getTitle({AlertType type}) {
    String title;
    switch (type) {
      case AlertType.success:
        title = 'Success';
        break;
      case AlertType.error:
        title = 'Error';
        break;
      default:
        title = 'Info';
        break;
    }
    return title;
  }
}
