import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:trade_app/utilities/utilities.dart';
import 'package:trade_app/widgets/widgets.dart';
import 'package:trade_app/blocs/blocs.dart' as blocs;

class Menu {
  final String title;
  final String imagePath;
  final blocs.Page page;

  Menu({@required this.title, @required this.imagePath, @required this.page});
}

class Home extends StatelessWidget {
  final List<Menu> menus = [
    Menu(
        title: 'Products',
        imagePath: 'assets/images/product.png',
        page: blocs.Page.Products),
    Menu(
        title: 'Purchase Records',
        imagePath: 'assets/images/record.png',
        page: blocs.Page.Purchases),
    Menu(
        title: 'Sale Records',
        imagePath: 'assets/images/record2.png',
        page: blocs.Page.Sales),
    Menu(
        title: 'Sync Data',
        imagePath: 'assets/images/cloud.png',
        page: blocs.Page.Sync),
    Menu(
        title: 'Settings',
        imagePath: 'assets/images/setting.png',
        page: blocs.Page.Settings),
  ];

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () => _onBackButtonPress(context),
      child: BlocBuilder<blocs.HomeBloc, blocs.HomeState>(
          builder: (context, homeState) {
        return Scaffold(
            drawer: Drawer(
                child: Container(
                    child: ListView.builder(
                        padding: EdgeInsets.all(0),
                        itemCount: menus.length + 1,
                        itemBuilder: _itemBuilder))),
            appBar: AppBar(
              title: Text(_getTitle(homeState)),
            ),
            body: BlocListener<blocs.ConnectivityBloc, blocs.ConnectivityState>(
              condition: (prevState, newState) =>
                  !(newState is blocs.ConnectivityOffline),
              listener: (context, state) {
                if (state is blocs.ConnectivityOnline) {
                  if (state.message != null) {
                    Scaffold.of(context).showSnackBar(SnackBar(
                      content: Text('Sync Error: ' + state.message),
                    ));
                  } else {
                    BlocProvider.of<blocs.ConnectivityBloc>(context)
                        .add(blocs.SyncOfflineData());
                  }
                } else if (state is blocs.OfflineDataSynced) {
                  BlocProvider.of<blocs.ProductsBloc>(context).add(
                      blocs.RefreshProducts(isOnline: true, searchText: ''));
                  BlocProvider.of<blocs.PurchasesOnlineBloc>(context)
                      .add(blocs.RefreshPurchasesOnline(month: ''));
                  BlocProvider.of<blocs.PurchasesOfflineBloc>(context)
                      .add(blocs.RefreshPurchasesOffline());
                  BlocProvider.of<blocs.SalesOnlineBloc>(context)
                      .add(blocs.RefreshSalesOnline(month: ''));
                  BlocProvider.of<blocs.SalesOfflineBloc>(context)
                      .add(blocs.RefreshSalesOffline());

                  Scaffold.of(context).showSnackBar(SnackBar(
                    content:
                        Text('Offline data were synchronised successfully!'),
                  ));
                }
              },
              child: Column(
                children: <Widget>[
                  ConnectivityOfflineBanner(),
                  Expanded(child: _renderPage(context, homeState))
                ],
              ),
            ));
      }),
    );
  }

  String _getTitle(blocs.HomeState state) {
    if (state is blocs.PageLoaded) {
      return menus.firstWhere((e) => e.page == state.page).title;
    } else {
      return 'Home';
    }
  }

  Widget _renderPage(BuildContext context, blocs.HomeState state) {
    if (state is blocs.PageLoaded) {
      Widget widget = Center(child: Text('Not yet implemented!'));
      switch (state.page) {
        case blocs.Page.Products:
          widget = Products();
          break;
        case blocs.Page.Purchases:
          widget = Purchases();
          break;
        case blocs.Page.Sales:
          widget = Sales();
          break;
        case blocs.Page.Sync:
          widget = SyncData();
          break;
        case blocs.Page.Settings:
          widget = Settings(
              setting: BlocProvider.of<blocs.SettingsBloc>(context).setting);
          break;
        default:
      }
      return widget;
    } else {
      return Center(
          child: CircularProgressIndicator(
        backgroundColor: Theme.of(context).primaryColor,
      ));
    }
  }

  Widget _itemBuilder(BuildContext context, int itemIndex) {
    if (itemIndex == 0) {
      return DrawerHeader(
        decoration: BoxDecoration(
            // color: Theme.of(context).primaryColor,
            // image: DecorationImage(
            //     image: AssetImage('assets/images/img1.jpg'),
            //     fit: BoxFit.cover,
            //     colorFilter: ColorFilter.mode(
            //         Theme.of(context).accentColor, BlendMode.darken)),
            gradient: LinearGradient(colors: [
              Theme.of(context).primaryColor,
              Theme.of(context).accentColor
            ])),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text('Trade',
                style: GoogleFonts.openSans(
                    textStyle: TextStyle(
                        color: Colors.white,
                        fontSize: 24,
                        letterSpacing: 2,
                        fontWeight: FontWeight.w600,
                        shadows: [
                      Shadow(color: Colors.black54, blurRadius: 2)
                    ]))),
            Container(
                decoration: BoxDecoration(
                    color: Theme.of(context).scaffoldBackgroundColor,
                    boxShadow: [
                      BoxShadow(
                          color: Colors.black54,
                          offset: Offset.fromDirection(1),
                          blurRadius: 2)
                    ]),
                padding: EdgeInsets.symmetric(vertical: 2, horizontal: 8),
                child: Text('MANAGER',
                    style: GoogleFonts.openSans(
                        textStyle: TextStyle(
                            color: Theme.of(context).accentColor,
                            letterSpacing: 2,
                            fontSize: 24,
                            fontWeight: FontWeight.w600))))
          ],
        ),
      );
    } else {
      var i = itemIndex - 1;
      return ListTile(
        dense: true,
        title: Text(
          menus[i].title,
          style: Theme.of(context).textTheme.bodyText1.copyWith(
              fontWeight: FontWeight.w500,
              color:
                  Theme.of(context).textTheme.bodyText1.color.withOpacity(.7)),
        ),
        leading: Image(
            image: AssetImage(menus[i].imagePath),
            width: 25,
            alignment: Alignment.center),
        trailing: Icon(Icons.chevron_right),
        onTap: () async {
          if (menus[i].title == 'Settings') {
            var isAuthorized = await Dialogs.getAuthDialog(context);
            if (!isAuthorized) {
              await Dialogs.alertDialog(context, 'Access denied',
                  type: AlertType.error);
              Navigator.pop(context);
            } else {
              Navigator.pop(context);
              BlocProvider.of<blocs.HomeBloc>(context)
                  .add(blocs.LoadPage(page: menus[i].page));
              
            }
          } else {
            Navigator.pop(context);
            BlocProvider.of<blocs.HomeBloc>(context)
                .add(blocs.LoadPage(page: menus[i].page));
            
          }
        },
      );
    }
  }

  Future<bool> _onBackButtonPress(BuildContext context) async {
    var res = await Dialogs.confirmDialog(
        context, 'Are you sure you want to exit this application?');
    if (res) {
      SystemNavigator.pop();
      return true;
    }
    return false;
  }
}
