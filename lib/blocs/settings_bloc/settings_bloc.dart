import 'dart:async';
import 'package:meta/meta.dart';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:trade_app/models/models.dart';
import 'package:trade_app/repositories/api_repository.dart';
import 'package:trade_app/repositories/db_repository.dart';

part 'settings_event.dart';
part 'settings_state.dart';

class SettingsBloc extends Bloc<SettingsEvent, SettingsState> {
  final DbRepository dbRepo;
  final ApiRepository apiRepo;
  Setting setting;

  SettingsBloc({@required this.dbRepo, @required this.apiRepo, this.setting}) : assert(dbRepo != null && apiRepo!=null);

  @override
  SettingsState get initialState => SettingsLoaded(setting: setting);

  @override
  Stream<SettingsState> mapEventToState(
    SettingsEvent event,
  ) async* {
    if (event is FetchSettings) {
      var _setting = await dbRepo.settingsDataProvider.get(1);
      setting = _setting;
      yield SettingsLoaded(setting: _setting);
    }
  }
}
