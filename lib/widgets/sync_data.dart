import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:trade_app/blocs/blocs.dart';
import 'package:trade_app/models/models.dart';
import 'package:trade_app/utilities/utilities.dart';
import 'package:trade_app/widgets/widgets.dart';

class SyncData extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GradientWithCardContainer(
          child: Center(
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 36),
            child: Column(
              children: <Widget>[
                Icon(
                  Icons.cloud,
                  size: 120,
                  color: Theme.of(context).textTheme.headline4.color,
                ),
                Text(
                  'Synchronize data with cloud',
                  style: Theme.of(context)
                      .textTheme
                      .headline4
                      .copyWith(fontWeight: FontWeight.w500),
                  textScaleFactor: .58,
                ),
                Padding(padding: EdgeInsets.symmetric(vertical: 4)),
                Text(
                  'Data synchronization is important as it ensures your data are consistent both in your mobile device and cloud.',
                  style: GoogleFonts.openSans(
                      textStyle: Theme.of(context)
                          .textTheme
                          .headline4
                          .copyWith(fontWeight: FontWeight.w500, height: 1.3)),
                  textScaleFactor: .4,
                  textAlign: TextAlign.center,
                ),
                Padding(padding: EdgeInsets.symmetric(vertical: 16)),
                BlocBuilder<ConnectivityBloc, ConnectivityState>(
                  builder: (context, state) {
                    return RaisedButton.icon(
                      onPressed: state is ConnectivityOnline
                          ? () {
                              _syncData(context);
                            }
                          : null,
                      icon: Icon(
                        Icons.sync,
                        size: 18,
                      ),
                      label: Text('Initiate Data Sync',
                          style: GoogleFonts.openSans()),
                      colorBrightness: Theme.of(context).accentColorBrightness,
                    );
                  },
                ),
              ],
            ),
          ),
        ),
      )),
    );
  }

  void _syncData(BuildContext context) async {
    var loader =
        Dialogs.getLoaderDialog(context, initialMessage: 'Synchronizing...')
          ..show();
    try {
      var apiRepo = BlocProvider.of<ProductsBloc>(context).apiRepo;
      var dbRepo = BlocProvider.of<ProductsBloc>(context).dbRepo;

      var purchases = await dbRepo.purchasesDataProvider.getAll();
      var sales = await dbRepo.salesDataProvider.getAll();

      if (purchases.length > 0) {
        await apiRepo.purchasesDataProvider.insertBatch(
            purchases.map((p) => ApiPurchase.fromPurchase(p)).toList());
        dbRepo.purchasesDataProvider.truncate();
      }
      if (sales.length > 0) {
        await apiRepo.salesDataProvider
            .insertBatch(sales.map((s) => ApiSale.fromSale(s)).toList());
        dbRepo.salesDataProvider.truncate();
      }

      BlocProvider.of<ProductsBloc>(context).add(FetchProducts(isOnline: true));

      var setting = await dbRepo.settingsDataProvider.get(1);
      await apiRepo.settingsDataProvider.update(ApiSetting(
          pswd: setting.pswd,
          sqstn: setting.sqstn,
          sans: setting.sans,
          url: setting.url));
      await loader.hide();
      await _refreshApp(context);
      await Dialogs.alertDialog(
          context, 'App data was synchronized successfully.',
          type: AlertType.success);
    } catch (ex) {
      var msg = ex is TimeoutException
          ? 'Request timed out! Ensure you have a strong network signal'
          : ex.toString();
      loader.hide();
      Dialogs.alertDialog(context, msg, type: AlertType.error);
    }
  }

  Future<void> _refreshApp(BuildContext context) async {
    BlocProvider.of<ProductsBloc>(context)
        .add(RefreshProducts(isOnline: false, searchText: ''));
    BlocProvider.of<PurchasesOfflineBloc>(context)
        .add(RefreshPurchasesOffline());
    BlocProvider.of<SalesOfflineBloc>(context).add(RefreshSalesOffline());
  }
}
