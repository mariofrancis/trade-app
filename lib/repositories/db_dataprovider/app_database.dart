import 'dart:async';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'package:trade_app/models/models.dart';

class AppDatabase {
  // This is the actual database filename that is saved in the docs directory.
  final _databaseName = "trade.db";
  // Increment this version when you need to change the schema.
  final _databaseVersion = 1;
  Database _database;

  // Make this a singleton class.
  AppDatabase._privateConstructor();
  static final AppDatabase instance = AppDatabase._privateConstructor();

  // exposed database
  Future<Database> get database async {
    if (_database == null) _database = await _initDatabase();
    return _database;
  }

  Future<Database> _initDatabase() async {
    return openDatabase(
      join(await getDatabasesPath(), _databaseName),
      onCreate: _onCreate,
      version: _databaseVersion,
    );
  }

  Future<void> _onCreate(Database db, int dbVersion) async {
    // Run the CREATE TABLE statement on the database.
    Batch batch = db.batch();
    batch.execute('''
              CREATE TABLE ${Setting.table}(
                ${Setting.colId} INTEGER PRIMARY KEY AUTOINCREMENT,
                ${Setting.colPswd} TEXT NOT NULL,
                ${Setting.colSqstn} TEXT NOT NULL,
                ${Setting.colSans} TEXT NOT NULL,
                ${Setting.colUrl} TEXT NOT NULL,
                ${Setting.colKey} TEXT NOT NULL,
                ${Setting.colTheme} INTEGER DEFAULT 0 NOT NULL,
                ${Setting.colDisplayCarton} INTEGER DEFAULT 0 NOT NULL
              )
              ''');
    batch.execute('''
              CREATE TABLE ${Product.table}(
                ${Product.colId} INTEGER PRIMARY KEY AUTOINCREMENT,
                ${Product.colPid} INTEGER NOT NULL,
                ${Product.colPname} TEXT NOT NULL,
                ${Product.colPrice} DECIMAL(10, 2) NOT NULL,
                ${Product.colQty} INTEGER NOT NULL,
                ${Product.colPcs} INTEGER NOT NULL,
                ${Product.colDateCreated} INTEGER NOT NULL
              )
              ''');
    batch.execute('''
              CREATE TABLE ${Purchase.table}(
                ${Purchase.colId} INTEGER PRIMARY KEY AUTOINCREMENT,
                ${Purchase.colPid} INTEGER NOT NULL,
                ${Purchase.colPrice} DECIMAL(10, 2) NOT NULL,
                ${Purchase.colQty} INTEGER NOT NULL,
                ${Purchase.colDay} TEXT NOT NULL,
                ${Purchase.colMonth} TEXT NOT NULL,
                ${Purchase.colDateCreated} INTEGER NOT NULL
              )
              ''');
    batch.execute('''
              CREATE TABLE ${Sale.table}(
                ${Sale.colId} INTEGER PRIMARY KEY AUTOINCREMENT,
                ${Sale.colPid} INTEGER NOT NULL,
                ${Sale.colPrice} DECIMAL(10, 2) NOT NULL,
                ${Sale.colQty} INTEGER NOT NULL,
                ${Sale.colDay} TEXT NOT NULL,
                ${Sale.colMonth} TEXT NOT NULL,
                ${Sale.colDateCreated} INTEGER NOT NULL
              )
              ''');
    await batch.commit(noResult: true);
  }
}
