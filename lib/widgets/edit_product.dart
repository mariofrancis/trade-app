import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:trade_app/blocs/blocs.dart';
import 'package:trade_app/models/models.dart';
import 'package:trade_app/utilities/utilities.dart';
import 'package:trade_app/widgets/connectivity_offline_banner.dart';
import 'package:trade_app/widgets/widgets.dart';

class EditProduct extends StatefulWidget {
  final BuildContext mainContext;
  final ProductVM product;

  EditProduct({@required this.mainContext, @required this.product});
  @override
  _EditProductState createState() =>
      _EditProductState(mainContext: mainContext, product: product);
}

class _EditProductState extends State<EditProduct> {
  final BuildContext mainContext;
  final ProductVM product;
  final _formKey = GlobalKey<FormState>();
  final _nameController = new TextEditingController();
  final _priceController = new TextEditingController();
  final _qtyController = new TextEditingController();
  final _pcsController = new TextEditingController();
  String _qtyUnit = 'piece';

  _EditProductState({@required this.mainContext, @required this.product});

  @override
  void initState() {
    super.initState();
    _nameController.text = product.pname;
    _pcsController.text = product.pcs.toString();
    var r = product.qty % product.pcs;
    if (r == 0) {
      _qtyUnit = 'carton';
      _qtyController.text =
          (int.parse((product.qty / product.pcs).round().toString()))
              .toString();
      _priceController.text = (product.price * product.pcs).toString();
    } else {
      _qtyUnit = 'piece';
      _qtyController.text = product.qty.toString();
      _priceController.text = product.price.toString();
    }
  }

  @override
  void dispose() {
    _nameController.dispose();
    _priceController.dispose();
    _qtyController.dispose();
    _pcsController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Edit Product'),
      ),
      body: Column(
        children: <Widget>[
          ConnectivityOfflineBanner(),
          Expanded(
            child: GradientContainer(
                child: Padding(
              padding:
                  const EdgeInsets.symmetric(horizontal: 4.0),
              child: _buildForm(context),
            )),
          ),
        ],
      ),
    );
  }

  // get form
  Widget _buildForm(BuildContext context) {
    return Form(
        key: _formKey,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: <Widget>[
            Expanded(
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Padding(padding: EdgeInsets.only(top:32)),
                    // product name
                    FormFieldCard(
                                          child: TextFormField(
                        controller: _nameController,
                        decoration: Styles.getInputDecoration(context,
                            label: 'Name *', hint: 'What is the product name?'),
                        style: Styles.getInputTextStyle(context).copyWith(
                            decorationColor: Theme.of(context)
                                .primaryColorDark
                                .withOpacity(.5)),
                        validator: (value) {
                          if (value.trim().isEmpty) {
                            return 'Product name is required';
                          }
                          return null;
                        },
                      ),
                    ),
                    Padding(padding: EdgeInsets.symmetric(vertical: 12)),
                    // Unit price
                    FormFieldCard(
                                          child: TextFormField(
                          controller: _priceController,
                          decoration: Styles.getInputDecoration(context,
                              label: 'Unit Price *',
                              hint: 'What is the price for a piece?'),
                          style: Styles.getInputTextStyle(context).copyWith(
                              decorationColor: Theme.of(context)
                                  .primaryColorDark
                                  .withOpacity(.5)),
                          keyboardType: TextInputType.number,
                          validator: (value) {
                            if (value.trim().isEmpty) {
                              return 'Unit price is required';
                            }
                            return null;
                          }),
                    ),
                    Padding(padding: EdgeInsets.symmetric(vertical: 12)),
                    Padding(
                      padding: const EdgeInsets.only(bottom: 4, left: 4),
                      child: Text(
                        'Available Quantity',
                        style: Theme.of(context).textTheme.headline4.copyWith(
                              fontSize: 12, fontWeight: FontWeight.w500
                            ),
                      ),
                    ),
                    Row(
                      children: <Widget>[
                        Expanded(
                          flex: 1,
                          child: FormFieldCard(
                                                      child: DropdownButtonFormField<String>(
                              value: _qtyUnit,
                              decoration: Styles.getInputDecoration(context, label: 'Qty Unit *'),
                              items: [
                                DropdownMenuItem(
                                  child: Text('Piece'),
                                  value: 'piece',
                                ),
                                DropdownMenuItem(
                                  child: Text('Carton'),
                                  value: 'carton',
                                )
                              ],
                              style: Styles.getInputTextStyle(context).copyWith(
                              decorationColor: Theme.of(context)
                                  .primaryColorDark
                                  .withOpacity(.5)),
                              onChanged: (val) {
                                setState(() {
                                  _qtyUnit = val;
                                });
                              },
                            ),
                          ),
                        ),
                        Padding(padding: EdgeInsets.symmetric(horizontal: 4)),
                        Expanded(
                          flex: 2,
                          child: FormFieldCard(
                                                      child: TextFormField(
                                controller: _qtyController,
                                decoration: Styles.getInputDecoration(context,
                                    label: 'Quantity *',
                                    hint: 'Quantity in pieces/cartons'),
                                style: Styles.getInputTextStyle(context).copyWith(
                                    decorationColor: Theme.of(context)
                                        .primaryColorDark
                                        .withOpacity(.5)),
                                keyboardType: TextInputType.numberWithOptions(decimal: false),
                                validator: (value) {
                                  if (value.trim().isEmpty) {
                                    return 'Total quantity is required';
                                  }
                                  return null;
                                }),
                          ),
                        )
                      ],
                    ),
                    Padding(padding: EdgeInsets.symmetric(vertical: 12)),
                    // Unit price
                    FormFieldCard(
                                          child: TextFormField(
                          controller: _pcsController,
                          decoration: Styles.getInputDecoration(context,
                              label: 'Quantity per carton *',
                              hint: 'How many pieces are in a carton?'),
                          style: Styles.getInputTextStyle(context).copyWith(
                              decorationColor: Theme.of(context)
                                  .primaryColorDark
                                  .withOpacity(.5)),
                          keyboardType: TextInputType.number,
                          validator: (value) {
                            if (value.trim().isEmpty) {
                              return 'Quantity per carton is required';
                            }
                            return null;
                          }),
                    ),
                    Padding(padding: EdgeInsets.symmetric(vertical: 12)),
                  ],
                ),
              ),
            ),
            Divider(
              height: 2,
            ),
            RaisedButton.icon(
              onPressed: () {
                _onSubmit(context);
              },
              icon: Icon(
                Icons.check_circle_outline,
                size: 16,
              ),
              label: Text('Update Product'),
              colorBrightness: Theme.of(context).accentColorBrightness,
            )
          ],
        ));
  }

  Future<void> _onSubmit(BuildContext _context) async {
    if (_formKey.currentState.validate()) {
      var qty = int.parse(_qtyController.text);
      var pcs = int.parse(_pcsController.text);
      var price = double.parse(_priceController.text);
      var _product = ApiProduct(
          id: product.pid,
          pname: _nameController.text,
          price: _qtyUnit == 'piece' ? price : (price / pcs),
          pcs: pcs,
          qty: _qtyUnit == 'piece' ? qty : (qty * pcs));

      var isNameExist = await BlocProvider.of<ProductsBloc>(mainContext)
              .dbRepo
              .productsDataProvider
              .isProductNameExist(_product.pname) &&
          product.pname.toLowerCase() != _product.pname.toLowerCase();
      if (isNameExist) {
        Dialogs.alertDialog(
            context, "Product with name '${_product.pname}' already exist.",
            type: AlertType.info);
      } else {
        var connectivityState =
            BlocProvider.of<ConnectivityBloc>(mainContext).state;
        if (connectivityState is ConnectivityOffline) {
          Dialogs.alertDialog(context,
              "You're currently offline! Ensure your mobile device has internet access and try again.",
              type: AlertType.info);
        } else {
          // BlocProvider.of<EditProductBloc>(_context)
          //     .add(UpdateProduct(product: _product));
          _updateProduct(_product);
        }
      }
    }
  }

  void _updateProduct(ApiProduct product) async {
    var loader =
        Dialogs.getLoaderDialog(context, initialMessage: 'Updating product...')
          ..show();

    try {
      var apiRepo = BlocProvider.of<ProductsBloc>(context).apiRepo;
      var dbRepo = BlocProvider.of<ProductsBloc>(context).dbRepo;
      await apiRepo.productsDataProvider.update(product);
      await dbRepo.productsDataProvider.update(product.toProduct());
      BlocProvider.of<ProductsBloc>(context)
          .add(RefreshProducts(isOnline: false, searchText: ''));
      await loader.hide();
      _clearFields();
      await Dialogs.alertDialog(context, 'Product was updated successfully.',
          type: AlertType.success);
      Navigator.pop(context);
    } catch (ex) {
      var msg = ex is TimeoutException
          ? 'Request timed out! Ensure you have a strong network signal'
          : ex.toString();

      await loader.hide();
      await Dialogs.alertDialog(context, msg, type: AlertType.error);
    }
  }

  void _clearFields() {
    _nameController.clear();
    _priceController.clear();
    _qtyController.clear();
    _pcsController.clear();
    // setState(() {
    //   _qtyUnit = 'piece';
    // });
  }
}
