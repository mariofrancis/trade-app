part of 'connectivity_bloc.dart';

@immutable
abstract class ConnectivityState extends Equatable {
  @override
  List<Object> get props => [];
}

class ConnectivityInitial extends ConnectivityState {}

class ConnectivityOnline extends ConnectivityState {
  final String message;

  ConnectivityOnline({this.message});

  @override
  List<Object> get props => [message];
}

class ConnectivityOffline extends ConnectivityState {}

class OfflineDataSynced extends ConnectivityState {}
