import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class Styles {
  static InputDecoration getInputDecoration(BuildContext context,
      {String label, String hint}) {
    return InputDecoration(
      //enabledBorder: UnderlineInputBorder(borderSide: BorderSide(color: Theme.of(context).primaryColorDark.withOpacity(.3))),
      enabledBorder: InputBorder.none,
      disabledBorder: InputBorder.none,
      focusedBorder: InputBorder.none,
        labelText: label,
        hintText: hint,
        hintStyle: TextStyle(color: Theme.of(context).textTheme.headline4.color.withOpacity(.5)),
       //errorStyle: TextStyle(color: Colors.redAccent),
       errorBorder: UnderlineInputBorder(borderSide: BorderSide(color: Theme.of(context).errorColor.withOpacity(.5), width: .4)),
        isDense: false,
        contentPadding: EdgeInsets.only(top: 8, bottom: 4, left: 12, right: 12),
        //fillColor: Theme.of(context).primaryColorLight,
        //filled: true
        );
  }

  static TextStyle getInputTextStyle(BuildContext context) {
    return GoogleFonts.openSans(
        textStyle: Theme.of(context).textTheme.subtitle2.copyWith(decorationStyle: TextDecorationStyle.dotted, color: Theme.of(context).textTheme.headline4.color));
  }

  static InputDecoration getSearchBoxDecoration(BuildContext context,
      {String label, String hint, void Function() onClear, bool showClearButton=true}) {
        var txtStyle = getInputTextStyle(context);
    return InputDecoration(
      contentPadding: EdgeInsets.fromLTRB(4, 4, 4, 4),
      isDense: true,
      focusedBorder: InputBorder.none,
      enabledBorder: InputBorder.none,
      hintText: hint,
      hintStyle: txtStyle.copyWith(color: txtStyle.color.withOpacity(.5)),
      suffix: showClearButton? IconButton(
        icon: Icon(Icons.cancel),
        iconSize: 14,
        color: Theme.of(context).textTheme.headline4.color,
        padding: EdgeInsets.only(right: 4),
        constraints: BoxConstraints(maxHeight: 18),
        onPressed: onClear,
      ):null,
    );
  }
}
