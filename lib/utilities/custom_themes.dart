import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class ThemeItem extends Equatable {
  final int id;
  final String name;
  final Color color;

  ThemeItem({this.id, this.name, this.color});

  @override
  List<Object> get props => [id, name, color];
}

class CustomThemes {
  static List<ThemeItem> get themes => [
        ThemeItem(
            id: 1, color: Color(0xffcc913f), name: 'Raw Sienna (Default)'),
        ThemeItem(id: 2, color: ThemeData.dark().primaryColor, name: 'Dark'),
        ThemeItem(id: 3, color: Color(0xff404040), name: 'Tundora'),
        ThemeItem(id: 4, color: Color(0xff163840), name: 'Big Stone'),
        ThemeItem(id: 5, color: Color(0xff2E83F2), name: 'Blue Ribbon'),
        ThemeItem(id: 6, color: Color(0xff595959), name: 'Scorpion'),
        ThemeItem(id: 7, color: Color(0xff544C59), name: 'Mortar'),
        ThemeItem(id: 8, color: Color(0xffF23838), name: 'Pomegranate'),
      ];

  static getThemeData(BuildContext context, int id) {
    ThemeData themeData = _getRawSienna(context);
    switch (id) {
      case 2:
        themeData = _getDark(context);
        break;
      case 3:
        themeData = _getTundora(context);
        break;
      case 4:
        themeData = _getBigStone(context);
        break;
      case 5:
        themeData = _getBlueRibbon(context);
        break;
      case 6:
        themeData = _getScorpion(context);
        break;
      case 7:
        themeData = _getMortar(context);
        break;
        case 8:
        themeData = _getPomegranate(context);
        break;
      default:
        break;
    }
    return themeData;
  }

  static ThemeData _getRawSienna(BuildContext context) {
    var color = Color(0xffcc913f);
    var color2 = Color(0xff8C642B);
    return ThemeData(
        // Define the default brightness and colors.
        brightness: Brightness.light,
        primaryColor: color,
        accentColor: color2,
        buttonColor: color2,
        errorColor: Colors.redAccent,
        primarySwatch: Colors.grey,
        visualDensity: VisualDensity.adaptivePlatformDensity,
        textTheme: _getTextTheme(context));
  }

  static ThemeData _getTundora(BuildContext context) {
    var color = Color(0xff404040);
    var color2 = Color(0xff737373);
    return ThemeData(
        // Define the default brightness and colors.
        brightness: Brightness.light,
        primaryColor: color,
        accentColor: color2,
        buttonColor: color2,
        errorColor: Colors.redAccent,
        primarySwatch: Colors.grey,
        visualDensity: VisualDensity.adaptivePlatformDensity,
        textTheme: _getTextTheme(context));
  }

  static ThemeData _getBigStone(BuildContext context) {
    var color = Color(0xff163840);
    var color2 = Color(0xff6BB0BF);
    return ThemeData(
        // Define the default brightness and colors.
        brightness: Brightness.light,
        primaryColor: color,
        accentColor: color2,
        buttonColor: color2,
        errorColor: Colors.redAccent,
        primarySwatch: Colors.grey,
        visualDensity: VisualDensity.adaptivePlatformDensity,
        textTheme: _getTextTheme(context));
  }

  static ThemeData _getBlueRibbon(BuildContext context) {
    var color = Color(0xff2E83F2);
    var color2 = Color(0xff164DF2);

    return ThemeData(
        // Define the default brightness and colors.
        brightness: Brightness.light,
        primaryColor: color,
        accentColor: color2,
        buttonColor: color2,
        errorColor: Colors.redAccent,
        primarySwatch: Colors.grey,
        visualDensity: VisualDensity.adaptivePlatformDensity,
        textTheme: _getTextTheme(context));
  }

  static ThemeData _getScorpion(BuildContext context) {
    var color = Color(0xff595959);
    var color2 = Color(0xff262626);
    return ThemeData(
        // Define the default brightness and colors.
        brightness: Brightness.light,
        primaryColor: color,
        accentColor: color2,
        buttonColor: color2,
        errorColor: Colors.redAccent,
        primarySwatch: Colors.grey,
        visualDensity: VisualDensity.adaptivePlatformDensity,
        textTheme: _getTextTheme(context));
  }

  static ThemeData _getMortar(BuildContext context) {
    var color = Color(0xff544C59);
    var color2 = Color(0xffD2D9CC);
    return ThemeData(
        // Define the default brightness and colors.
        brightness: Brightness.light,
        primaryColor: color,
        accentColor: color2,
        buttonColor: color2,
        errorColor: Colors.redAccent,
        primarySwatch: Colors.grey,
        visualDensity: VisualDensity.adaptivePlatformDensity,
        textTheme: _getTextTheme(context));
  }

  static ThemeData _getPomegranate(BuildContext context) {
    var color = Color(0xffF23838);
    var color2 = Color(0xffD91414);
    return ThemeData(
        // Define the default brightness and colors.
        brightness: Brightness.light,
        primaryColor: color,
        accentColor: color2,
        buttonColor: color2,
        errorColor: Colors.redAccent,
        primarySwatch: Colors.grey,
        visualDensity: VisualDensity.adaptivePlatformDensity,
        textTheme: _getTextTheme(context));
  }

  static ThemeData _getDark(BuildContext context) {
    var color = ThemeData.dark().primaryColor;
    var color2 = ThemeData.dark().accentColor;
    return ThemeData.dark().copyWith(
        primaryColor: color,
        accentColor: color2,
        buttonColor: color2,
        errorColor: Colors.redAccent,
        visualDensity: VisualDensity.adaptivePlatformDensity,
        buttonTheme: ButtonThemeData(
          buttonColor: color2,
        ));
  }

  static TextTheme _getTextTheme(BuildContext context) {
    return TextTheme(
      headline1:
          GoogleFonts.roboto(textStyle: Theme.of(context).textTheme.headline1),
      headline2:
          GoogleFonts.roboto(textStyle: Theme.of(context).textTheme.headline2),
      headline3:
          GoogleFonts.roboto(textStyle: Theme.of(context).textTheme.headline3),
      headline4:
          GoogleFonts.roboto(textStyle: Theme.of(context).textTheme.headline4),
      headline5:
          GoogleFonts.roboto(textStyle: Theme.of(context).textTheme.headline5),
      headline6:
          GoogleFonts.roboto(textStyle: Theme.of(context).textTheme.headline6),
      subtitle1:
          GoogleFonts.roboto(textStyle: Theme.of(context).textTheme.subtitle1),
      subtitle2:
          GoogleFonts.roboto(textStyle: Theme.of(context).textTheme.subtitle2),
      bodyText1:
          GoogleFonts.roboto(textStyle: Theme.of(context).textTheme.bodyText1),
      bodyText2:
          GoogleFonts.roboto(textStyle: Theme.of(context).textTheme.bodyText2),
    );
  }
}
