import 'package:equatable/equatable.dart';

class Setting extends Equatable {
  // member fields
  final int id;
  final String pswd;
  final String sqstn;
  final String sans;
  final String url;
  final String key;
  final int theme;
  final int displayCarton;

  // constants
  static const table = 'settings';
  static const colId = 'id';
  static const colPswd = 'pswd';
  static const colSqstn = 'sqstn';
  static const colSans = 'sans';
  static const colUrl = 'url';
  static const colKey = 'key';
  static const colTheme = 'theme';
  static const colDisplayCarton = 'display_carton';

  // constructors
  Setting(
      {this.id,
      this.pswd,
      this.sqstn,
      this.sans,
      this.url,
      this.key,
      this.theme,
      this.displayCarton});

  @override
  List<Object> get props =>
      [id, pswd, sqstn, sans, url, key, theme, displayCarton];

  static Setting fromMap(Map<String, dynamic> map) {
    return Setting(
        id: int.parse(map[colId].toString()),
        pswd: map[colPswd],
        sqstn: map[colSqstn],
        sans: map[colSans],
        url: map[colUrl],
        key: map[colKey],
        theme: int.parse(map[colTheme].toString()),
        displayCarton: int.parse(map[colDisplayCarton].toString()));
  }

  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{
      colPswd: pswd,
      colSqstn: sqstn,
      colSans: sans,
      colUrl: url,
      colKey: key,
      colTheme: theme,
      colDisplayCarton: displayCarton
    };
    if (id != null) {
      map[colId] = id;
    }
    return map;
  }

  Setting copyWith(
      {int id,
      String pswd,
      String sqstn,
      String sans,
      String url,
      String key,
      int theme,
      int displayCarton}) {
    return Setting(
        id: id ?? this.id,
        pswd: pswd ?? this.pswd,
        sqstn: sqstn ?? this.sqstn,
        sans: sans ?? this.sans,
        url: url ?? this.url,
        key: key ?? this.key,
        theme: theme ?? this.theme,
        displayCarton: displayCarton ?? this.displayCarton);
  }

  @override
  String toString() {
    return '{pswd: $pswd, sqstn: $sqstn, sans: $sans, url: $url, key:$key, theme:$theme, displayCarton:$displayCarton}';
  }
}
