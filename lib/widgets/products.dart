import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:trade_app/blocs/blocs.dart';
import 'package:trade_app/models/models.dart';
import 'package:trade_app/utilities/debouncer.dart';
import 'package:trade_app/utilities/utilities.dart';
import 'package:trade_app/widgets/widgets.dart';

class Products extends StatefulWidget {
  @override
  _ProductsState createState() => _ProductsState();
}

class _ProductsState extends State<Products> {
  ScrollController _scrollController =
      new ScrollController(); // set controller on scrolling
  TextEditingController _searchController = TextEditingController();
  Completer<void> _refreshCompleter;

  @override
  void initState() {
    super.initState();
    _handleScroll();
    _refreshCompleter = Completer<void>();
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var _context = context;
    return Scaffold(
        body: GradientContainer(
            child: BlocConsumer<ProductsBloc, ProductsState>(
                listener: (context, productState) {
          if (productState is ProductsLoaded) {
            _refreshCompleter?.complete();
            _refreshCompleter = Completer();
          }
        }, buildWhen: (previousState, currentState) {
          return !(currentState is ProductsRefreshLoading);
        }, builder: (context, productsState) {
          return Column(
            children: <Widget>[
              Card(
                elevation: 4,
                child: Padding(
                  padding: const EdgeInsets.all(4.0),
                  child: Row(
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(left: 4, right: 6),
                        child: Icon(
                          Icons.search,
                          size: 18,
                          color: Theme.of(context).textTheme.headline4.color,
                        ),
                      ),
                      Expanded(
                        child: TextField(
                          controller: _searchController,
                          autofocus: false,
                          cursorColor:
                              Theme.of(context).textTheme.headline4.color,
                          decoration: Styles.getSearchBoxDecoration(context,
                              hint: 'Search Products', onClear: () {
                            _searchController.text = '';
                            _onSearchTextChange('');
                          }),
                          style: Styles.getInputTextStyle(context).copyWith(
                              decorationStyle: TextDecorationStyle.dotted,
                              decorationColor:
                                  Styles.getInputTextStyle(context).color),
                          onChanged: _onSearchTextChange,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Expanded(
                child: Card(
                  elevation: 4,
                  child: _listBuilder(context, productsState),
                ),
              )
            ],
          );
        })),
        floatingActionButton: BlocBuilder<FloatButtonBloc, FloatButtonState>(
            builder: (context, btnState) {
          return Visibility(
            visible: btnState is FloatButtonShown,
            child: FloatingActionButton(
              child: Icon(Icons.add),
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) {
                    return NewProduct(
                      mainContext: _context,
                    );
                  }),
                );
              },
            ),
          );
        }));
  }

  Widget _listBuilder(BuildContext context, ProductsState state) {
    if (state is ProductsLoading) {
      return Center(child: CircularProgressIndicator());
    } else if (state is ProductsFetchError) {
      return LoadError(message: state.message, onTryAgain: _fetchProducts);
    } else if (state is ProductsLoaded) {
      return RefreshIndicator(
          child: ListView.separated(
              itemCount: state.products.length,
              separatorBuilder: _seperatorBuilder,
              itemBuilder: _listItemBuilder,
              controller: _scrollController),
          onRefresh: _onRefresh);
    } else {
      // if page is empty
      return EmptyPage(
          icon: Image.asset('assets/images/empty.png', width: 70),
          message: 'No products found',
          onRefresh: _fetchProducts);
    }
  }

  Widget _seperatorBuilder(BuildContext context, int itemIndex) {
    return Divider(height: 0);
  }

  Widget _listItemBuilder(BuildContext context, int itemIndex) {
    var products =
        (BlocProvider.of<ProductsBloc>(context).state as ProductsLoaded)
            .products;
    return ListTile(
      dense: true,
      contentPadding: EdgeInsets.symmetric(horizontal: 16, vertical: 8),
      title: Padding(
        padding: const EdgeInsets.only(bottom: 2.0),
        child: Row(
          children: <Widget>[
            Expanded(
                child: Text(products[itemIndex].pname,
                    style: Theme.of(context)
                        .textTheme
                        .headline4
                        .copyWith(fontSize: 14, fontWeight: FontWeight.w600))),
            Container(
                padding: EdgeInsets.fromLTRB(4, 2, 4, 0),
                decoration: BoxDecoration(
                    border: Border.all(
                        color: Theme.of(context)
                            .textTheme
                            .headline4
                            .color
                            .withOpacity(0.7)),
                    borderRadius: BorderRadius.all(Radius.circular(5))),
                child: Text(products[itemIndex].displayPrice,
                    style: Theme.of(context)
                        .textTheme
                        .headline4
                        .copyWith(fontSize: 10)))
          ],
        ),
      ),
      subtitle: Row(
        children: <Widget>[
          Expanded(
              child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text('${products[itemIndex].pcs} items per carton',
                  style: Theme.of(context)
                      .textTheme
                      .headline4
                      .copyWith(fontSize: 12)),
              products[itemIndex].qty > 0
                  ? Text(products[itemIndex].displayQty,
                      style: Theme.of(context)
                          .textTheme
                          .headline4
                          .copyWith(fontSize: 12))
                  : Container(
                      margin: EdgeInsets.only(top: 2),
                      padding: EdgeInsets.fromLTRB(4, 1, 4, 1),
                      decoration: BoxDecoration(
                          color: Theme.of(context).errorColor,
                          border: Border.all(
                              color: Theme.of(context)
                                  .errorColor
                                  .withOpacity(0.7)),
                          borderRadius: BorderRadius.all(Radius.circular(5))),
                      child: Text('Out of stock',
                          style: Theme.of(context)
                              .textTheme
                              .headline4
                              .copyWith(fontSize: 10, color: Colors.white)))
            ],
          )),
          PopupMenuButton(
            child: Icon(Icons.more_vert,
                size: 20, color: Theme.of(context).textTheme.headline4.color),
            itemBuilder: (BuildContext context) => <PopupMenuEntry<String>>[
              PopupMenuItem<String>(
                height: 35,
                value: 'Edit_${products[itemIndex].pid}',
                child: Row(
                  children: <Widget>[
                    Icon(Icons.edit,
                        size: 14,
                        color: Theme.of(context).textTheme.headline4.color),
                    Padding(padding: EdgeInsets.symmetric(horizontal: 4)),
                    Text(
                      'Edit',
                      style: Theme.of(context)
                          .textTheme
                          .headline4
                          .copyWith(fontSize: 14),
                    ),
                  ],
                ),
              ),
              // PopupMenuDivider(
              //   height: 2,
              // ),
              PopupMenuItem<String>(
                height: 35,
                value: 'Delete_${products[itemIndex].pid}',
                child: Row(
                  children: <Widget>[
                    Icon(Icons.delete,
                        size: 14,
                        color: Theme.of(context).textTheme.headline4.color),
                    Padding(padding: EdgeInsets.symmetric(horizontal: 4)),
                    Text('Delete',
                        style: Theme.of(context)
                            .textTheme
                            .headline4
                            .copyWith(fontSize: 14)),
                  ],
                ),
              ),
            ],
            onSelected: (val) {
              _onItemOptionSelect(context, val);
            },
          )
        ],
      ),
    );
  }

  void _handleScroll() {
    _scrollController.addListener(() {
      Debouncer(milliseconds: 1000).run(() {
        if (_scrollController.position.userScrollDirection ==
            ScrollDirection.reverse) {
          BlocProvider.of<FloatButtonBloc>(context)
              .add(SetButtonVisibility(isVisible: false));
        }
        if (_scrollController.position.userScrollDirection ==
            ScrollDirection.forward) {
          BlocProvider.of<FloatButtonBloc>(context)
              .add(SetButtonVisibility(isVisible: true));
        }
      });
    });
  }

  void _onSearchTextChange(String searchText) {
    Debouncer(milliseconds: 500).run(() {
      BlocProvider.of<ProductsBloc>(context)
          .add(SearchProducts(isOnline: false, searchText: searchText));
    });
  }

  Future<void> _onRefresh() async {
    BlocProvider.of<ProductsBloc>(context).add(RefreshProducts(
        isOnline: BlocProvider.of<ConnectivityBloc>(context).state
            is ConnectivityOnline,
        searchText: _searchController.text));
    return _refreshCompleter.future;
  }

  void _fetchProducts() {
    BlocProvider.of<ProductsBloc>(context).add(FetchProducts(
        isOnline: BlocProvider.of<ConnectivityBloc>(context).state
            is ConnectivityOnline));
  }

  Future<void> _onItemOptionSelect(
      BuildContext _context, String optionValue) async {
    var isAuthorized = await Dialogs.getAuthDialog(context);
    if (!isAuthorized) {
      Dialogs.alertDialog(context, 'Access denied', type: AlertType.error);
    } else {
      var arr = optionValue.split('_');
      int val = int.parse(arr[1]);
      if (arr[0] == 'Edit') {
        var product = await BlocProvider.of<ProductsBloc>(context)
            .dbRepo
            .productsDataProvider
            .get(val);
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) {
            return EditProduct(
              mainContext: _context,
              product: ProductVM.fromProduct(product),
            );
          }),
        );
      } else if (arr[0] == 'Delete') {
        var connectivityState =
            BlocProvider.of<ConnectivityBloc>(context).state;
        if (connectivityState is ConnectivityOffline) {
          Dialogs.alertDialog(context,
              "You're currently offline! Ensure your mobile device has internet access and try again.",
              type: AlertType.info);
        } else {
          var proceed = await Dialogs.confirmDialog(
              context, 'Do you wish to continue?',
              subtitle:
                  'Deleting this product will also delete all records (both purchases and sales) associated with it.',
              buttonLabels: ['No', 'Yes']);
          if (proceed) {
            // BlocProvider.of<DeleteProductBloc>(context)
            //     .add(DeleteProduct(productId: val));
            _deleteProduct(val);
          }
        }
      }
    }
  }

  void _deleteProduct(int productId) async {
    var loader =
        Dialogs.getLoaderDialog(context, initialMessage: 'Deleting product...')
          ..show();
    try {
      var apiRepo = BlocProvider.of<ProductsBloc>(context).apiRepo;
      var dbRepo = BlocProvider.of<ProductsBloc>(context).dbRepo;
      await apiRepo.productsDataProvider.delete(productId);
      await dbRepo.productsDataProvider.delete(productId);

      await loader.hide();
      await Dialogs.alertDialog(context, 'Product was deleted successfully.',
          type: AlertType.success);
      BlocProvider.of<ProductsBloc>(context).add(
          RefreshProducts(isOnline: false, searchText: _searchController.text));
    } catch (ex) {
      var msg = ex is TimeoutException
          ? 'Request timed out! Ensure you have a strong network signal'
          : ex.toString();

      await loader.hide();
      await Dialogs.alertDialog(context, msg, type: AlertType.error);
    }
  }
}
