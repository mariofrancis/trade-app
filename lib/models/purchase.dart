import 'package:equatable/equatable.dart';

class Purchase extends Equatable {
  final int id;
  final int pid;
  final double price;
  final int qty;
  final String day;
  final String month;
  final int dateCreated;

  Purchase(
      {this.id,
      this.pid,
      this.price,
      this.qty,
      this.day,
      this.month,
      this.dateCreated});

  @override
  List<Object> get props => [id, pid, price, qty, day, month, dateCreated];

  // constants
  static const table = 'purchases';
  static const colId = 'id';
  static const colPid = 'pid';
  static const colPrice = 'price';
  static const colQty = 'qty';
  static const colDay = 'day';
  static const colMonth = 'month';
  static const colDateCreated = 'date_created';

  static Purchase fromMap(Map<String, dynamic> map) {
    return Purchase(
        id: int.parse(map[colId].toString()),
        pid: int.parse(map[colPid].toString()),
        price: double.parse(map[colPrice].toString()),
        qty: int.parse(map[colQty].toString()),
        day: map[colDay],
        month: map[colMonth],
        dateCreated: int.parse(map[colDateCreated].toString()));
  }

  Purchase copyWith(
      {int id,
      int pid,
      double price,
      int qty,
      String day,
      String month,
      int dateCreated}) {
    return Purchase(
        id: id ?? this.id,
        pid: pid ?? this.pid,
        price: price ?? this.price,
        qty: qty ?? this.qty,
        day: day ?? this.day,
        month: month ?? this.month,
        dateCreated: dateCreated ?? this.dateCreated);
  }

  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{
      colPid: pid,
      colPrice: price,
      colQty: qty,
      colDay: day,
      colMonth: month,
      colDateCreated: dateCreated
    };
    if (id != null) {
      map[colId] = id;
    }
    return map;
  }
}
