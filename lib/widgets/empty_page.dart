import 'package:flutter/material.dart';

class EmptyPage extends StatelessWidget {
  final Widget icon;
  final String message;
  final void Function() onRefresh;

  EmptyPage({@required this.icon, @required this.message, @required this.onRefresh});
  
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
          child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Padding(padding: EdgeInsets.only(top: 100)),
              Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
                  //child: Image.asset('assets/images/empty.png', width: 70)
                  child: icon,
                  ),
              Text(
                message,
                style: Theme.of(context).textTheme.headline4.copyWith(
                    fontSize: 18,
                    color: Theme.of(context).textTheme.headline4.color.withOpacity(.5)),
              ),
              Padding(
                  padding: EdgeInsets.symmetric(horizontal: 16, vertical: 8),
                  child: FlatButton(
                    child: Text('Refresh'),
                    color: Theme.of(context).primaryColorLight,
                    textColor: Theme.of(context).textTheme.headline4.color.withOpacity(.6),
                    onPressed: onRefresh,
                  ))
            ],
          ),
        ),
    );
  }
}