import 'package:flutter/material.dart';

class FormFieldCard extends StatelessWidget {
  final Widget child;

  FormFieldCard({@required this.child}) : assert(child != null);
  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 2,
      //color: Theme.of(context).primaryColorLight,
      child: Padding(
        padding: const EdgeInsets.only(bottom: 4, top:2),
        child: child,
      ),
    );
  }
}
