part of 'sales_online_bloc.dart';

abstract class SalesOnlineEvent extends Equatable {
  const SalesOnlineEvent();
}
class FetchSalesOnline extends SalesOnlineEvent{
  final String month;

  FetchSalesOnline({@required this.month}):assert(month!=null);

  @override
  List<Object> get props => [month];
}
class RefreshSalesOnline extends SalesOnlineEvent{
  final String month;

  RefreshSalesOnline({@required this.month}):assert(month!=null);

  @override
  List<Object> get props => [month];
}