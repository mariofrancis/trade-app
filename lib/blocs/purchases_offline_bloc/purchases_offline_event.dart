part of 'purchases_offline_bloc.dart';

abstract class PurchasesOfflineEvent extends Equatable {
  const PurchasesOfflineEvent();

   @override
  List<Object> get props => [];
}
class FetchPurchasesOffline extends PurchasesOfflineEvent{}
class RefreshPurchasesOffline extends PurchasesOfflineEvent{}