import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:trade_app/blocs/blocs.dart';
import 'package:trade_app/models/models.dart';
import 'package:trade_app/utilities/utilities.dart';
import 'package:trade_app/widgets/widgets.dart';

class EditPurchase extends StatefulWidget {
  final BuildContext mainContext;
  final Product product;
  final Purchase purchase;
  final bool isOnline;

  EditPurchase(
      {@required this.mainContext,
      @required this.product,
      @required this.purchase,
      @required this.isOnline});

  @override
  _EditPurchaseState createState() => _EditPurchaseState(
      mainContext: mainContext,
      product: product,
      purchase: purchase,
      isOnline: isOnline);
}

class _EditPurchaseState extends State<EditPurchase> {
  final Product product;
  final Purchase purchase;
  final bool isOnline;
  final BuildContext mainContext;
  final _formKey = GlobalKey<FormState>();
  final _priceController = TextEditingController();
  final _qtyController = TextEditingController();
  String _qtyUnit = 'piece';

  _EditPurchaseState(
      {@required this.mainContext,
      @required this.product,
      @required this.purchase,
      @required this.isOnline});

  @override
  void initState() {
    var r = purchase.qty % product.pcs;
    if (r == 0) {
      _qtyUnit = 'carton';
      _qtyController.text =
          (int.parse((purchase.qty / product.pcs).round().toString()))
              .toString();
      _priceController.text = (purchase.price * purchase.qty).toStringAsFixed(2);
    } else {
      _qtyUnit = 'piece';
      _qtyController.text = purchase.qty.toString();
      _priceController.text = (purchase.price * purchase.qty).toStringAsFixed(2);
    }
    super.initState();
  }

  @override
  void dispose() {
    _priceController.dispose();
    _qtyController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Edit Purchase Record'),
      ),
      body: Column(
        children: <Widget>[
          ConnectivityOfflineBanner(),
          Expanded(
            child: GradientContainer(
                child: Padding(
              padding:
                  const EdgeInsets.symmetric(horizontal: 4.0, vertical: 4.0),
              child: _buildForm(context),
            )),
          ),
        ],
      ),
    );
  }

  // get form
  Widget _buildForm(BuildContext context) {
    return Form(
        key: _formKey,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: <Widget>[
            Expanded(
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Padding(padding: EdgeInsets.only(top: 32)),
                    FormFieldCard(
                      child: TextFormField(
                        initialValue: product.pname,
                        enabled: false,
                        decoration: Styles.getInputDecoration(context,
                            label: 'Product Name',
                            hint: 'What is the product name?'),
                        style: Styles.getInputTextStyle(context).copyWith(
                            decorationColor: Theme.of(context)
                                .primaryColorDark
                                .withOpacity(.5)),
                      ),
                    ),
                    Padding(padding: EdgeInsets.symmetric(vertical: 12)),

                    Padding(
                      padding: const EdgeInsets.only(bottom: 4, left: 4),
                      child: Text(
                        'Quantity Purchased',
                        style: Theme.of(context).textTheme.headline4.copyWith(
                              fontSize: 12,
                            ),
                      ),
                    ),
                    Row(
                      children: <Widget>[
                        Expanded(
                          flex: 1,
                          child: FormFieldCard(
                            child: DropdownButtonFormField<String>(
                              value: _qtyUnit,
                              decoration: Styles.getInputDecoration(context),
                              items: [
                                DropdownMenuItem(
                                  child: Text('Piece'),
                                  value: 'piece',
                                ),
                                DropdownMenuItem(
                                  child: Text('Carton'),
                                  value: 'carton',
                                )
                              ],
                              style: Styles.getInputTextStyle(context).copyWith(
                                  decorationColor: Theme.of(context)
                                      .primaryColorDark
                                      .withOpacity(.5)),
                              onChanged: (val) {
                                setState(() {
                                  _qtyUnit = val;
                                });
                              },
                            ),
                          ),
                        ),
                        Padding(padding: EdgeInsets.symmetric(horizontal: 4)),
                        Expanded(
                          flex: 2,
                          child: FormFieldCard(
                            child: TextFormField(
                                controller: _qtyController,
                                decoration: Styles.getInputDecoration(context,
                                    label: 'Quantity *',
                                    hint: 'Quantity in pieces/cartons'),
                                style: Styles.getInputTextStyle(context)
                                    .copyWith(
                                        decorationColor: Theme.of(context)
                                            .primaryColorDark
                                            .withOpacity(.5)),
                                keyboardType: TextInputType.numberWithOptions(decimal: false, signed: false),
                                validator: (value) {
                                  if (value.trim().isEmpty) {
                                    return 'Quantity is required';
                                  }else if(int.parse(value) < 1){
                                    return 'Quantity cannot be less than 1';
                                  }
                                  return null;
                                }),
                          ),
                        )
                      ],
                    ),
                    Padding(padding: EdgeInsets.symmetric(vertical: 12)),
                    // Price Purchases
                    FormFieldCard(
                      child: TextFormField(
                          controller: _priceController,
                          decoration: Styles.getInputDecoration(context,
                              label: 'Toatl price purchased *',
                              //hint: 'What is the price for a carton/piece?'),
                              hint: 'What is the total price?'),
                          style: Styles.getInputTextStyle(context).copyWith(
                              decorationColor: Theme.of(context)
                                  .primaryColorDark
                                  .withOpacity(.5)),
                          keyboardType: TextInputType.number,
                          validator: (value) {
                            if (value.trim().isEmpty) {
                              return 'Price is required';
                            }
                            return null;
                          }),
                    ),

                    Padding(padding: EdgeInsets.symmetric(vertical: 12)),
                  ],
                ),
              ),
            ),
            Divider(
              height: 2,
            ),
            RaisedButton.icon(
              onPressed: () {
                _onSubmit(context);
              },
              icon: Icon(
                Icons.check_circle_outline,
                size: 16,
              ),
              label: Text('Update Record'),
              colorBrightness: Theme.of(context).accentColorBrightness,
            )
          ],
        ));
  }

  Future<void> _onSubmit(BuildContext _context) async {
    if (_formKey.currentState.validate()) {
      var qty = int.parse(_qtyController.text);
      var price = double.parse(_priceController.text);
      var _purchase = Purchase(
          id: purchase.id,
          pid: product.pid,
          price: _qtyUnit == 'piece' ? (price/qty) : ((price / qty)/ product.pcs),
          qty: _qtyUnit == 'piece' ? qty : (qty * product.pcs),
          day: purchase.day,
          month: purchase.month,
          dateCreated: purchase.dateCreated);

      // BlocProvider.of<EditPurchaseBloc>(_context)
      //     .add(UpdatePurchase(purchase: _purchase, isOnline: isOnline));
      _updatePurchase(_purchase, isOnline);
    }
  }

  void _updatePurchase(Purchase purchase, bool isOnline) async {
    var loader = Dialogs.getLoaderDialog(context,
        initialMessage: 'Updating purchase record...');
    await loader.show();

    try {
      var apiRepo = BlocProvider.of<PurchasesOnlineBloc>(context).apiRepo;
      var dbRepo = BlocProvider.of<PurchasesOnlineBloc>(context).dbRepo;

      var product = await dbRepo.productsDataProvider.get(purchase.pid);
      if (isOnline) {
        await apiRepo.purchasesDataProvider
            .update(ApiPurchase.fromPurchase(purchase));
        BlocProvider.of<ProductsBloc>(context)
            .add(RefreshProducts(isOnline: true, searchText: ''));

        await _onPurchaseUpdated(loader, isOnline);
      } else {
        var prec = await dbRepo.purchasesDataProvider.get(purchase.id);
        if (prec == null) {
          await loader.hide();
          await Dialogs.alertDialog(context,
              'Specified offline record cannot be found! This record might have been sent to server. Kindly check under online records.',
              type: AlertType.error);
        } else {
          await dbRepo.purchasesDataProvider.update(purchase);
          var diff = purchase.qty - prec.qty;
          product = product.copyWith(qty: product.qty + diff); // update product
          dbRepo.productsDataProvider.update(product); // update product
          BlocProvider.of<ProductsBloc>(context)
              .add(RefreshProducts(isOnline: false, searchText: ''));

          await _onPurchaseUpdated(loader, isOnline);
        }
      }
    } catch (ex) {
      var msg = ex is TimeoutException
          ? 'Request timed out! Ensure you have a strong network signal'
          : ex.toString();
      await loader.hide();
      await Dialogs.alertDialog(context, msg, type: AlertType.error);
    }
  }

  Future _onPurchaseUpdated(ProgressDialog loader, bool isOnline) async {
    await loader.hide();
    await Dialogs.alertDialog(
        context,
        isOnline
            ? 'Purchase record was updated successfully.'
            : 'Purchase record was updated offline.',
        type: AlertType.success);
    _refreshPurchaseRecords(isOnline);
    _clearFields();
    Navigator.pop(context);
  }

  void _clearFields() {
    _priceController.clear();
    _qtyController.clear();
    // setState(() {
    //   _qtyUnit = 'piece';
    // });
  }

  void _refreshPurchaseRecords(bool isOnline) {
    if (isOnline) {
      BlocProvider.of<PurchasesOnlineBloc>(context).add(RefreshPurchasesOnline(
          month: BlocProvider.of<PurchasesOnlineBloc>(context).month));
    } else {
      BlocProvider.of<PurchasesOfflineBloc>(context)
          .add(RefreshPurchasesOffline());
    }
  }
}
