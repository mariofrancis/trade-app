import 'dart:async';
import 'package:meta/meta.dart';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:trade_app/blocs/blocs.dart';
import 'package:trade_app/models/models.dart';
import 'package:trade_app/repositories/repositories.dart';

part 'purchases_offline_event.dart';
part 'purchases_offline_state.dart';

class PurchasesOfflineBloc
    extends Bloc<PurchasesOfflineEvent, PurchasesOfflineState> {
  final ApiRepository apiRepo;
  final DbRepository dbRepo;
  final ProductsBloc productsBloc;
  final SettingsBloc settingsBloc;

  PurchasesOfflineBloc(
      {@required this.apiRepo,
      @required this.dbRepo,
      @required this.productsBloc,
      @required this.settingsBloc})
      : assert(apiRepo != null && dbRepo != null && productsBloc != null && settingsBloc!=null);

  @override
  PurchasesOfflineState get initialState => PurchasesOfflineEmpty();

  @override
  Stream<PurchasesOfflineState> mapEventToState(
    PurchasesOfflineEvent event,
  ) async* {
    if (event is FetchPurchasesOffline) {
      yield* _fetchPurchases(event);
    } else if (event is RefreshPurchasesOffline) {
      yield* _refreshPurchases(event);
    }
  }

// =============== private methods ==========================
  Stream<PurchasesOfflineState> _fetchPurchases(
      FetchPurchasesOffline event) async* {
    yield PurchasesOfflineLoading();
    try {
      final purchases = await dbRepo.purchasesDataProvider.getAll();
      if (purchases.length == 0) {
        yield PurchasesOfflineEmpty();
      } else {
        var records = List<Record>();

        for (int i = 0; i < purchases.length; i++) {
          var r = await Record.fromPurchase(purchases[i], dbRepo, inCartons: settingsBloc.setting.displayCarton==1?true:false);
          records.add(r);
        }

        yield PurchasesOfflineLoaded(purchases: records);
      }
    } catch (ex) {
      yield PurchasesOfflineFetchError(message: ex.toString());
    }
  }

  Stream<PurchasesOfflineState> _refreshPurchases(
      RefreshPurchasesOffline event) async* {
    yield PurchasesOfflineRefreshLoading();
    try {
      final purchases = await dbRepo.purchasesDataProvider.getAll();
      if (purchases.length == 0) {
        yield PurchasesOfflineEmpty();
      } else {
        var records = List<Record>();

        for (int i = 0; i < purchases.length; i++) {
          var r = await Record.fromPurchase(purchases[i], dbRepo, inCartons: settingsBloc.setting.displayCarton==1?true:false);
          records.add(r);
        }

        yield PurchasesOfflineLoaded(purchases: records);
      }
    } catch (ex) {
      yield PurchasesOfflineFetchError(message: ex.toString());
    }
  }
}
