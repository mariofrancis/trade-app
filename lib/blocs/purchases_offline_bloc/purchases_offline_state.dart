part of 'purchases_offline_bloc.dart';

abstract class PurchasesOfflineState extends Equatable {
  const PurchasesOfflineState();

  @override
  List<Object> get props => [];
}

class PurchasesOfflineEmpty extends PurchasesOfflineState {}

class PurchasesOfflineLoading extends PurchasesOfflineState {}

class PurchasesOfflineRefreshLoading extends PurchasesOfflineState {}

class PurchasesOfflineLoaded extends PurchasesOfflineState {
  final List<Record> purchases;

  PurchasesOfflineLoaded({@required this.purchases}) : assert(purchases != null);
  @override
  List<Object> get props => [purchases];
}

class PurchasesOfflineFetchError extends PurchasesOfflineState {
  final String message;

  PurchasesOfflineFetchError({@required this.message}) : assert(message != null);
  @override
  List<Object> get props => [message];
}
