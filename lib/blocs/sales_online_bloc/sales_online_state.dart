part of 'sales_online_bloc.dart';

abstract class SalesOnlineState extends Equatable {
  const SalesOnlineState();

  @override
  List<Object> get props => [];
}

class SalesOnlineEmpty extends SalesOnlineState {}

class SalesOnlineLoading extends SalesOnlineState {}

class SalesOnlineRefreshLoading extends SalesOnlineState {}

class SalesOnlineLoaded extends SalesOnlineState {
  final SaleVM sales;

  SalesOnlineLoaded({@required this.sales}) : assert(sales != null);
  @override
  List<Object> get props => [sales];
}

class SalesOnlineFetchError extends SalesOnlineState {
  final String message;

  SalesOnlineFetchError({@required this.message}) : assert(message != null);
  @override
  List<Object> get props => [message];
}
