import 'package:equatable/equatable.dart';

class ApiSetting extends Equatable {
  final String pswd;
  final String sqstn;
  final String sans;
  final String url;

  ApiSetting({this.pswd, this.sqstn, this.sans, this.url});

  @override
  List<Object> get props => [pswd, sqstn, sans, url];

  static ApiSetting fromMap(Map<String, dynamic> map){
    return ApiSetting(
      pswd: map['pswd'].toString(),
      sqstn: map['sqstn'].toString(),
      sans: map['sans'].toString(),
      url: map['url'].toString()
    );
  }

  Map<String, dynamic> toMap(){
    return {
      'pswd':pswd,
      'sqstn':sqstn,
      'sans':sans,
      'url':url
    };
  }

  @override
  String toString(){
    return '{pswd:$pswd, sqstn:$sqstn, sans:$sans, url:$url}';
  }
}
