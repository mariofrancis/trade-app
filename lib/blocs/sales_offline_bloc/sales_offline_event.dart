part of 'sales_offline_bloc.dart';

abstract class SalesOfflineEvent extends Equatable {
  const SalesOfflineEvent();

   @override
  List<Object> get props => [];
}
class FetchSalesOffline extends SalesOfflineEvent{}
class RefreshSalesOffline extends SalesOfflineEvent{}