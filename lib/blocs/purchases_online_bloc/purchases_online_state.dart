part of 'purchases_online_bloc.dart';

abstract class PurchasesOnlineState extends Equatable {
  const PurchasesOnlineState();

  @override
  List<Object> get props => [];
}

class PurchasesOnlineEmpty extends PurchasesOnlineState {}

class PurchasesOnlineLoading extends PurchasesOnlineState {}

class PurchasesOnlineRefreshLoading extends PurchasesOnlineState {}

class PurchasesOnlineLoaded extends PurchasesOnlineState {
  final PurchaseVM purchases;

  PurchasesOnlineLoaded({@required this.purchases}) : assert(purchases != null);
  @override
  List<Object> get props => [purchases];
}

class PurchasesOnlineFetchError extends PurchasesOnlineState {
  final String message;

  PurchasesOnlineFetchError({@required this.message}) : assert(message != null);
  @override
  List<Object> get props => [message];
}
