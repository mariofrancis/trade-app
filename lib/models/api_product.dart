import 'package:equatable/equatable.dart';
import 'package:trade_app/models/models.dart';

class ApiProduct extends Equatable {
  final int id;
  final String pname;
  final double price;
  final int qty;
  final int pcs;
  final int dateCreated;

  // constructor
  ApiProduct(
      {this.id, this.pname, this.price, this.qty, this.pcs, this.dateCreated});

  @override
  List<Object> get props => [id, pname, price, qty, pcs, dateCreated];

  static ApiProduct fromMap(Map<String, dynamic> map) {
    return ApiProduct(
        id: int.parse(map['id'].toString()),
        pname: map['pname'],
        price: double.parse(map['price'].toString()),
        qty: int.parse(map['qty'].toString()),
        pcs: int.parse(map['pcs'].toString()),
        dateCreated: int.parse(map['date_created'].toString()));
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'pname': pname,
      'price': price,
      'qty': qty,
      'pcs': pcs,
      'date_created': dateCreated
    };
  }

  Product toProduct() {
    return Product(
        pid: id,
        pname: pname,
        price: price,
        qty: qty,
        pcs: pcs,
        dateCreated: dateCreated);
  }

  @override
  String toString() {
    return '{id:$id, pname:$pname, price:$price, qty:$qty, pcs:$pcs, dateCreated:${DateTime.fromMillisecondsSinceEpoch(dateCreated * 1000).toLocal()}}';
  }
}
