import 'dart:async';
import 'package:meta/meta.dart';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

part 'theme_event.dart';
part 'theme_state.dart';

class ThemeBloc extends Bloc<ThemeEvent, ThemeState> {
  final int initialThemeId;

  ThemeBloc({@required this.initialThemeId}):assert(initialThemeId!=null);
  @override
  ThemeState get initialState => ThemeUpdated(themeId: initialThemeId);

  @override
  Stream<ThemeState> mapEventToState(
    ThemeEvent event,
  ) async* {
    if(event is UpdateTheme){
      yield ThemeUpdated(themeId: event.themeId);
    }
  }
}
