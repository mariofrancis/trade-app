import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:trade_app/blocs/blocs.dart';
import 'package:trade_app/widgets/widgets.dart';

class Sales extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
        length: 2,
        child: Scaffold(
            bottomNavigationBar: Card(
              elevation: 8,
              //shape: ContinuousRectangleBorder(),
              margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
              child: TabBar(
                onTap: (i) {
                  print(i);
                },
                indicatorWeight: 2,
                labelStyle: Theme.of(context).textTheme.headline5,
                 labelColor: Theme.of(context).accentColor,
                unselectedLabelStyle: Theme.of(context).textTheme.headline5,
                unselectedLabelColor:
                    Theme.of(context).primaryColorDark.withOpacity(.6),
                labelPadding: EdgeInsets.all(0),
                indicatorPadding: EdgeInsets.all(0),
                tabs: [
                  Tab(
                    child: Column(
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(top: 4, bottom: 2),
                          child: Icon(
                            Icons.cloud_queue,
                            size: 20,
                          ),
                        ),
                        Text(
                          'Online Records',
                          textScaleFactor: .5,
                        )
                      ],
                    ),
                  ),
                  Tab(
                    child: Column(
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(top: 4, bottom: 2),
                          child: Icon(
                            Icons.cloud_off,
                            size: 20,
                          ),
                        ),
                        Text(
                          'Offline Records',
                          textScaleFactor: .5,
                        )
                      ],
                    ),
                  )
                ],
              ),
            ),
            body: TabBarView(
              children: [
                SalesOnline(),
                SalesOffline(),
              ],
            ),
            floatingActionButton:
                BlocBuilder<FloatButtonBloc, FloatButtonState>(
                    builder: (context, btnState) {
              return Visibility(
                visible: btnState is FloatButtonShown,
                child: FloatingActionButton(
                  child: Icon(Icons.add),
                  onPressed: () async {
                    await _goToNewSale(context);
                  },
                ),
              );
            })));
  }

  Future _goToNewSale(BuildContext context) async {
    var dbRepo = BlocProvider.of<ProductsBloc>(context).dbRepo;
    var products = await dbRepo.productsDataProvider.getAll();
    products.sort((a, b) => a.pname.compareTo(b.pname));
    await Navigator.push(
      context,
      MaterialPageRoute(builder: (_context) {
        return NewSale(
          mainContext: context,
          products: products,
        );
      }),
    );
    var connected =
        BlocProvider.of<ConnectivityBloc>(context).state is ConnectivityOnline;
    if (connected) {
      BlocProvider.of<SalesOnlineBloc>(context)
          .add(RefreshSalesOnline(month: ''));
    }
    BlocProvider.of<SalesOfflineBloc>(context).add(RefreshSalesOffline());
  }
}
