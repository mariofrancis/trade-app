import 'package:equatable/equatable.dart';
import 'package:intl/intl.dart';
import 'record.dart';

class PurchaseVM extends Equatable {
  final String month;
  final List<String> months;
  final List<DayRecord> dayRecords;
  final String total;

  PurchaseVM({this.month, this.months, this.dayRecords, this.total});

  @override
  List<Object> get props => [month, months, dayRecords, total];

  static PurchaseVM fromJson(dynamic json, bool inCartons) {
    var jsonDayRecs = json['day_records'] as List;
    var jsonMonths = json['months'] as List;
    var dayRecs = jsonDayRecs.map((e) => DayRecord.fromJson(e, inCartons: inCartons)).toList();
    var months = jsonMonths.map((e) => e.toString()).toList();
    return PurchaseVM(
        month: json['month'],
        months: months,
        dayRecords: dayRecs,
        total: NumberFormat.simpleCurrency(name: 'NGN', decimalDigits: 2)
            .format(double.parse(json['total'].toString())));
  }
}
